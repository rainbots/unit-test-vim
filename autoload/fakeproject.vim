" FakeProject is a class for creating fake C projects to test Vim scripts.
"
" Dependecies:
    " NERDTree (`nerdtree.vim`)
    " my extension lib `nerdtree_.vim`.
    " `folder.vim`
"=====[ Private ]=====
function! fakeproject#_CreateFolders(show) dict
    "Create fake project 'self.name' for testing Vim scripts.
    "All projects have folders `src` and `test`.
    "If `show` is 1, show new folders in NERDTree.
    call folder#Create(self.srcpath)
    call folder#Create(self.testpath)
    if a:show
        call nerdtree_#ExpandLocalFolder(self.name)
    endif
endfunction
function! fakeproject#_DestroyFolders(show) dict
    "If `show` is 1, refresh NERDTree after deleting project.
    if !folder#Exists(self.projpath)
        echo 'Nothing to delete.' self.projpath 'does not exist.'
        return
    endif
    let deleted_empty_folder = folder#DeleteIfEmpty(self.projpath)
    if  deleted_empty_folder
        if a:show
            silent call nerdtree_#RefreshActiveFolder()
        endif
        return
    endif
    " The folder is not empty!
    " Check if the user wants a recursive delete.
    " If this is the expected project folder, do not bother asking the user.
    let ok_to_delete_these_folders = [
        \'/home/Mike/.vim/pack/bundle/dev/autoload/tests/project',
        \'/home/Mike/.vim/pack/bundle/dev/expectmocking/project',
        \]
    let recursively_deleted_folder = folder#RecursiveDelete(
        \self.projpath,
        \ok_to_delete_these_folders
        \)
    if recursively_deleted_folder
        if a:show
            silent call nerdtree_#RefreshActiveFolder()
        endif
        return
    endif
    echo 'Failed to delete ' self.projpath
endfunction
function! fakeproject#_CreateFiles(show) dict
    "Create empty files in the project.
    "TODO: take a list of file names instead of hardcoding.
    "If `show` is 1, show new project in NERDTree.
    for test_file in self.test_files
        if !file#New(self.testpath . '/' . test_file)
            call assert_report( 'Failed to create file "'.test_file.'".')
            return 0
        endif
    endfor
    for src_file in self.src_files
        if !file#New(self.srcpath . '/' . src_file)
            call assert_report( 'Failed to create file "'.src_file.'".')
            return 0
        endif
    endfor
    if a:show
        call nerdtree_#ExpandLocalFolder(self.name)
    endif
    return 1
endfunction
function! fakeproject#_PathToCodeRef()
    return '/home/Mike/.vim/pack/bundle/dev/expectmocking/c-code-ref'
endfunction
function! fakeproject#_CopyAllCode() dict
    for test_file in self.test_files
        let copy_to = self.testpath . '/' . test_file
        if !file#Exists(copy_to)
            call assert_report('File ' . copy_to . ' does not exist. Cannot copy code there.')
            return 0
        endif
        let copy_from = fakeproject#_PathToCodeRef()
        let copy_from = copy_from . '/' . test_file
        let code = readfile(copy_from, 'b')
        call writefile(code, copy_to, 'b')
    endfor
    for src_file in self.src_files
        let copy_to = self.srcpath . '/' . src_file
        if !file#Exists(copy_to)
            call assert_report('File ' . copy_to . ' does not exist. Cannot copy code there.')
            return 0
        endif
        let copy_from = fakeproject#_PathToCodeRef()
        let copy_from = copy_from . '/' . src_file
        let code = readfile(copy_from, 'b')
        call writefile(code, copy_to, 'b')
    endfor
    return 1
endfunction
function! fakeproject#_CopyStartingCode(filename) dict
    if fakeproject#_IsATestFile(a:filename)
        let copy_to = self.testpath . '/' . a:filename
    else
        let copy_to = self.srcpath . '/' . a:filename
    endif
    "let copy_to = self.testpath.'/test_LUT.c'
    if !file#Exists(copy_to)
        call assert_report('File ' . copy_to . ' does not exist. Cannot copy code there.')
        return 0
    endif
    let copy_from = fakeproject#_PathToCodeRef()
    let copy_from = copy_from . '/' . a:filename
    "echomsg "copy from: " . copy_from
    let code = readfile(copy_from, 'b')
    call writefile(code, copy_to, 'b')
    return 1
endfunction
function! fakeproject#_IsATestFile(filepath)
    let filename = fnamemodify(a:filepath, ":t")
    let prefix = filename[0:4]
    if (prefix !=# 'test_') && (prefix !=# 'mock_')
        return 0
    endif
    return 1
endfunction

"=====[ Public ]=====
" The FakeProject class is defined here:
function! fakeproject#NewProject(name)
    " Create a fake project named 'name' in the current dev folder.
    let fake = {
        \'_CreateFolders'       : function('fakeproject#_CreateFolders'),
        \'_DestroyFolders'      : function('fakeproject#_DestroyFolders'),
        \'_CreateFiles'         : function('fakeproject#_CreateFiles'),
        \'_CopyAllCode'         : function('fakeproject#_CopyAllCode'),
        \'_CopyStartingCode'    : function('fakeproject#_CopyStartingCode'),
        \'_UpdateTags'           : function('fakeproject#_UpdateTags'),
        \'Make'                 : function('fakeproject#Make'),
        \'Clean'                : function('fakeproject#Clean'),
        \'ReturnFromFile'       : function('fakeproject#ReturnFromFile'),
        \}
        "\'CdProjectDir'         : function('fakeproject#CdProjectDir'),
        "\'GoToFile'             : function('fakeproject#GoToFile'),
        "\'Toplevel'             : function('fakeproject#TopLevel')
    let fake.name = a:name
    let fake.projpath = expand("%:p:h").'/'.fake.name
    let fake.srcpath  = fake.projpath.'/src'
    let fake.testpath = fake.projpath.'/test'
    let fake.src_files = [
        \'DOL.c',
        \'DOL.h',
        \'DOL2.c',
        \'DOL2.h',
        \'LUT.c',
        \'LUT.h'
        \]
    let fake.test_files = [
        \'test_LUT.c',
        \'test_LUT.h',
        \'test_LUT_MockUps.c',
        \'test_LUT_MockUps.h',
        \'mock_DOL.c',
        \'mock_DOL.h',
        \'mock_DOL2.c',
        \'mock_DOL2.h'
        \]
    return fake
endfunction
function! fakeproject#CdProjectDir(project_dir)
    "Change cwd to fake project top-level. Return the pwd before the change.
    "Example:
    "   let save_dir = self.CdProjectDir()
    "   ... do stuff, then return to previous working directory
    "   silent execute 'cd ' save_dir
    let save_dir = getcwd()
    silent execute 'cd ' a:project_dir
    return save_dir
endfunction
function! fakeproject#_UpdateTags() dict
    "Creates/updates a tags file for the fake project.
    "Example: call self._UpdateTags()
    "change directory
    let save_dir = fakeproject#CdProjectDir(self.projpath)
    silent call ctags#Update()
    "restore directory
    silent execute 'cd ' save_dir
    return 1
endfunction
function! fakeproject#Make(show) dict
    "If `show` is 1, show new project in NERDTree.
    call self._CreateFolders(a:show)
    if !self._CreateFiles(a:show)
        "_CreateFiles logs the test failures for any files it cannot create.
        return 0
    endif
    "if !self._CopyStartingCode('test_LUT.c')
    "    "_CopyStartingCode logs the test failures for any code it cannot copy.
    "    return 0
    "endif
    if !self._CopyAllCode()
        "_CopyAllCode logs the test failures for any files that do not exist.
        return 0
    endif
    call self._UpdateTags()
    return 1
endfunction
function! fakeproject#Clean(show) dict
    "Example: call fake.Clean()
    call self._DestroyFolders(a:show)
endfunction
" And these helper functions to maintain FakeProject as an ADT.
function! fakeproject#Show_changing_in_Nerdtree()
    " Do not refresh the Nerdtree view each time the project is created/deleted.
    " This makes the test run much faster. I do a final refresh of the Nerdtree
    " after all tests are done running.
    "Usage: Use this function as a global.
    "Example:
    "   let run_quickly = !fakeproject#Show_changing_in_Nerdtree()
    " Then use in calls to fake.Setup and fake.Teardown:
    "   call fake.Setup(g:run_quickly)
    "   call a:Project.Teardown(g:run_quickly)
    return 1
endfunction
function! fakeproject#GoToFile(filepath)
    "GoToFile goes to 'filepath' in fake project and returns 1.
    "If `filepath` does not exist, GoToFile logs in v:errors and returns 0.
    "Examples:
    "let filepath = 
        "   call fakeproject#GoToFile(filepath)
        "or this safer try/catch flavor:
        "   if !fakeproject#GoToFile(filepath)
        "       return 0 " What to do if unable to go to the file.
        "   endif
    if !file#Exists(a:filepath)
        call assert_report('No file named `' . a:filepath . '` in the project.')
        return 0
    endif
    "File exists. Go to it.
    silent execute "noswapfile edit " . a:filepath
    return 1
endfunction
function! fakeproject#OldGoToFile(filename) dict "not used
    "GoToFile goes to 'filename' in fake project and returns 1.
    "If `filename` does not exist, GoToFile logs in v:errors and returns 0.
    "'filename' is just a file, not a path.
    "   GoToFile determines from `filename` whether it is in `test` or `src`.
    "   and fills in the rest of the path.
    "Examples:
        "   call fake.GoToFile('test_LUT.c')
        "or this safer try/catch flavor:
        "   if !fake.GoToFile('test_LUT.c')
        "       return 0 " What to do if unable to go to the file.
        "   endif
    let filepath =  fakeproject#_IsATestFile(a:filename) ?
                    \ self.testpath . '/' . a:filename  :
                    \ self.srcpath  . '/' . a:filename
    if !file#Exists(filepath)
        call assert_report('No file named `' . a:filename . '` in the project.')
        return 0
    endif
    "File exists. Go to it.
    silent execute "noswapfile edit " . filepath
    return 1
endfunction
function! fakeproject#ReturnFromFile() dict
    silent execute "noswapfile buffer #"
endfunction
function! fakeproject#TopLevel() dict " not used
    return self.projpath
endfunction
