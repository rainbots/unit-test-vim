function! testgroup#_SaveTestResults(filepath) dict
    "let results = ['# Test group: '. self.name]
    let results = ['## '. self.name]
    call add(results, '')
    let results += self.failed
    call add(results, '')
    let results += self.summary
    call writefile(results, a:filepath, 'a')
endfunction
function! testgroup#_CreateSummary() dict
    let self.summary = []
    call add(self.summary, '------------------')
    let totals  = self.number_tested.' Tests '
                \.self.number_failed.' Failures'
    call add(self.summary, totals)
endfunction
function! testgroup#ViewTestResults(filepath) dict
    call self._CreateSummary()
    call self._SaveTestResults(a:filepath)
    " Print to Vim command line.
    "echomsg 'Test group: '. self.name
    echomsg '~'
    echomsg self.name
    for line in self.failed
        echomsg line
    endfor
    for line in self.summary
        echomsg line
    endfor
endfunction
function! testgroup#RecordTest(test_name) dict
    let self.number_tested += 1
    let test_passed = v:errors == []
    if test_passed | let self.number_passed += 1
    else           | let self.number_failed += 1
    endif
    "Announce tests that pass
    if test_passed
        "let good_news = '- *Test #'
        let good_news = '- **PASS** *Test #'
                    \ . self.number_tested . '*: '
                    \ . '`' . a:test_name . '`'
                    "\ . '`' . a:test_name . '`' . ' **PASS**'
        call add(self.failed, good_news)
    endif
    "Copy fail messages from v:errors to self.failed.
    if !test_passed
        for failure in v:errors
            let traceback = split(failure,':')[0]
            let reason    = split(failure,':')[1]
            let setup_failed = matchstr(traceback, '\M**Setup failed**') != ''
            if  setup_failed
                let reason = '- **Setup failed** for *Test #'
                            \ . self.number_tested . '*:`'
                            \ . a:test_name . '`:' . reason . '.'
            endif
            let teardown_failed = matchstr(traceback, '\M**Teardown failed**') != ''
            if  teardown_failed
                let reason = '- **Teardown failed** for *Test #'
                            \ . self.number_tested . '*:`'
                            \ . a:test_name . '`:' . reason . '.'
            endif
            if !setup_failed && !teardown_failed
                "let reason = '- *Test #'
                let reason = '- **FAIL** *Test #'
                            \ . self.number_tested . '*: '
                            \ . '`' . a:test_name . '`' . reason . '.'
                            "\ . '`' . a:test_name . '`' . ' **FAIL**:' . reason . '.'
            endif
            call add(self.failed, reason)
            "Eliminate the repetitive first part of the traceback.
            let traceback = traceback[matchstrpos(traceback,'\M..')[1]:]
            "Add indentation and markdown prettiness.
            let traceback = '    - *Traceback*: `' . traceback . '`'
            call add(self.failed, traceback)
        endfor
    endif
endfunction
function! testgroup#Setup()
    echo "Nothing to setup." | return 1
endfunction
function! testgroup#Teardown()
    echo "Nothing to teardown." | return 1
endfunction
function! testgroup#_LogFailedSetup()
    let fail_msg = '**Setup failed** '
    let default  = '..Setup failed without any assertion..so no traceback: Cause of failure unknown'
    if len(v:errors) == 0
        let v:errors = [fail_msg . default]
    else
        let v:errors[-1] = '**Setup failed** ' . v:errors[-1]
    endif
endfunction
function! testgroup#_LogFailedTeardown()
    let fail_msg = '**Teardown failed** '
    let default  = '..Teardown failed without any assertion..so no traceback: Cause of failure unknown'
    if len(v:errors) == 0
        let v:errors = [fail_msg . default]
    else
        let v:errors[-1] = '**Teardown failed** ' . v:errors[-1]
    endif
endfunction
function! testgroup#RunTest(test_name) dict
    "Call 'test_name' and log the result in the TestGroup instance.
    let v:errors = []
    let Fref = function(a:test_name)
    if self.Setup() | call Fref()
    else | call testgroup#_LogFailedSetup()    | endif
    if self.Teardown()
    else | call testgroup#_LogFailedTeardown() | endif
    call self.RecordTest(a:test_name)
endfunction
function! testgroup#New(name)
    "USAGE: let tests = testgroup#New()
    let TestGroup = {
        \'_CreateSummary'       : function('testgroup#_CreateSummary'),
        \'_SaveTestResults'     : function('testgroup#_SaveTestResults'),
        \'ViewTestResults'      : function('testgroup#ViewTestResults'),
        \'RecordTest'           : function('testgroup#RecordTest'),
        \'RunTest'              : function('testgroup#RunTest'),
        \'Setup'                : function('testgroup#Setup'),
        \'Teardown'             : function('testgroup#Teardown')
        \}
    let TestGroup.name = a:name
    let TestGroup.failed  = [ ]
    let TestGroup.summary = [ ]
    let TestGroup.number_tested = 0
    let TestGroup.number_passed = 0
    let TestGroup.number_failed = 0
    return TestGroup
endfunction

