" A simple unit-test lib to write Vim script using TDD.
"
" Concept:
    " Uses the `dict` Vim datatype as an "object" to store test results.
    " Output test results to screen with `_EchoResults()` or to a file with
    " `SaveResultsToFile()`. Or use these as starting points to customize your
    " own test output.
" Dependencies: none
" USAGE:
    " "Install" this script.
        " Get the Vim `runtimepath` to pick up `vimtdd.vim` by placing it in an
        " `autoload` folder like any other plugin.
    " Write Vim script using TDD (test driven development):
        " Develop `foobar.vim` by creating `test_foobar.vim`.
        " `test_foobar.vim` creates a *TestGroup*:
            "
            "   let tests = vimtdd#NewTestGroup()
            "
        " All other functions defined in `vimtdd.vim` are TestGroup methods,
        " for example:
            "
            "   tests.RunTest('Delete_should_prompt_user_if_foobar_is_not_empty')
            "
        " Destroy the dictionary when you are done with the results:
            "
            "   unlet tests
            "
        " All tests in `test_foobar.vim` call functions defined in `foobar.vim`
        " and make assertions about their behavior. In the above example, the
        " test calls function:
            "
            "   foobar#Delete()
            "
        " and the test checks that `foobar#Delete()` prompts the user.
    " The test file (e.g., `test_foobar.vim`) has two parts:
        " Part I: define tests:
            "funtion! Module_foobar_func_Blah1_should_do_blah1()
            "   call foobar#Blah1()  "*operate* function under test
            "   call assert_true()...    "*test* by asserting some behavior
            "endfunction
            "funtion! BlahFunc2_should_do_blah2()
            "   foobar#BlahFunc2()  "*operate* function under test
            "   assert_blah()...    "*test* by asserting some behavior
            "endfunction
        " A failing assertion adds to the list `v:errors`.
        " Example:
            " :echo len(v:errors)
            " :call assert_true(0)
            " :echo len(v:errors)
        " The assertion fails because 0 is considered false.
        " Notice that the length of `v:errors` increases by one.
        " v:errors is cleared by `RunTest`
        " The assertions are typically either of these flavors:
            " - an expected value equals the actual value returned by the
            "   *function under test*
            " - the *function under test* returns `true` (or `false`)
            " See the list of Vim assertions at the end of this file.
            " These assertions should be sufficient to cover all scenarios.
        " Part II: define a *TestGroup* to run the tests and output the result:
            "let   tests = vimtdd#NewTestGroup()
            "call  tests.RunTest('BlahFunc1_should_do_blah1')
            "call  tests.RunTest('BlahFunc2_should_do_blah2')
            "call  tests.ViewResults()
            "call  tests.SaveResultsToFile('./example-test-results.md')
            "unlet tests
    " See full test file example at end of this file.
" TODO:
    " [ ] add support for setup and teardown in each TestGroup
    " [ ] add support for mocks to isolate the FUT:
    "   - stub DOFs with versions that record to a list of actual calls
    "   - set up test with a list of expected calls
    "   - call the FUT to trigger the stubbed DOFs
    "   - test passes if the expected calls match the actual calls

function! vimtdd#_EchoResults() dict
    for line in self.failed
        echomsg line
    endfor
    echomsg '------------------'
    echomsg self.number_tested 'Tests' self.number_failed 'Failures'
    "Example:
    "------------------
    "1 Tests 0 Failures
endfunction
function! vimtdd#ViewTestResults() dict
    "Call this after the final call to RunTest for a TestGroup.
    silent call nerdtree_#RefreshActiveFolder()
    call self._EchoResults()
endfunction
function! vimtdd#SaveResultsToFile(filepath) dict
    let results = []
    let results += self.failed
    call add(results, '')
    call add(results, '------------------')
    let summary = self.number_tested.' Tests '
                \.self.number_failed.' Failures'
    call add(results, summary)
    "Example:
    "------------------
    "1 Tests 0 Failures
    call writefile(results, a:filepath)
endfunction
function! vimtdd#RunTest(test_name) dict
    "Call 'test_name' and log the result using the test-results object.
    let v:errors = []
    let Fref = function(a:test_name)
    call Fref()
    call self._RecordThisTest(a:test_name)
endfunction
" Leading '_' denotes private. This is an unenforced convention.
function! vimtdd#_RecordThisTest(test_name) dict
    "Increment number of tests/pass/fail. If `test_name` failed, record why.
    let self.number_tested += 1
    let test_passed = v:errors == []
    "if v:errors == []
    if test_passed
        let self.number_passed += 1
    else
        let self.number_failed += 1
        let fail_msg = a:test_name.':FAIL:'.split(v:errors[0],':')[1]
        call add(self.failed, fail_msg)
    endif
endfunction
" The TestGroup class is defined here:
function! vimtdd#NewTestGroup()
    "USAGE: let tests = vimtdd#NewTestGroup
    let TestGroup = {
        \'_RecordThisTest'      : function('vimtdd#_RecordThisTest'),
        \'_EchoResults'         : function('vimtdd#_EchoResults'),
        \'ViewTestResults'      : function('vimtdd#ViewTestResults'),
        \'SaveResultsToFile'    : function('vimtdd#SaveResultsToFile'),
        \'RunTest'              : function('vimtdd#RunTest')
        \}
    let TestGroup.failed = []
    let TestGroup.number_tested = 0
    let TestGroup.number_passed = 0
    let TestGroup.number_failed = 0
    return TestGroup
endfunction
function! vimtdd#_IsATestFile(filepath)
    let filename = fnamemodify(a:filepath, ":t")
    let prefix = filename[0:4]
    if (prefix !=# 'test_')
        return 0
    endif
    let extension = fnamemodify(a:filepath, ":e")
    if (extension !=# 'vim')
        return 0
    endif
    return 1
endfunction
function! vimtdd#_FileHasUnsavedChanges()
    return &modified
endfunction
function! vimtdd#AddTestToHeader()
    "echo ">^.^< Called lib vimtdd from the .vimrc."
    if !vimtdd#_IsATestFile(expand("%"))
        echo "This is not a vim test file."
        return
    endif
    "echo "This is a vim test file."
    if vimtdd#_FileHasUnsavedChanges()
        echo "This file has unsaved changes. Nothing was done."
        return
    endif
    "Copy test name from its function name:
    let test = matchstr(split(getline('.'))[1],'.*(')[0:-2]
    "Make this a RunTest call:
    let test = "    call tests.RunTest('" . test . "')"
    " Find next end-of-tests without wrapping:
    let save_cursor = getcurpos()
    let line_num = search('ViewTestResults', 'W')
    execute '-1put=test'
    call setpos('.', save_cursor)
    echo "Pasted test name on line above 'ViewTestResults'"
endfunction
"This is here as a reminder to future me:
    "function! vimtdd#DestroyTestGroup(TestGroup)
    "You cannot destroy an argument. The user has to call `unlet`.
    "    unlet a:TestGroup
    "endfunction

"----------------------------------------------------------------------------
"=====[ Example tests ]=====
"function! This_should_pass()
"    call assert_equal(1,1)
"endfunction
"function! This_should_pass_also()
"    call assert_equal('alice','alice')
"endfunction
"function! This_should_fail_as_not_equal()
"    call assert_equal(1,2)
"endfunction
"function! This_should_fail_as_not_true()
"    call assert_true(0)
"endfunction
"function! This_should_fail_with_a_message()
"    call assert_true(0, 'Result was false, here is the message.')
"endfunction

"=====[ Example test runner -- you write your own test runner! ]=====
"let tests = vimtdd#NewTestGroup()
"call tests.RunTest('This_should_pass')
"call tests.RunTest('This_should_fail_as_not_equal')
"call tests.RunTest('This_should_fail_as_not_true')
"call tests.RunTest('This_should_fail_with_a_message')
"call tests.RunTest('This_should_pass_also')
"call tests.EchoResults()
"call tests.SaveResultsToFile('./example-test-results.md')
"unlet tests

" Full list of Vim assertions:
" assert_equal({expected}, {actual} [, {msg}])
" assert_equalfile({fname-one}, {fname-two})
" assert_exception({error} [, {msg}])
" assert_fails({cmd} [, {error}])
" assert_false({actual} [, {msg}])
" assert_inrange({lower}, {upper}, {actual} [, {msg}])
" assert_match({pattern}, {actual} [, {msg}])
" assert_notequal({expected}, {actual} [, {msg}])
" assert_notmatch({pattern}, {actual} [, {msg}])
" assert_report({msg})
" assert_true({actual} [, {msg}])

