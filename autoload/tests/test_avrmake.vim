" Unit tests for `avrmake.vim`.
" Well, I started setting up the tests, but I ended up just testing the code
" manually and moving on.

execute 'source autoload/avrmake.vim'
execute 'source autoload/testgroup.vim'

let s:this_test_file = expand("%:p")
let s:results_save_path = fnamemodify(s:this_test_file, ":h").'/test-results.md'

" =====[ List of tests ]=====
    " [x] CloseWindow returns 0 if no unique buffer exists
    " [x] CloseWindow returns 0 if the buffer is not in a window
    " [x] CloseWindow returns 1 if buffer window was found and closed
    " [ ] CloseLogfileWindows closes my avr log files
        " [x] Manually tested.
    " [ ] CloseLogfileWindows does not affect other files
        " How do I test this?

" =====[ Test globals ]=====
" s:existing_buf is created by the test and destroyed by Teardown
let s:existing_buf = 'unique_bufname_1'
" s:imaginary_buf never exists
let s:imaginary_buf = 'imaginary_bufname_1'

" =====[ Test helpers ]=====
function! s:CreateBuffer(bufname)
    " Buffer is created, but then it does not exist in any open window.
    silent execute "noswapfile edit " . a:bufname
    silent execute "noswapfile buffer #"
endfunction
function! s:CreateBufferInNewWindow(bufname)
    " Buffer is created and lives in its own window.
    silent execute "split"
    silent execute "noswapfile edit " . a:bufname
    silent execute "wincmd p"
endfunction
"
" =====[ The tests ]=====
function! CloseWindow_returns_0_if_no_unique_buffer_exists()
    call assert_equal(0, avrmake#CloseWindow(s:imaginary_buf))
endfunction
function! CloseWindow_returns_0_if_the_buffer_is_not_in_a_window()
    " call assert_false(1)
    call s:CreateBuffer(s:existing_buf)
    call assert_equal(0, avrmake#CloseWindow(s:existing_buf))
endfunction
function! CloseWindow_returns_1_if_buffer_window_was_found_and_closed()
    " call assert_false(1)
    call s:CreateBufferInNewWindow(s:existing_buf)
    call assert_equal(1, avrmake#CloseWindow(s:existing_buf))
endfunction
"
" =====[ Setup and Teardown ]=====
function! s:Setup_CloseWindow(bufname)
    " echo 'Setup_CloseWindow() received arg `' . a:bufname . '`' | return 1
    " call s:CreateBuffer(a:bufname)
    return 1
endfunction
function! s:UniqueBufferExists(bufname)
    let l:buffer_number = bufnr(a:bufname)
    let unique_buffer_exists = l:buffer_number != -1
    return unique_buffer_exists
endfunction
function! s:Teardown_CloseWindow(bufname)
    " echo 'Teardown_CloseWindow() received arg `' . a:bufname . '`' | return 1
    if s:UniqueBufferExists(a:bufname)
        silent execute "bwipe " . a:bufname
    endif
    return 1
endfunction
"
" =====[ ;yt to add tests to the group ]=====
function! s:Testgroup_CloseWindow()
    let tests = testgroup#New('Testing lib: `avrmake` function: `CloseWindow()`')
    let tests.Setup    = function('s:Setup_CloseWindow',    [s:existing_buf])
    let tests.Teardown = function('s:Teardown_CloseWindow', [s:existing_buf])
    call tests.RunTest('CloseWindow_returns_0_if_no_unique_buffer_exists')
    call tests.RunTest('CloseWindow_returns_0_if_the_buffer_is_not_in_a_window')
    call tests.RunTest('CloseWindow_returns_1_if_buffer_window_was_found_and_closed')
    call tests.ViewTestResults(s:results_save_path)
    call s:AddTestGroup(s:all_tests, tests)
endfunction

"=====[ test runner ]=====
function! s:TestRunner()
    function! s:AddTestGroup(total, testgroup)
        "Called by each test group before being popped off the stack.
        let a:total.number_tested += a:testgroup.number_tested
        let a:total.number_passed += a:testgroup.number_passed
        let a:total.number_failed += a:testgroup.number_failed
    endfunction
    "Clear out test results file.
    call writefile(['# Test Results'], s:results_save_path)
    let s:all_tests = testgroup#New('Summary for all tests')
        "See :h script-variable
    "Run tests.
    call s:Testgroup_CloseWindow()
    "View the summary for all tests.
    call s:all_tests.ViewTestResults(s:results_save_path)
"unlet all_tests
endfunction

call s:TestRunner()

" Full list of Vim assertions:
" assert_equal({expected}, {actual} [, {msg}])
" assert_equalfile({fname-one}, {fname-two})
" assert_exception({error} [, {msg}])
" assert_fails({cmd} [, {error}])
" assert_false({actual} [, {msg}])
" assert_inrange({lower}, {upper}, {actual} [, {msg}])
" assert_match({pattern}, {actual} [, {msg}])
" assert_notequal({expected}, {actual} [, {msg}])
" assert_notmatch({pattern}, {actual} [, {msg}])
" assert_report({msg})
" assert_true({actual} [, {msg}])
