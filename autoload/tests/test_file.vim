"Unit tests for 'file.vim'
"TODO: reorganize tests using testgroup.vim

execute 'source autoload/file.vim'

"=====[ List of tests ]=====
    "[x] UnsavedChanges returns true if current file has unsaved changes.
    "[x] ThisIsC returns true if filetype is C.
    "[x] ThisIsC returns false if filetype is not C.
    "[x] New creates a file if the folder exists.
    "[x] New creates nothing if filepath directory does not exist.
    "[x] New returns 0 if filepath directory does not exist.
    "[x] New overwrites an existing file with an empty file

let run_quickly = !fakeproject#Show_changing_in_Nerdtree()

function! s:Setup_Testing_ThisIsC(Project)
    let Teardown = function('s:Teardown_Testing_ThisIsC', [a:Project])
    if !a:Project.Make(g:run_quickly)    | call Teardown() | return 0 | endif
    if !a:Project.GoToFile('test_LUT.c') | call Teardown() | return 0 | endif
    return 1
endfunction
function! s:Teardown_Testing_ThisIsC(Project)
    call a:Project.ReturnFromFile()
    silent call a:Project.Clean(g:run_quickly)
endfunction
function! s:Teardown_Testing_New(Project)
    call a:Project.Clean(g:run_quickly)
endfunction
"=====[ tests ]=====
function! UnsavedChanges_returns_true_if_current_file_has_unsaved_changes()
    "Setup: create a file, go to it, make some unsaved changes.
    let project = fakeproject#NewProject('project')
    call folder#Create(project.Toplevel())
    let filepath = project.Toplevel() . '/blah.c'
    if !file#New(filepath) | call project.Clean(g:run_quickly) | return | endif
    silent call nerdtree_#RefreshActiveFolder()
    "make unsaved changes
    silent execute "noswapfile edit " . filepath
    execute 'normal! iblah'
    execute 'normal! oblah'

    call assert_true(file#UnsavedChanges())
    "
    "Teardown: write file, this project folder.
    silent execute 'w'
    call project.ReturnFromFile()
    call project.Clean(g:run_quickly)
endfunction
function! ThisIsC_returns_true_if_filetype_is_C()
    let project = fakeproject#NewProject('project')
    if !s:Setup_Testing_ThisIsC(project) | return | endif
    call assert_true(file#ThisIsC())
    call s:Teardown_Testing_ThisIsC(project)
endfunction
function! ThisIsC_returns_false_if_filetype_is_not_C()
    let fake = fakeproject#NewProject('project')
    call assert_false(file#ThisIsC())
endfunction
function! New_creates_a_file_if_the_folder_exists()
    " Setup
    let fake = fakeproject#NewProject('project')
    call folder#Create(fake.Toplevel())
    "call nerdtree_#RefreshActiveFolder()
    let filepath = fake.Toplevel() . '/blah.c'
    " Operate
    call file#New(filepath)
    " Test
    call assert_true(file#Exists(filepath))
    " Teardown
    call s:Teardown_Testing_New(fake)
endfunction
function! New_creates_nothing_if_filepath_directory_does_not_exist()
    " Setup
    let fake = fakeproject#NewProject('project')
    let filepath = fake.Toplevel() . '/blah.c'
    " Operate and Test
    call assert_false(file#Exists(filepath))
    " Teardown
endfunction
function! New_returns_0_if_filepath_directory_does_not_exist()
    " Setup
    let fake = fakeproject#NewProject('project')
    let filepath = fake.Toplevel() . '/blah.c'
    " Operate and Test
    call assert_false(file#New(filepath))
    " Teardown
endfunction
function! New_overwrites_an_existing_file_with_an_empty_file()
    " Setup
    let fake = fakeproject#NewProject('project')
    call folder#Create(fake.Toplevel())
    let old_filepath = fake.Toplevel() . '/old.c'
    call writefile(['line1', 'line2'], old_filepath)
    let empty_filepath = fake.Toplevel() . '/empty.c'
    call writefile([], empty_filepath)
    call file#New(old_filepath)
    " Operate and Test
    call assert_equalfile(empty_filepath, old_filepath)
    " Teardown
    call s:Teardown_Testing_New(fake)
endfunction

"=====[ test runner ]=====
silent call nerdtree_#RefreshActiveFolder()
let tests = vimtdd#NewTestGroup()
call tests.RunTest('ThisIsC_returns_true_if_filetype_is_C')
call tests.RunTest('ThisIsC_returns_false_if_filetype_is_not_C')
call tests.RunTest('New_creates_a_file_if_the_folder_exists')
call tests.RunTest('New_creates_nothing_if_filepath_directory_does_not_exist')
call tests.RunTest('New_returns_0_if_filepath_directory_does_not_exist')
call tests.RunTest('New_overwrites_an_existing_file_with_an_empty_file')
call tests.RunTest('UnsavedChanges_returns_true_if_current_file_has_unsaved_changes')
call tests.ViewTestResults()
call tests.SaveResultsToFile('./autoload/tests/test-results.md')
unlet tests
