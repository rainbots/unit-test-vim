# Test Results
## Testing lib: `avrmake` function: `CloseWindow()`

- **PASS** *Test #1*: `CloseWindow_returns_0_if_no_unique_buffer_exists`
- **PASS** *Test #2*: `CloseWindow_returns_0_if_the_buffer_is_not_in_a_window`
- **PASS** *Test #3*: `CloseWindow_returns_1_if_buffer_window_was_found_and_closed`

------------------
3 Tests 0 Failures
## Summary for all tests


------------------
3 Tests 0 Failures
