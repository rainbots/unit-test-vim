"Unit tests for 'tables.vim'
" Usage:
" set pwd to ~/.vim/pack/bundle/dev/
" Create test functions.
" Add each test function to the test group. [ ] shortcut?
" Run this test script:
    " ;so
    " [ ] shortcut to clear messages first and *then* source script?
" If the script has an error, view the errors:
    " :messages
" View just the last 20 messages:
    " :20mess
" Clear messages:
    " :messages clear
" If you spacebar past the end or quit and want to review, go back to
" the last viewed page of messages with:
    " g<

" Reload the framework for creating test groups.
" execute 'source autoload/testgroup.vim'
" Reload the module under test.
execute 'source autoload/tables.vim'

function! Module_tables_has_plumbing()
   " call assert_true(0) " Expect this fails.
   " call tables#HasPlumbing()  " Check module under test is callable.
   call assert_true(tables#HasPlumbing()) " Expect this passes.
endfunction

function! Module_tables_modifies_highlighted_text()
endfunction

let tests = vimtdd#NewTestGroup()
" call  tests.RunTest('Module_tables_has_plumbing')
call tests.RunTest('Module_tables_modifies_highlighted_text')
call  tests.ViewTestResults()
call  tests.SaveResultsToFile('./autoload/tests/tables-test-results.md')
unlet tests
