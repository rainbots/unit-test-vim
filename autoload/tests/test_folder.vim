"Unit tests for 'folder.vim'
"
execute 'source autoload/folder.vim'
call nerdtree_#RefreshActiveFolder()

let run_quickly = !fakeproject#Show_changing_in_Nerdtree()

function! s:Setup_Testing_ThisIsInFolder(Project)
    call a:Project.Make(g:run_quickly)
    return a:Project.GoToFile('test_LUT.c')
endfunction
function! s:Teardown_Testing_ThisIsInFolder(Project)
    call a:Project.ReturnFromFile()
    call a:Project.Clean(g:run_quickly)
endfunction
"=====[ tests ]=====
function! ThisIsInFolderX_is_true_if_current_file_is_in_folder_X()
    let fake = fakeproject#NewProject('project')
    if !s:Setup_Testing_ThisIsInFolder(fake)
        return 0
    endif
    call assert_true(folder#ThisIsInFolder('test'))
    call s:Teardown_Testing_ThisIsInFolder(fake)
endfunction
function! ThisIsInFolderX_is_false_if_current_file_is_not_in_folder_X()
    let fake = fakeproject#NewProject('project')
    if !s:Setup_Testing_ThisIsInFolder(fake)
        return 0
    endif
    call assert_false(folder#ThisIsInFolder('src'))
    call s:Teardown_Testing_ThisIsInFolder(fake)
endfunction
function! Create_creates_a_directory()
    " Setup
    let fake = fakeproject#NewProject('project')
    call assert_false(folder#Exists( fake.Toplevel() ))
    call folder#Create(fake.Toplevel())
    " Operate and Test
    call assert_true(folder#Exists( fake.Toplevel() ))
    " Teardown
    call fake.Clean(g:run_quickly)
endfunction
function! Create_returns_1_if_it_creates_a_directory()
    " Setup
    let fake = fakeproject#NewProject('project')
    " Operate and Test
    call assert_true(folder#Create( fake.Toplevel() ))
    " Teardown
    call fake.Clean(g:run_quickly)
endfunction
function! Create_creates_intermediate_directories_as_necessary()
    " Setup
    let fake = fakeproject#NewProject('project')
    call assert_false(folder#Exists( fake.Toplevel() ))
    let path_with_new_intermediate_directory = fake.Toplevel() . '/sub-folder'
    call folder#Create(path_with_new_intermediate_directory)
    " Operate and Test
    call assert_true(folder#Exists( path_with_new_intermediate_directory ))
    " Teardown
    call fake.Clean(g:run_quickly)
endfunction
function! Create_does_nothing_and_returns_0_if_directory_already_exists()
    " Setup
    let fake = fakeproject#NewProject('project')
    call folder#Create(fake.Toplevel())
    " Operate and Test
    call assert_false(folder#Create( fake.Toplevel() ))
    " Teardown
    call fake.Clean(g:run_quickly)
endfunction

"=====[ test runner ]=====
let tests = vimtdd#NewTestGroup()
call tests.RunTest('ThisIsInFolderX_is_true_if_current_file_is_in_folder_X')
call tests.RunTest('ThisIsInFolderX_is_false_if_current_file_is_not_in_folder_X')
call tests.RunTest('Create_creates_a_directory')
call tests.RunTest('Create_returns_1_if_it_creates_a_directory')
call tests.RunTest('Create_does_nothing_and_returns_0_if_directory_already_exists')
call tests.RunTest('Create_creates_intermediate_directories_as_necessary')
call nerdtree_#RefreshActiveFolder()
call tests.EchoResults()
call tests.SaveResultsToFile('./autoload/tests/test-results.md')
unlet tests


" Full list of Vim assertions:
" assert_equal({expected}, {actual} [, {msg}])
" assert_equalfile({fname-one}, {fname-two})
" assert_exception({error} [, {msg}])
" assert_fails({cmd} [, {error}])
" assert_false({actual} [, {msg}])
" assert_inrange({lower}, {upper}, {actual} [, {msg}])
" assert_match({pattern}, {actual} [, {msg}])
" assert_notequal({expected}, {actual} [, {msg}])
" assert_notmatch({pattern}, {actual} [, {msg}])
" assert_report({msg})
" assert_true({actual} [, {msg}])


