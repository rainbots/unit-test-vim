" Invoke make targets for AVR MCU flash programming.
" TODO: move CloseWindow to `file.vim`.

let s:happy_kitty     = ">\^.\^<"
let s:sad_kitty       = "<\*.\*>"
let s:trippy_kitty    = ">@.@<"
let s:waiting_kitty   = "-O.O-"
let s:default_kitty   = " v.v "
let s:invisible_kitty = "     "

" function! avrmake#EchoKitty(mood)
"     echomsg a:mood
" endfunction
" call avrmake#EchoKitty(s:happy_kitty)
" call avrmake#EchoKitty(s:sad_kitty)
" call avrmake#EchoKitty(s:trippy_kitty)
" call avrmake#EchoKitty(s:waiting_kitty)
" call avrmake#EchoKitty(s:default_kitty)

function! avrmake#GotoWindow(buffer_name)
    " Go to the buffer_name window if you can find it.
    " Return 1 for success, 0 for failure.
    " Input `buffer_name` is the name of an open buffer.
    " It should be as complete as possible to avoid ambiguity.
    " If there is more than buffer with the same file name,
    " this function does nothing.
    let l:buffer_number = bufnr(a:buffer_name)
    let unique_buffer_exists = l:buffer_number != -1
    if !unique_buffer_exists
        echomsg s:sad_kitty . 'No unique buffer exists.'
        echomsg s:invisible_kitty . 'Make sure the file is in this folder:'
        echomsg s:invisible_kitty . '`' .fnamemodify(".", ":p") . '`'
        return 0
    endif
    let window_id = win_findbuf(l:buffer_number)
    let window_exists = window_id != []
    if !window_exists
        echomsg s:sad_kitty . 'No window exists.'
        return 0
    endif
    " Go to the window if you can find it.
    let found_window = win_gotoid(window_id[0])
    return found_window
endfunction
function! avrmake#ResizeWindowHeight(buffer_name, height)
    " Resize the buffer_name window if you can find it.
    " Return 1 for success, 0 for failure.
    " See avrmake#GotoWindow() for details on input `buffer_name`.
    " If there is not exactly one buffer with name `buffer_name`,
    " this function does nothing.
    " If the cursor originated in a different window, it is returned there.
    let orig_window_number = winnr()
    let found_window = avrmake#GotoWindow(a:buffer_name)
    if !found_window
        echomsg s:sad_kitty . 'Cannot find window.'
        return 0
    endif
    " Resize the window
    execute a:height . "wincmd _"
    execute 'normal! G'
    execute orig_window_number . "wincmd w"
    return 1
endfunction
function! avrmake#CloseWindow(buffer_name)
    " Close the buffer_name window if you can find it.
    " Return 1 for success, 0 for failure.
    " See avrmake#GotoWindow() for details on input `buffer_name`.
    " If there is not exactly one buffer with name `buffer_name`,
    " this function does nothing.
    let found_window = avrmake#GotoWindow(a:buffer_name)
    if !found_window
        echomsg s:sad_kitty . 'Cannot find window.'
        return 0
    endif
    " Close the window
    execute "quit"
    " execute "bd!" l:buffer_number
    return 1
endfunction
" echo avrmake#CloseWindow('autoload/avrmake.vim')
function! avrmake#PwdHead()
    " Return the `head` from the pwd.
    " Usage: let pwd_head = avrmake#PwdHead()
    " The `head` is the last folder.
    " If `pwd` is `/home/mool/vim/src`
    " The `head` is `src`.
    " echomsg s:trippy_kitty
    let l:pwd = fnamemodify(".", ":p")
    let pwd_folders = split(l:pwd, '/')
    let pwd_head = pwd_folders[-1]
    return pwd_head
endfunction
function! avrmake#CloseLogfileWindows()
    " [x] Manually tested.
    " If the *pwd* contains either logfile,
    " and if either is open in a window,
    " that window is closed.
    " For the avr-size log file, determine if the *pwd* head is `simBrd` or
    " `mBrd`.
    "TODO: check if each call to CloseWindow is successful
    let pwd_head = avrmake#PwdHead()
    let l:logfile_buffers = [
        \'./build/atprogram-download_flash-stdout.log',
        \'./build/atprogram-download_flash-stderr.log',
        \'./build/avr-size_' . pwd_head . '.log'
        \]
    for l:logfile in l:logfile_buffers
        call avrmake#CloseWindow(l:logfile)
    endfor
endfunction
" call avrmake#CloseLogfileWindows()
