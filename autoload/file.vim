" Common functionality for managing files.

function! file#FindBufnum(path)
    "Return the buffer number of the file at path.
    "Use an absolute path ("%:p").
    "Example:
    "   let h_path = expand("%:p:r") . ".h"
    "   if bufexists(h_path)
    "       let h_bufnr = file#findBufnum(path)
    "       silent execute "noswapfile buffer "h_bufnr
    "   endif
    "return bufnr('') "This returns the current buffer number!
    if bufexists(a:path) | return bufnr(bufname("^".a:path."$")) | endif
    return ''
endfunction
"echo "does not exist -- buffer number:"file#FindBufnum(expand("%:p:r")."h")
"echo "        exists -- buffer number:"file#FindBufnum(expand("%:p"))

function! file#New(path)
    "Create a new file.
    "Do nothing and return 0 if `path` directory does not exist.
    if !folder#Exists(fnamemodify(a:path, ":h"))
        return 0
    endif
    let empty_file = []
    call writefile(empty_file, a:path)
    return 1
endfunction
function! file#Exists(path)
    if filewritable(a:path)
        return 1
    else
        return 0
    endif
endfunction
function! file#ThisIsC()
    " Return true if current filetype is C.
    "TODO: write unit test
    "ThisIsC returns 1 if cursor is in a C file.
    "TODO: write unit test
    "ThisIsC returns 0 if cursor is not in a C file.
    let this_file = expand("%:t")
    if &filetype != 'c' | echo '`'.this_file.'` is not a C file.' | endif
    return &filetype == 'c'
endfunction
"call file#ThisIsC()

function! file#UnsavedChanges()
    let this_file = expand("%:t")
    if &modified
        echo "Please save unsaved changes to `" . this_file . "`."
    endif
    return &modified
endfunction
"call file#UnsavedChanges()

"function! CurrentFileIsModified()
"    let this_file = expand("%:t")
"    if &modified
"        echo "Please save unsaved changes to `" . this_file . "`."
"    endif
"    return &modified
"endfunction

