let s:happy_kitty = ">\^.\^<"
let s:sad_kitty   = "<\*.\*>"
let s:default_kitty   = " v.v "

function! obj#Kitty() dict
    if self.happy | return s:happy_kitty | endif
    if self.sad   | return s:sad_kitty   | endif
    return s:default_kitty
endfunction
function! obj#Happy() dict
    let self.happy = 1
    let self.sad   = 0
endfunction
function! obj#Sad() dict
    let self.happy = 0
    let self.sad   = 1
endfunction
function! obj#DoHardThing() dict
    " A function that puts an error in v:errors
    call assert_true(0)
    return 0
endfunction
function! obj#DoEasyThing() dict
    " A function that does not put an error in v:errors
    call assert_true(1)
    return 1
endfunction
function! obj#New()
    "USAGE: let obj = obj#New()
    let Object = {
        \'Kitty' : function('obj#Kitty'),
        \'Happy' : function('obj#Happy'),
        \'Sad'   : function('obj#Sad'),
        \'DoHardThing'  : function('obj#DoHardThing'),
        \'DoEasyThing'  : function('obj#DoEasyThing')
        \}
    let Object.happy = 0
    let Object.sad = 0
    return Object
endfunction
