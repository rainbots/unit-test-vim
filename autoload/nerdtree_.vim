" *NERDtree* plugin extensions for use in key-mapping and Vim scripts.
"
" `nerdtree_.vim` has a trailing '_' so that the Vim `runtimepath` is not
" confused with the `nerdtree.vim` in the *actual NERDTree* plugin folder.
"
" Dependencies: NERDTree (`nerdtree.vim`)

let s:happy_kitty     = ">\^.\^<"
let s:sad_kitty       = "<\*.\*>"
let s:trippy_kitty    = ">@.@<"
let s:waiting_kitty   = "-O.O-"
let s:default_kitty   = " v.v "
let s:invisible_kitty = "     "

function! nerdtree_#RefreshActiveFolder()
    if ThisIsANerdtreeWindow()
        " Refresh this folder and return. As if user pressed 'r'.
        execute "normal r"
        return
    endif
    execute "NERDTreeFind"
    execute "normal r"
    silent execute "wincmd p"
endfunction
function! nerdtree_#ExpandLocalFolder(folder)
    call nerdtree_#RefreshActiveFolder()
    execute "NERDTreeFind"
    " if call search(a:folder, 'b') | execute "normal O" | endif
    call search(a:folder, 'b')
    execute "normal O"
    silent execute "wincmd p"
endfunction
function! s:PutCursorOnFolder(folder)
    " ---Used in s:OpenFolder(folder)---
    " Put the cursor on the `folder` in the NERDTree window.
    " Return 0 if folder was not found.
    " Return 1 if cursor is on folder.
    execute "NERDTreeFocus"
    " Place cursor on root to jump past any bookmarks with the word 'build'
    execute "normal P"
    " `search()` with `W` means `don't Wrap around the end`
    if search(a:folder, 'W') | return 1 | endif
    return 0
endfunction
function! s:OpenFolder(folder)
    " ---Used in nerdtree_#RefreshFolder()---
    " Opens a folder in the NERDTree window.
    " Return 0 if the folder was not found.
    " Return 1 if the folder was found.
    " If the folder was found, leave the cursor on it.
    " If the folder was not found, put the cursor back where it started.
    " echomsg s:happy_kitty | return
    let not_invoked_from_nerdtree_window =
        \!(
            \ (&filetype == 'nerdtree')
            \ &&
            \ (&buftype == 'nofile')
        \ )
    if !s:PutCursorOnFolder(a:folder)
        if not_invoked_from_nerdtree_window
            " cannot find folder -- go back to original window
            silent execute "wincmd p"
        endif
        echomsg s:sad_kitty . 'Cannot see `'. a:folder .'` folder.'
        return 0
    endif
    " `O` only opens nodes, it does not toggle like `o`.
    " Use 'normal', not 'normal!' for 'O' to behave as 'NERDTree-O' instead of
    " the Vim mapping.
    execute "normal O"
    echomsg s:happy_kitty | return 1
endfunction
function! nerdtree_#RefreshFolder(folder)
    " Return 1 if folder is refreshed.
    " Return 0 if refresh failed.
    " The refresh fails if the folder is not found.
    "
    let not_invoked_from_nerdtree_window =
        \!(
            \ (&filetype == 'nerdtree')
            \ &&
            \ (&buftype == 'nofile')
        \ )
    if !s:OpenFolder(a:folder) | return 0 | endif
    " Refresh
    execute "normal r"
    " Make sure cursor is back in original window.
    if not_invoked_from_nerdtree_window
        " cannot find folder -- go back to original window
        silent execute "wincmd p"
    endif
    return 1
endfunction

