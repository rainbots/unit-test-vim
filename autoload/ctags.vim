" Create and update tags files.
"
" Dependencies: 
    " ctags: Exuberant Ctags 5.8

function! ctags#Update()
    "Update an existing tags file or create a new one if it does not exist.
    "Only call this from the project root folder.
    "It recursively scans source files in sub-folders for tags.
    " call system("ctags --c-types=+l -R .")
    call system("ctags --c-kinds=+l --exclude=deprecated --exclude=Makefile -R .")
        "'-R' is recursive, and '.' is starting in the current folder.
        "See the list of options:
        "$ ctags --list-kinds=c
        "~~Use the above '--c-types=+' to turn these on.~~
        "Use the above '--c-kinds=+' to turn these on.
        "+l includes locals in the file. By default, locals are not tagged.
endfunction
function! ctags#UpdateLibTags()
    call system("make lib-dependencies compiler=gcc")
    " TODO: remind me how to make `lib-dependencies.c` if it does not exist yet
    call system("ctags -f lib-tags --c-kinds=+x+p -L lib-dependencies")
    echomsg ">\^.\^< Updated project `lib-tags` (tags for dependencies)."
endfunction
" function! ctags#PythonTags()
"     " Nah, see <leader>tp in .vimrc.
"     echomsg ">\^.\^<"
" endfunction
