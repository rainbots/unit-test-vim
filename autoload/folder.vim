" Common functionality for managing folders.
"
" Dependencies:
    " NERDTree (`nerdtree.vim`)
    " my extension lib `nerdtree_.vim`.

function! folder#ThisIsInFolder(foldername)
    "Return 1 if this file is in a folder named foldername."
    let folder = expand("%:h:t")
    if folder !=# a:foldername
        return 0
    endif
    return 1
endfunction
function! folder#Exists(path)
    if filewritable(a:path)
        return 1
    else
        return 0
    endif
endfunction
function! folder#Create(path)
    " Creates path if it does not exist, and any intermediate directories.
    " Return 1 if path is created, 0 if path already exists.
    if folder#Exists(a:path)
        echo a:path 'already exists'
        return 0
    endif
    call mkdir(a:path, 'p')
    return 1
endfunction
"=====[ Delete ]=====
function! folder#DeleteIfEmpty(path)
    "Delete an empty folder. Return 1 for success, 0 for failure.
    "If successful, refresh folder view and notify user.
    let delete_failed = delete(a:path,'d')
    if !delete_failed
        "call nerdtree_#RefreshActiveFolder()
        echo 'Deleted empty folder' a:path
        return 1
    endif
    return 0
endfunction
function! folder#_PermissionToDeleteContents(folder)
    "Return 1 if the user confirms to recursively delete the folder."
    let msg = "STOP! Directory is not empty. To delete, type 'yes'\n"
                \.a:folder.": "
    let reply = input(msg)
    echo "\n"
    return reply ==# "yes" ? 1 : 0
endfunction
function! folder#RecursiveDelete(folder, delete_list)
    "Recursively delete a folder. Return 1 for success, 0 for failure.
    "If successful, refresh folder view and notify user.
    " delete_list are folders that are OK to delete.
    " Example:
    " call folder#RecursiveDelete('./expectmocking/project/', ['./expectmocking/project/'])
    let ask_permission = 1
    for folder in a:delete_list
        if a:folder ==# folder
            let ask_permission = 0
        endif
    endfor
    if ask_permission
        if !folder#_PermissionToDeleteContents(a:folder)
            return 0
        endif
    endif
    let delete_failed = delete(a:folder,'rf')
    if !delete_failed
        echo 'Recursively deleted folder' a:folder
        return 1
    endif
    return 0
endfunction
