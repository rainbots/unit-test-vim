Vim script variable         | Type   | Purpose/Contents
-----------------------     | ------ | ----------------
`testfile`                  | string | path to return to this file after tag jumping
`fut`                       | dict   | group info about the FUT
`fut.lib`                   | string | 
`fut.name`                  | string | stop if tag not found
`fut.SetUp`                 | string | stop if tag not found
`fut.TearDown`              | string | stop if tag not found
`fut.SetUpMock`             | string | put lutSrc.callSetUpMock    in fut.SetUp
`fut.TearDownMock`          | string | put lutSrc.callTearDownMock in fut.TearDown
`dof`                       | dict   | group info about DOF
`dof.sig`                   | string | `DOF(arg1, arg2)`
`dof.lib`                   | string | stop if name of DOL is unknown
`dolSrc`                    | dict   | group info about source code added to the DOL
`dolSrc.files`              | dict   | group paths to source code files in the DOL
`dolSrc.files.c`            | string | stop if path to DOL C file does not exist
`dolSrc.files.h`            | string | stop if path to DOL H file does not exist
`dof.name`                  | string | stop if `DOF` (name from dof.sig) is defined
`dof.name`                  | string | stop if `DOF_Implementation` is defined
`dof.args`                  | string | stop if undeclared args in dof.sig: `(arg1, arg2)`
`dof.argtypes`              | string | dof.args with arg names replaced by types
`lutSrc.callSetUpMock`      | list   | created code
`lutSrc.callTearDownMock`   | list   | created code
`dolSrc.fp_implem`          | list   | created code
`dolSrc.fp_assign`          | list   | created code

