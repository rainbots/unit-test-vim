"Unit tests for expectmocking.`vim`.
"TODO:
execute 'source expectmocking/expectmocking.vim'
"=====[ Context ]=====
" mockist tests check a function's behavior in one path through its conditionals
    " The FUT is a function in the LUT
    " FUT := Function Under Test
    " LUT := Lib      Under Test
    " The FUT depends on other functions. This is why we mock.
    " A DOF is a function in the DOL
    " DOF := Depended On Function
    " DOL := Depended On Lib
    " The FUT and DOF are always different functions.
    " The LUT and DOL could be the same lib or different libs.
    " For one FUT there can be 0, 1, or many DOFs.
    " If there are 0 DOFs, the unit test is an ordinary unit test.
    " If there are 1 or more DOFs, it is a mockist test.
    "
" FUT and LUT are a point-of-view
    " All project libs require dev! Which dev lib is the LUT and which are the
    " DOLs depends on the perspective of the lib being developed. Not all libs
    " are depended on and not all libs require mocking, so rather than create
    " behemoth test code files full of unused boiler plate, the project test
    " code is created as-needed.
"=====[ Setup ]=====
" the mocks assume a project structure
    " proj/tags
    " proj/test/test_runner.c
    " proj/src/LUT.c
    " proj/src/LUT.h
    " proj/src/DOL.c
    " proj/src/DOL.h
    " proj/test/test_LUT.c
    " proj/test/test_LUT.h
    " proj/test/mock_DOL.c
    " proj/test/mock_DOL.h
" Setup() creates this project structure and fills it with code from
" Teardown() deletes the project. If the project folder is not empty, the user
" is asked for permission. I can set an 'ignore' list to delete without asking
" permission. If the project folder is in the `expectmocking` folder, do not ask
" the user for permission.
" See hardcoded list of ignore folders in fakeproject#DestroyFolders.

"=====[ List of tests of lib `expectmocking` ]=====
    "[x] ThisIsATestFile returns true if current file is a test file.
    "[x] ThisIsATestFile is false if current file is a src file.
    "[x] NameOfLut returns name of lib under test.
    "[x] NameOfDol returns name of depended on lib.
        "[x] NameOfDol uses macro on Expect line if it is there.
        "[x] NameOfDol uses include mock line if no macro on Expect line.
        "[x] NameOfDol fails by returning an empty string.
        "[x] NameOfDol fails if no macro and more than one include mock line.
        "[x] NameOfDol fails if no macro and no include mock lines.
    "[ ] 

let run_quickly = !fakeproject#Show_changing_in_Nerdtree()
let this_test_file = expand("%:p")
let save_dir = getcwd()

function! s:Setup_Testing_NameOfDol(Project)
    "Setup a fake project with all code and a tags file.
    "Place cursor on the Expect_ call line in the test_LUT.c
    "CD into the fake project directory.
    let Teardown = function('s:Teardown_AllTests', [a:Project])
    if !a:Project.Make(g:run_quickly)    | call Teardown() | return 0 | endif
    if !a:Project.UpdateTags()           | call Teardown() | return 0 | endif
    if !a:Project.GoToFile('test_LUT.c') | call Teardown() | return 0 | endif
    call search('Expect_')
    call a:Project.CdProjectDir()
    return 1
endfunction
function! s:Setup_TestsUsingFile_test_LUT(Project)
    "Setup a fake project. Prepare to run the test from file `test_LUT.c`.
    let Teardown = function('s:Teardown_AllTests', [a:Project])
    if !a:Project.Make(g:run_quickly)    | call Teardown() | return 0 | endif
    if !a:Project.GoToFile('test_LUT.c') | call Teardown() | return 0 | endif
    return 1
endfunction
function! s:Setup_TestsUsingFile_LUT(Project)
    "Setup a fake project. Prepare to run the test from file `LUT.c`.
    let Teardown = function('s:Teardown_AllTests', [a:Project])
    if !a:Project.Make(g:run_quickly)    | call Teardown() | return 0 | endif
    if !a:Project.GoToFile('LUT.c') | call Teardown() | return 0 | endif
    return 1
endfunction
function! s:Teardown_AllTests(Project)
    silent execute 'cd ' g:save_dir
    silent execute "noswapfile edit " . g:this_test_file
    silent call a:Project.Clean(g:run_quickly)
endfunction
"=====[ tests ]=====
function! ThisIsATestFile_is_true_if_current_file_is_a_test_file()
    let project = fakeproject#NewProject('project')
    if !s:Setup_TestsUsingFile_test_LUT(project) | return | endif
    call assert_true(expectmocking#ThisIsATestFile())
    call s:Teardown_AllTests(project)
endfunction
function! ThisIsATestFile_is_false_if_current_file_is_a_src_file()
    let project = fakeproject#NewProject('project')
    if !s:Setup_TestsUsingFile_LUT(project) | return | endif
    call assert_false(expectmocking#ThisIsATestFile())
    call s:Teardown_AllTests(project)
endfunction
function! NameOfLut_returns_name_of_lib_under_test()
    let project = fakeproject#NewProject('project')
    if !s:Setup_TestsUsingFile_test_LUT(project) | return | endif
    call assert_equal('LUT', expectmocking#NameOfLut())
    call s:Teardown_AllTests(project)
endfunction
function! NameOfDol_uses_macro_on_Expect_line_if_it_is_there()
    let project = fakeproject#NewProject('project')
    if !s:Setup_Testing_NameOfDol(project) | return | endif
    let macro = '_MOCK_DOL2_H'
    call assert_equal(macro, matchstr(getline('.'), macro) )
    call assert_equal('DOL2', expectmocking#NameOfDol())
    call s:Teardown_AllTests(project)
endfunction
function! NameOfDol_uses_include_mock_line_if_no_macro_on_Expect_line()
    let project = fakeproject#NewProject('project')
    if !s:Setup_Testing_NameOfDol(project) | return | endif
    "Eliminate the other #include mock:
    call search('#include "mock_DOL2.h"', 'w')
    execute 'delete'
    execute 'w'
    "Put cursor on second Expect call.
    call search('Expect_')
    call search('Expect_')
    call assert_equal('DOL', expectmocking#NameOfDol())
    call s:Teardown_AllTests(project)
endfunction
function! NameOfDol_fails_by_returning_an_empty_string()
    "Note this is identical to the next test. I'm just listing the specs.
    return 0
    let project = fakeproject#NewProject('project')
    if !s:Setup_Testing_NameOfDol(project) | return | endif
    "Go to the Expect_ line without the macro.
    call search('Expect_')
    call assert_equal('', expectmocking#NameOfDol())
    call s:Teardown_AllTests(project)
endfunction
function! NameOfDol_fails_if_no_macro_and_more_than_one_include_mock_line()
    let project = fakeproject#NewProject('project')
    if !s:Setup_Testing_NameOfDol(project) | return | endif
    "Go to the Expect_ line without the macro.
    call search('Expect_')
    call assert_equal('', expectmocking#NameOfDol())
    "if expectmocking#NameOfDol() != ''
    "    call assert_report('Expected error: "There is more than one #include mocked lib header."')
    "endif
    call s:Teardown_AllTests(project)
endfunction
function! NameOfDol_fails_if_no_macro_and_no_include_mock_lines()
    let project = fakeproject#NewProject('project')
    if !s:Setup_Testing_NameOfDol(project) | return | endif
    "Eliminate the #include mock lines:
    call search('#include "mock_DOL.h"', 'w')
    execute 'delete'
    call search('#include "mock_DOL2.h"', 'w')
    execute 'delete'
    execute 'w'
    "Go to the Expect_ line without the macro.
    call search('Expect_')
    call search('Expect_')
    call assert_equal('', expectmocking#NameOfDol())
    call s:Teardown_AllTests(project)
endfunction

"=====[ test runner ]=====
let tests = vimtdd#NewTestGroup()
call tests.RunTest('ThisIsATestFile_is_true_if_current_file_is_a_test_file')
call tests.RunTest('ThisIsATestFile_is_false_if_current_file_is_a_src_file')
call tests.RunTest('NameOfLut_returns_name_of_lib_under_test')
call tests.RunTest('NameOfDol_uses_macro_on_Expect_line_if_it_is_there')
call tests.RunTest('NameOfDol_uses_include_mock_line_if_no_macro_on_Expect_line')
call tests.RunTest('NameOfDol_fails_by_returning_an_empty_string')
call tests.RunTest('NameOfDol_fails_if_no_macro_and_more_than_one_include_mock_line')
call tests.RunTest('NameOfDol_fails_if_no_macro_and_no_include_mock_lines')
call tests.ViewTestResults()
call tests.SaveResultsToFile('./expectmocking/test-results.md')
unlet tests

" Full list of Vim assertions:
" assert_equal({expected}, {actual} [, {msg}])
" assert_equalfile({fname-one}, {fname-two})
" assert_exception({error} [, {msg}])
" assert_fails({cmd} [, {error}])
" assert_false({actual} [, {msg}])
" assert_inrange({lower}, {upper}, {actual} [, {msg}])
" assert_match({pattern}, {actual} [, {msg}])
" assert_notequal({expected}, {actual} [, {msg}])
" assert_notmatch({pattern}, {actual} [, {msg}])
" assert_report({msg})
" assert_true({actual} [, {msg}])

"        "Save current file and location to come back.
"        let save_file   = expand("%:p")
"        let save_cursor = getcurpos()
"        silent execute "noswapfile edit " . copy_from
"        let first_line = 1
"        let last_line = line('$')
"        let lines = getline(first_line, last_line)
"        silent execute "noswapfile edit " . copy_to
