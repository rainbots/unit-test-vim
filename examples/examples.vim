function! examples#ReturnsTrue()
    return 1
endfunction

function! examples#SingleLineIf()
    "The syntax must use pipes.
    "You cannot delete the pipe.
    if examples#ReturnsTrue() | return 1 | endif
    return 0
endfunction

function! examples#UseObj()
    let obj = obj#New() | echomsg obj.Kitty() . " This kitty has yet to follow a path."
    call obj.Happy()    | echomsg obj.Kitty() . " This kitty has taken the happy path."
    call obj.Sad()      | echomsg obj.Kitty() . " & this kitty has taken the sad path."
endfunction

function! s:Ternary(number)
    let five = '5'
    let dunno = 'unknown'
    "if a:number == 5
    "    let ans = 'number is ' . five
    "else
    "    let ans = 'number is ' . dunno
    "endif
    "Two ways of doing this:
    "let ans = a:number == 5 ? 'number is ' . five : 'number is ' . dunno
    let ans = 'number is ' . (a:number == 5 ? five : dunno)
    return ans
endfunction
"echo s:Ternary(5)
"echo s:Ternary(2)

function! s:QuickTest(list_has_undeclared_types)
    if a:list_has_undeclared_types
        let arg_list = ['one', 'two', 'three', 'undeclared', 'four']
    else
        let arg_list = ['one', 'two', 'three', 'four']
    endif
    let func_types = '('
    for arg in arg_list
        let type = arg
        let func_types = func_types . type
        if type ==# 'undeclared' | return func_types . ')' | endif
        let func_types = func_types . (arg == arg_list[-1] ? ')' : ', ')
    endfor
    return func_types
endfunction
"echo s:QuickTest(0)
"echo s:QuickTest(1)

function! s:MakeFakeProject()
    let project = fakeproject#NewProject('ex-project')
    call project.Make(1)
endfunction
call s:MakeFakeProject()
