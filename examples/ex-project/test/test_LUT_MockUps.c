#include "Lis.h"
#include "mock_Lis.h"
#include "Ft1248.h"
#include "mock_Ft1248.h"
#include "mock_DOL3.h"
#include <Mock.h>
#include "test_LUT_MockUps.h"
#include "DOL3.h"

static void (*DOF3_Saved)(uint16_t, uint8_t);
static void Stub_DOF3(void) {
    DOF3_Saved = DOF3;
    DOF3 = DOF3_Stubbed;
}
static void Unstub_DOF3(void) {
    DOF3 = DOF3_Saved;
}
static void (*DOF4_Saved)(void);
static void Stub_DOF4(void) {
    DOF4_Saved = DOF4;
    DOF4 = DOF4_Stubbed;
}
static void Unstub_DOF4(void) {
    DOF4 = DOF4_Saved;
}
static void (*DOF_ReturnsBool_Saved)(void);
static void Stub_DOF_ReturnsBool(void) {
    DOF_ReturnsBool_Saved = DOF_ReturnsBool;
    DOF_ReturnsBool = DOF_ReturnsBool_Stubbed;
}
static void Unstub_DOF_ReturnsBool(void) {
    DOF_ReturnsBool = DOF_ReturnsBool_Saved;
}
static void (*DOF2_ReturnsBool_Saved)(void);
static void Stub_DOF2_ReturnsBool(void) {
    DOF2_ReturnsBool_Saved = DOF2_ReturnsBool;
    DOF2_ReturnsBool = DOF2_ReturnsBool_Stubbed;
}
static void Unstub_DOF2_ReturnsBool(void) {
    DOF2_ReturnsBool = DOF2_ReturnsBool_Saved;
}
static bool (*DOF3_ReturnsBool_Saved)(void);
static void Stub_DOF3_ReturnsBool(void) {
    DOF3_ReturnsBool_Saved = DOF3_ReturnsBool;
    DOF3_ReturnsBool = DOF3_ReturnsBool_Stubbed;
}
static void Unstub_DOF3_ReturnsBool(void) {
    DOF3_ReturnsBool = DOF3_ReturnsBool_Saved;
}
static void (*DOF4_ReturnsBool_Saved)(void);
static void Stub_DOF4_ReturnsBool(void) {
    DOF4_ReturnsBool_Saved = DOF4_ReturnsBool;
    DOF4_ReturnsBool = DOF4_ReturnsBool_Stubbed;
}
static void Unstub_DOF4_ReturnsBool(void) {
    DOF4_ReturnsBool = DOF4_ReturnsBool_Saved;
}
static bool (*DOF5_ReturnsBool_Saved)(void);
static void Stub_DOF5_ReturnsBool(void) {
    DOF5_ReturnsBool_Saved = DOF5_ReturnsBool;
    DOF5_ReturnsBool = DOF5_ReturnsBool_Stubbed;
}
static void Unstub_DOF5_ReturnsBool(void) {
    DOF5_ReturnsBool = DOF5_ReturnsBool_Saved;
}
void SetUpMock_FUT(void)  // FUT
{
    mock = Mock_new();
    //
    Stub_DOF3();  // DOF
    Stub_DOF4();  // DOF
    Stub_DOF_ReturnsBool();  // DOF
    Stub_DOF2_ReturnsBool();  // DOF
    Stub_DOF3_ReturnsBool();  // DOF
    Stub_DOF4_ReturnsBool();  // DOF
    Stub_DOF5_ReturnsBool();  // DOF
}
void TearDownMock_FUT(void)  // FUT
{
    Mock_destroy(mock); mock = NULL;
    //
    Unstub_DOF3();  // DOF
    Unstub_DOF4();  // DOF
    Unstub_DOF_ReturnsBool();  // DOF
    Unstub_DOF2_ReturnsBool();  // DOF
    Unstub_DOF3_ReturnsBool();  // DOF
    Unstub_DOF4_ReturnsBool();  // DOF
    Unstub_DOF5_ReturnsBool();  // DOF
}

static void (*FtRead_Saved)(uint8_t *);
static void Stub_FtRead(void) {
    FtRead_Saved = FtRead;
    FtRead = FtRead_Stubbed;
}
static void Unstub_FtRead(void) {
    FtRead = FtRead_Saved;
}
static void (*DoThingWithGString_Saved)(GString *);
static void Stub_DoThingWithGString(void) {
    DoThingWithGString_Saved = DoThingWithGString;
    DoThingWithGString = DoThingWithGString_Stubbed;
}
static void Unstub_DoThingWithGString(void) {
    DoThingWithGString = DoThingWithGString_Saved;
}
void SetUpMock_UsbRead(void)  // FUT
{
    mock = Mock_new();
    //
    Stub_FtRead();  // DOF
    Stub_DoThingWithGString();  // DOF
}
void TearDownMock_UsbRead(void)  // FUT
{
    Mock_destroy(mock); mock = NULL;
    //
    Unstub_FtRead();  // DOF
    Unstub_DoThingWithGString();  // DOF
}

static void (*LoadNextCfgBit_Saved)(uint32_t);
static void Stub_LoadNextCfgBit(void) {
    LoadNextCfgBit_Saved = LoadNextCfgBit;
    LoadNextCfgBit = LoadNextCfgBit_Stubbed;
}
static void Unstub_LoadNextCfgBit(void) {
    LoadNextCfgBit = LoadNextCfgBit_Saved;
}
void SetUpMock_LisWriteCfg(void)  // FUT
{
    mock = Mock_new();
    //
    Stub_LoadNextCfgBit();  // DOF
}
void TearDownMock_LisWriteCfg(void)  // FUT
{
    Mock_destroy(mock); mock = NULL;
    //
    Unstub_LoadNextCfgBit();  // DOF
}

