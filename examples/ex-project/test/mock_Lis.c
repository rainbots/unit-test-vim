#include <Mock.h>
#include <RecordedCall.h>
#include <RecordedArg.h>
#include "mock_Lis.h"

static RecordedCall * Mock_LoadNextCfgBit(uint32_t arg1)
{
    char const *call_name = "LoadNextCfgBit";
    RecordedCall *record_of_this_call = RecordedCall_new(call_name);
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_uint32_t);
    *((uint32_t*)record_of_arg1->pArg) = arg1;
    RecordArg(record_of_this_call, record_of_arg1);
    return record_of_this_call;
}
void Expect_LoadNextCfgBit(uint32_t arg1) {
    RecordExpectedCall(mock, Mock_LoadNextCfgBit(arg1));
}
void LoadNextCfgBit_Stubbed(uint32_t arg1) {
    RecordActualCall(mock, Mock_LoadNextCfgBit(arg1));
}
