#ifndef _MOCK_DOL2_H
#define _MOCK_DOL2_H

#include <stdint.h>   // uint8_t

void Expect_DOF2(uint8_t arg1);
void DOF2_Stubbed(uint8_t arg1);
void Expect_DOF_ReturnsBool(void);
void DOF_ReturnsBool_Stubbed(void);
void Expect_DOF2_ReturnsBool(void);
void DOF2_ReturnsBool_Stubbed(void);
void Expect_DOF3_ReturnsBool(void);
void DOF3_ReturnsBool_Stubbed(void);
void Expect_DOF4_ReturnsBool(void);
void DOF4_ReturnsBool_Stubbed(void);
void Expect_DOF5_ReturnsBool(void);
extern bool ReturnsBool_StubbedReturnValue;
bool DOF5_ReturnsBool_Stubbed(void);

#endif // _MOCK_DOL2_H
