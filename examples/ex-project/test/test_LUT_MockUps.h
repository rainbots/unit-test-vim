#ifndef _TEST_LUT_MOCKUPS_H
#define _TEST_LUT_MOCKUPS_H

void SetUpMock_FUT(void);
void TearDownMock_FUT(void);
void SetUpMock_UsbRead(void);
void TearDownMock_UsbRead(void);
void SetUpMock_LisWriteCfg(void);
void TearDownMock_LisWriteCfg(void);

#endif // _TEST_LUT_MOCKUPS_H
