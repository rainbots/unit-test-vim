#include "mock_DOL.h"
#include <Mock.h>           // RecordExpectedCall, RecordActualCall
#include <RecordedCall.h>   // Class: RecordedCall
#include <RecordedArg.h>    // Class: RecordedArg

static RecordedCall * Mock_DOF(uint8_t arg1)
{
    char const *call_name = "DOF";
    RecordedCall *record_of_this_call = RecordedCall_new(call_name);
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_uint8);
    *((uint8_t *)record_of_arg1->pArg) = arg1;
    RecordArg(record_of_this_call, record_of_arg1);
    return record_of_this_call;
}
void Expect_DOF(uint8_t arg1) {
    RecordExpectedCall(mock, Mock_DOF(arg1));
}
void DOF_Stubbed(uint8_t arg1) {
    RecordActualCall(mock, Mock_DOF(arg1));
}


