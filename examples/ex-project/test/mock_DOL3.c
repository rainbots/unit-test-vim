#include "mock_DOL3.h"
#include <Mock.h>
#include <RecordedCall.h>
#include <RecordedArg.h>
void x(void)

static RecordedCall * Mock_DOF3(uint16_t arg1, uint8_t arg2)
{
    char const *call_name = "DOF3";
    RecordedCall *record_of_this_call = RecordedCall_new(call_name);
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_uint16_t);
    *((uint16_t *)record_of_arg1->pArg) = arg1;
    RecordArg(record_of_this_call, record_of_arg1);
    RecordedArg *record_of_arg2 = RecordedArg_new(SetupRecord_uint8_t);
    *((uint8_t *)record_of_arg2->pArg) = arg2;
    RecordArg(record_of_this_call, record_of_arg2);
    return record_of_this_call;
}
void Expect_DOF3(uint16_t arg1, uint8_t arg2) {
    RecordExpectedCall(mock, Mock_DOF3(arg1, arg2));
}
void DOF3_Stubbed(uint16_t arg1, uint8_t arg2) {
    RecordActualCall(mock, Mock_DOF3(arg1, arg2));
}

static RecordedCall * Mock_DOF4(void)
{
    char const *call_name = "DOF4";
    RecordedCall *record_of_this_call = RecordedCall_new(call_name);
    return record_of_this_call;
}
void Expect_DOF4(void) {
    RecordExpectedCall(mock, Mock_DOF4());
}
void DOF4_Stubbed(void) {
    RecordActualCall(mock, Mock_DOF4());
}

static RecordedCall * Mock_DoThingWithGString(GString* arg1)
{
    char const *call_name = "DoThingWithGString";
    RecordedCall *record_of_this_call = RecordedCall_new(call_name);
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_p_GString);
    *((GString**)record_of_arg1->pArg) = arg1;
    RecordArg(record_of_this_call, record_of_arg1);
    return record_of_this_call;
}
void Expect_DoThingWithGString(GString* arg1) {
    RecordExpectedCall(mock, Mock_DoThingWithGString(arg1));
}
void DoThingWithGString_Stubbed(GString* arg1) {
    RecordActualCall(mock, Mock_DoThingWithGString(arg1));
}
