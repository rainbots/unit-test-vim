#include "mock_Lis.h"
#include "DOL2.h"
#include "Ft1248.h"
#include "mock_Ft1248.h"
#include "Lis.h"
#include "DOL3.h"
#include "mock_DOL3.h"
#include "test_LUT.h"           // header for this file
#include "test_LUT_MockUps.h"   // mockups for testing each FUT in FUT lib
#include <LUT.h>                // FUT lib
#include <DOL.h>                // DOF lib
#include <DOL2.h>               // DOF2 lib
#include "mock_DOL.h"           // mocked version of DOF lib
#include "mock_DOL2.h"          // mocked version of DOF2 lib
#include <Mock.h>               // RanAsHoped, WhyDidItFail
#include <unity.h>              // all the TEST_blah macros

void SetUp_FUT(void){
    SetUpMock_FUT();    // create the mock object to record calls
    // other setup code
}
void TearDown_FUT(void){
    TearDownMock_FUT();    // destroy the mock object
    // other teardown code
}
void FUT_Read_does_entire_command_phase_for_ReadCmd(void)
{
    //Example of what to do if there is more than one mocked lib:
    _MOCK_DOL2_H; Expect_DOF_ReturnsBool();
    _MOCK_DOL2_H; Expect_DOF2_ReturnsBool();
    _MOCK_DOL2_H; Expect_DOF3_ReturnsBool();
    _MOCK_DOL2_H; Expect_DOF4_ReturnsBool();
    _MOCK_DOL2_H; Expect_DOF5_ReturnsBool();
    _MOCK_DOL2_H; Expect_DOF2(FtCmd_Read);
    _MOCK_DOL2_H; Expect_BOF2(FtCmd_Read);
    _MOCK_DOL2_H; Expect_BOF3(FtCmd_Read);
    _MOCK_DOL2_H; Expect_BOF0();
    uint16_t some_local = 0xABCD;
    _MOCK_DOL3_H; Expect_DOF3(some_local, FtCmd_Read2);
    _MOCK_DOL3_H; Expect_DOF9(some_local, FtCmd_Read2);
    _MOCK_DOL3_H; Expect_DOF4();
    _MOCK_DOL3_H; Expect_DOF5(some_local);
    _MOCK_DOL3_H; Expect_DOF6();
    _MOCK_DOL3_H; Expect_DOF3(some_local, 0xABCD);
    _MOCK_DOL3_H; Expect_DOF3(some_local, bob);
    _MOCK_DOL4_H; Expect_DOF4(some_local, FtCmd_Read2);
    Expect_DOF(FtCmd_Read);
    //=====[ Operate ]=====
    FUT(FtCmd_Read);
    TEST_ASSERT_TRUE_MESSAGE(
        RanAsHoped(mock),           // If this is false,
        WhyDidItFail(mock)          // print this message.
        );
}
void SetUp_UsbRead(void){
    SetUpMock_UsbRead();    // create the mock object to record calls
    // other setup code
}
void TearDown_UsbRead(void){
    TearDownMock_UsbRead();    // destroy the mock object
    // other teardown code
}
void UsbRead_should_read_until_buffer_is_empty(void)
{
    //=====[ Mock-up values returned by stubbed functions ]=====
    FtBusTurnaround_StubbedReturnValue = true;
    FtRead_StubbedReturnValue = false;
    // mock-up one value for MIOSIO port to return
    //=====[ Set expectations ]=====
    Expect_FtSendCommand(FtCmd_Read);
    Expect_FtBusTurnaround();
    uint8_t read_buffer = 0x00;
    uint8_t *read_buffer_address = &read_buffer;
    _MOCK_FT1248_H; Expect_FtRead(read_buffer_address);
    GString *pGS1 = g_string_new("what about GStrings -- how does ;me look?");
    _MOCK_DOL3_H; Expect_DoThingWithGString(pGS1);
    Expect_FtDeactivateInterface();
    //=====[ Operate ]=====
    UsbRead();
    //=====[ Test ]=====
    TEST_ASSERT_TRUE_MESSAGE(
        RanAsHoped(mock),           // If this is false,
        WhyDidItFail(mock)          // print this message.
        );
}
void SetUp_LisWriteCfg(void){
    SetUpMock_LisWriteCfg();    // create the mock object to record calls
    // other setup code
}
void TearDown_LisWriteCfg(void){
    TearDownMock_LisWriteCfg();    // destroy the mock object
    // other teardown code
}
void LisWriteCfg_outputs_cfg_bits_on_Lis_Rst_pin(void) // 4 bytes: 28 bits
{
    /* =====[ Setup ]===== */
    *Lis_port1 = 0x00; // fake port with Lis_Rst pin
    uint32_t cfg = 0xFFFFFFFF; // cfg to write to Lis
    /* =====[ Test ]===== */
    /* This only tests the first last bit is written correctly. */
    /* TEST_ASSERT_BIT_HIGH(Lis_Rst, *Lis_ddr1); */
    /* Shoot, no, I can't even test that. */
    /* I have to mock this if I want to be sure it's working correclty. */
    //=====[ Set expectations ]=====
    _MOCK_LIS_H; Expect_LoadNextCfgBit(cfg);
    /* =====[ Operate ]===== */
    LisWriteCfg(cfg);
    TEST_ASSERT_TRUE_MESSAGE(
        RanAsHoped(mock),           // If this is false,
        WhyDidItFail(mock)          // print this message.
        );
}
