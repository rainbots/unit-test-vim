#include "mock_DOL2.h"
#include <Mock.h>           // RecordExpectedCall, RecordActualCall
#include <RecordedCall.h>   // Class: RecordedCall
#include <RecordedArg.h>    // Class: RecordedArg

static RecordedCall * Mock_DOF2(uint8_t arg1)
{
    char const *call_name = "DOF2";
    RecordedCall *record_of_this_call = RecordedCall_new(call_name);
    RecordedArg *record_of_arg1 = RecordedArg_new(SetupRecord_uint8);
    *((uint8_t *)record_of_arg1->pArg) = arg1;
    RecordArg(record_of_this_call, record_of_arg1);
    return record_of_this_call;
}
void Expect_DOF2(uint8_t arg1) {
    RecordExpectedCall(mock, Mock_DOF2(arg1));
}
void DOF2_Stubbed(uint8_t arg1) {
    RecordActualCall(mock, Mock_DOF2(arg1));
}


static RecordedCall * Mock_DOF_ReturnsBool(void)
{
    char const *call_name = "DOF_ReturnsBool";
    RecordedCall *record_of_this_call = RecordedCall_new(call_name);
    return record_of_this_call;
}
void Expect_DOF_ReturnsBool(void) {
    RecordExpectedCall(mock, Mock_DOF_ReturnsBool());
}
void DOF_ReturnsBool_Stubbed(void) {
    RecordActualCall(mock, Mock_DOF_ReturnsBool());
}

static RecordedCall * Mock_DOF2_ReturnsBool(void)
{
    char const *call_name = "DOF2_ReturnsBool";
    RecordedCall *record_of_this_call = RecordedCall_new(call_name);
    return record_of_this_call;
}
void Expect_DOF2_ReturnsBool(void) {
    RecordExpectedCall(mock, Mock_DOF2_ReturnsBool());
}
void DOF2_ReturnsBool_Stubbed(void) {
    RecordActualCall(mock, Mock_DOF2_ReturnsBool());
}

static RecordedCall * Mock_DOF3_ReturnsBool(void)
{
    char const *call_name = "DOF3_ReturnsBool";
    RecordedCall *record_of_this_call = RecordedCall_new(call_name);
    return record_of_this_call;
}
void Expect_DOF3_ReturnsBool(void) {
    RecordExpectedCall(mock, Mock_DOF3_ReturnsBool());
}
bool DOF3_ReturnsBool_Stubbed(void) {
    RecordActualCall(mock, Mock_DOF3_ReturnsBool());
}

static RecordedCall * Mock_DOF4_ReturnsBool(void)
{
    char const *call_name = "DOF4_ReturnsBool";
    RecordedCall *record_of_this_call = RecordedCall_new(call_name);
    return record_of_this_call;
}
void Expect_DOF4_ReturnsBool(void) {
    RecordExpectedCall(mock, Mock_DOF4_ReturnsBool());
}
void DOF4_ReturnsBool_Stubbed(void) {
    RecordActualCall(mock, Mock_DOF4_ReturnsBool());
}

static RecordedCall * Mock_DOF5_ReturnsBool(void)
{
    char const *call_name = "DOF5_ReturnsBool";
    RecordedCall *record_of_this_call = RecordedCall_new(call_name);
    return record_of_this_call;
}
void Expect_DOF5_ReturnsBool(void) {
    RecordExpectedCall(mock, Mock_DOF5_ReturnsBool());
}
bool ReturnsBool_StubbedReturnValue = false;
bool DOF5_ReturnsBool_Stubbed(void) {
    RecordActualCall(mock, Mock_DOF5_ReturnsBool());
    return ReturnsBool_StubbedReturnValue;
}
