#ifndef _MOCK_DOL3_H
#define _MOCK_DOL3_H

void x(void);
void Expect_DOF3(uint16_t arg1, uint8_t arg2);
void DOF3_Stubbed(uint16_t arg1, uint8_t arg2);
void Expect_DOF4(void);
void DOF4_Stubbed(void);
void Expect_DoThingWithGString(GString* arg1);
void DoThingWithGString_Stubbed(GString* arg1);

#endif // _MOCK_DOL3_H
