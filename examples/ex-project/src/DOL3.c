#include "DOL3.h"
void dol3x(void)

static void DOF3_Implementation(uint16_t, uint8_t)
{}

void (*DOF3)(uint16_t, uint8_t) = DOF3_Implementation;

static void DOF4_Implementation(void)
{}

void (*DOF4)(void) = DOF4_Implementation;

static void DoThingWithGString_Implementation(GString *)
{}

void (*DoThingWithGString)(GString *) = DoThingWithGString_Implementation;
