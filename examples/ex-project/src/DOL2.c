#include "DOL2.h"

uint8_t const FtCmd_Read2  = 0xC6;

//static void DOF2_Implementation(uint8_t FtCmd)
//{}
//void (*DOF2)(uint8_t FtCmd) = DOF2_Implementation;

static void DOF2_Implementation(uint8_t)
{}
void (*DOF2)(uint8_t) = DOF2_Implementation;

static void BOF2_Implementation(uint8_t)
{}
void (*BOF2)(uint8_t) = BOF2_Implementation;

static void BOF3_Implementation(uint8_t)
{}
void (*BOF3)(uint8_t) = BOF3_Implementation;

static void DOF_ReturnsBool_Implementation(void)
{}

void (*DOF_ReturnsBool)(void) = DOF_ReturnsBool_Implementation;

static void DOF2_ReturnsBool_Implementation(void)
{}

void (*DOF2_ReturnsBool)(void) = DOF2_ReturnsBool_Implementation;

static void DOF3_ReturnsBool_Implementation(void)
{}

void (*DOF3_ReturnsBool)(void) = DOF3_ReturnsBool_Implementation;

static void DOF4_ReturnsBool_Implementation(void)
{}

void (*DOF4_ReturnsBool)(void) = DOF4_ReturnsBool_Implementation;

static void DOF5_ReturnsBool_Implementation(void)
{}

void (*DOF5_ReturnsBool)(void) = DOF5_ReturnsBool_Implementation;
