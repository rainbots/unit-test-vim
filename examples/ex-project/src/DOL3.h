#ifndef _DOL3_H
#define _DOL3_H

void dol3x(void);
extern void (*DOF3)(uint16_t, uint8_t);
extern void (*DOF4)(void);
extern void (*DoThingWithGString)(GString *);

#endif // _DOL3_H
