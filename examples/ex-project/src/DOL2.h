#ifndef _DOL2_H
#define _DOL2_H

extern uint8_t const FtCmd_Read2;
extern void (*DOF2)(uint8_t FtCmd);
extern void (*BOF2)(uint8_t);
extern void (*BOF3)(uint8_t);
extern void (*DOF_ReturnsBool)(void);
extern void (*DOF2_ReturnsBool)(void);
extern void (*DOF3_ReturnsBool)(void);
extern void (*DOF4_ReturnsBool)(void);
extern void (*DOF5_ReturnsBool)(void);

#endif // _DOL2_H
