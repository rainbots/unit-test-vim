# Test group: No *Setup* or *Teardown*

- *Test #1*:`ReturnsTrue_returns_true` **PASS**
- *Test #2*:`ReturnsTrue_returns_false` **FAIL**: Expected False but got 1.
    - *Traceback*: `..ReturnsTrue_returns_false line 1`

------------------
2 Tests 1 Failures
# Test group: A passing *Setup* and passing *Teardown*

- *Test #1*:`ReturnsTrue_returns_true` **PASS**
- *Test #2*:`ReturnsTrue_returns_false` **FAIL**: Expected False but got 1.
    - *Traceback*: `..ReturnsTrue_returns_false line 1`

------------------
2 Tests 1 Failures
# Test group: A failing *Setup* and passing *Teardown*

- **Setup failed** for *Test #1*:`ReturnsTrue_returns_true`: Expected True but got 0.
    - *Traceback*: `..<SNR>79_Setup_PickObjectMood_FAIL[5]..obj#DoHardThing line 2`
- **Setup failed** for *Test #2*:`ReturnsTrue_returns_false`: Expected True but got 0.
    - *Traceback*: `..<SNR>79_Setup_PickObjectMood_FAIL[5]..obj#DoHardThing line 2`

------------------
2 Tests 2 Failures
# Test group: A passing *Setup* and failing *Teardown*

- **Teardown failed** for *Test #1*:`ReturnsTrue_returns_true`: Expected True but got 0.
    - *Traceback*: `..<SNR>79_Teardown_ShowObjectMood_FAIL[2]..obj#DoHardThing line 2`
- *Test #2*:`ReturnsTrue_returns_false` **FAIL**: Expected False but got 1.
    - *Traceback*: `..ReturnsTrue_returns_false line 1`
- **Teardown failed** for *Test #2*:`ReturnsTrue_returns_false`: Expected True but got 0.
    - *Traceback*: `..<SNR>79_Teardown_ShowObjectMood_FAIL[2]..obj#DoHardThing line 2`

------------------
2 Tests 2 Failures
# Test group: A failing *Setup* and failing *Teardown*

- **Setup failed** for *Test #1*:`ReturnsTrue_returns_true`: Expected True but got 0.
    - *Traceback*: `..<SNR>79_Setup_PickObjectMood_FAIL[5]..obj#DoHardThing line 2`
- **Teardown failed** for *Test #1*:`ReturnsTrue_returns_true`: Expected True but got 0.
    - *Traceback*: `..<SNR>79_Teardown_ShowObjectMood_FAIL[2]..obj#DoHardThing line 2`
- **Setup failed** for *Test #2*:`ReturnsTrue_returns_false`: Expected True but got 0.
    - *Traceback*: `..<SNR>79_Setup_PickObjectMood_FAIL[5]..obj#DoHardThing line 2`
- **Teardown failed** for *Test #2*:`ReturnsTrue_returns_false`: Expected True but got 0.
    - *Traceback*: `..<SNR>79_Teardown_ShowObjectMood_FAIL[2]..obj#DoHardThing line 2`

------------------
2 Tests 2 Failures
