execute 'source examples/examples.vim'
execute 'source autoload/obj.vim'
execute 'source autoload/testgroup.vim'

let this_test_file = expand("%:p")

"=====[ tests ]=====
function! ReturnsTrue_returns_true()
    call assert_true(examples#ReturnsTrue())
    echo "(Just ran test: ReturnsTrue_returns_true)"
endfunction
function! ReturnsTrue_returns_false()
    call assert_false(examples#ReturnsTrue())
    echo "(Just ran test: ReturnsTrue_returns_false)"
endfunction
function! SingleLineIf_syntax_is_valid_in_Vim()
    call assert_true(examples#SingleLineIf())
endfunction
function! Kitty_returns_ascii_art_kitty()
    let obj = obj#New()
    " assert_match rabbit hole:
        " See :h pattern-overview:
            " > To include a literal ']', '^', '-' or '\' in the collection,
            " > put a backslash before it:
            " > "[xyz\]]", "[\^xyz]", "[xy\-z]" and "[xyz\\]".
        " Note the \M and single quotes in the searches!
        " And in the original obj.vim double quotes and '\' are used.
        " assert_match() is following regex rules,
        " but echomsg() just echoes what is in quotes where:
        "   single quotes are printed literally: '\n' is \n
        "   double quotes allow escaping characters "\n" is a line break
        "call assert_match('\Mv.v', obj.Kitty()) " default Kitty art
        "call obj.Happy() | call assert_match('\M>^.^<', obj.Kitty())
        "call obj.Sad()   | call assert_match('\M<*.*>', obj.Kitty())
        "No do not use match! It passes a partial match. Use equal.
    call assert_equal(' v.v ', obj.Kitty()) " default Kitty art
    call obj.Happy() | call assert_equal('>^.^<', obj.Kitty())
    call obj.Sad()   | call assert_equal('<*.*>', obj.Kitty())
endfunction

"=====[ test runner ]=====
"let tests = vimtdd#NewTestGroup()
"call tests.RunTest('ReturnsTrue_returns_true')
"call tests.RunTest('SingleLineIf_syntax_is_valid_in_Vim')
"call tests.RunTest('Kitty_returns_ascii_art_kitty')
"call tests.ViewTestResults()
"unlet tests

function! s:Setup_PickObjectMood_PASS(mood) dict
    let self.obj = obj#New() | echo "   Setup: " self.obj.Kitty()
    "Setup something to show Teardown works with the same instance of obj.
    if a:mood ==# 'happy'    | call self.obj.Happy() | endif
    if a:mood ==# 'sad'      | call self.obj.Sad()   | endif
    if !self.obj.DoEasyThing() | return 0 | endif
    return 1
endfunction
function! s:Setup_PickObjectMood_FAIL(mood) dict
    let self.obj = obj#New() | echo "   Setup: " self.obj.Kitty()
    "Setup something to show Teardown works with the same instance of obj.
    if a:mood ==# 'happy'    | call self.obj.Happy() | endif
    if a:mood ==# 'sad'      | call self.obj.Sad()   | endif
    if !self.obj.DoHardThing() | return 0 | endif
    return 1
endfunction
function! s:Teardown_ShowObjectMood_PASS() dict
    echo "Teardown: " self.obj.Kitty()
    if !self.obj.DoEasyThing() | return 0 | endif
    return 1
endfunction
function! s:Teardown_ShowObjectMood_FAIL() dict
    echo "Teardown: " self.obj.Kitty()
    if !self.obj.DoHardThing() | return 0 | endif
    return 1
endfunction

"=====[ test runner ]=====
"Clear out test results file
let g:results_save_path = fnamemodify(g:this_test_file, ":h").'/test-results.md'
call writefile([], g:results_save_path)
let tests = testgroup#New('No *Setup* or *Teardown*')
" with a passing test
call tests.RunTest('ReturnsTrue_returns_true')
" with a failing test
call tests.RunTest('ReturnsTrue_returns_false')
" Expect the tests run fine. 2 tests, 1 fails.
call tests.ViewTestResults(g:results_save_path)
unlet tests

let tests = testgroup#New('A passing *Setup* and passing *Teardown*')
let tests.Setup = function('s:Setup_PickObjectMood_PASS', ['happy'])
let tests.Teardown = function('s:Teardown_ShowObjectMood_PASS')
" with a passing test
call tests.RunTest('ReturnsTrue_returns_true')
" with a failing test
call tests.RunTest('ReturnsTrue_returns_false')
" Expect the tests run fine. 2 tests, 1 fails. Just like before.
call tests.ViewTestResults(g:results_save_path)
unlet tests

let tests = testgroup#New('A failing *Setup* and passing *Teardown*')
let tests.Setup = function('s:Setup_PickObjectMood_FAIL', ['sad'])
let tests.Teardown = function('s:Teardown_ShowObjectMood_PASS')
" with a passing test
call tests.RunTest('ReturnsTrue_returns_true')
" with a failing test
call tests.RunTest('ReturnsTrue_returns_false')
" Expect the tests do not run. Both fail at Setup.
call tests.ViewTestResults(g:results_save_path)
unlet tests

let tests = testgroup#New('A passing *Setup* and failing *Teardown*')
let tests.Setup = function('s:Setup_PickObjectMood_PASS', ['happy'])
let tests.Teardown = function('s:Teardown_ShowObjectMood_FAIL')
" with a passing test
call tests.RunTest('ReturnsTrue_returns_true')
" with a failing test
call tests.RunTest('ReturnsTrue_returns_false')
" Expect both tests fail at teardown and 2nd test also fails on its own.
call tests.ViewTestResults(g:results_save_path)
unlet tests

let tests = testgroup#New('A failing *Setup* and failing *Teardown*')
let tests.Setup = function('s:Setup_PickObjectMood_FAIL', ['sad'])
let tests.Teardown = function('s:Teardown_ShowObjectMood_FAIL')
" with a passing test
call tests.RunTest('ReturnsTrue_returns_true')
" with a failing test
call tests.RunTest('ReturnsTrue_returns_false')
" Expect both tests fail at setup and at teardown. Neither test runs.
call tests.ViewTestResults(g:results_save_path)
unlet tests

" Full list of Vim assertions:
" assert_equal({expected}, {actual} [, {msg}])
" assert_equalfile({fname-one}, {fname-two})
" assert_exception({error} [, {msg}])
" assert_fails({cmd} [, {error}])
" assert_false({actual} [, {msg}])
" assert_inrange({lower}, {upper}, {actual} [, {msg}])
" assert_match({pattern}, {actual} [, {msg}])
" assert_notequal({expected}, {actual} [, {msg}])
" assert_notmatch({pattern}, {actual} [, {msg}])
" assert_report({msg})
" assert_true({actual} [, {msg}])
