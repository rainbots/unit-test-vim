" Generates the mock boilerplate code for an expected C call in a mockist test.
"
" Dependencies: 
"   file.vim

"echomsg ">^.^< expectmocking was sourced"
let s:happy_kitty = ">\^.\^<"
let s:sad_kitty   = "<\*.\*>"
let s:waiting_kitty   = "-O.O-"
let s:default_kitty   = " v.v "
let s:invisible_kitty = "     "
function! expectmocking#FuncDefFilepath(func_name)
    "Return the relative file path to the definition of func_name.
    "func_name is a string with the tag name of the function.
    "Return an empty string if file path is not found.
    "If you just want to know if func_name has a definition, call IsDefined().
    "Example:
    "calling: `echo expectmocking#FuncDefFilepath('SetUpMock_FUT')`
    "returns: `test/test_LUT_MockUps.c`
    "
    let this_file = expand("%") | let save_cursor = getcurpos()
    execute "normal! l" | let check_cursor = getcurpos()
    " Jump to the source file with the function definition
    silent! execute 'tag ' . a:func_name
    let found_func_name_definition = getcurpos() != check_cursor
    if  found_func_name_definition
        let filepath = expand("%")
        if this_file != filepath | silent execute "noswapfile buffer #" | endif
        call setpos('.', save_cursor)
        return filepath
    endif
    return ''
endfunction
"echo expectmocking#FuncDefFilepath('SetUpMock_FUT')
function! expectmocking#ThisFileHasDefinition(func_name)
    "Return 1 if this file has the definition of func_name.
    "func_name is a string with the tag name of the function.
    "This is for seeing if a function is defined yet in this file.
    "Use this when you know you are in the file where the function should be
    "defined.
    "If you want to know:
    "- if the function is defined in the project,
    "  call IsDefined()
    "- what file has the function,
    "  call FuncDefFilepath()
    let this_file = expand("%")
    let  def_file = expectmocking#FuncDefFilepath(a:func_name)
    if (this_file != def_file) | return 0 | endif
    return 1
endfunction
"echo expectmocking#ThisFileHasDefinition('SetUpMock_FUT')
function! expectmocking#GoToLineOfDef(func_name)
    "Move the cursor to the line of the function definition.
    "The func_tag definition must be in the current file.
    "This function guards against jumping to a new file.
    "If 0 is returned, cursor did not move because def is not in this file.
    "If 1 is returned, the cursor moves to func_tag's definition.
    "
    if !expectmocking#ThisFileHasDefinition(a:func_name) | return 0 | endif
    silent! execute 'tag ' . a:func_name
    return 1
endfunction
function! expectmocking#GoToFirstLineOfDef(func_name)
    "Move the cursor to the first line of the function definition.
    "The func_tag definition must be in the current file.
    "This function guards against jumping to a new file.
    "If 0 is returned, the cursor did not move.
    "If 1 is returned, the cursor moves to func_tag definition's opening '{'.
    "
    if !expectmocking#ThisFileHasDefinition(a:func_name) | return 0 | endif
    silent! execute 'tag ' . a:func_name
    execute "normal! j"
    return 1
endfunction
function! expectmocking#LastLineNumOfFuncDef(func_name, filepath)
    "Return line number of last line of function definition func_name in filepath.
    let this_file = expand("%") | let save_cursor = getcurpos()
    silent execute "noswapfile edit " . a:filepath
    if !expectmocking#GoToFirstLineOfDef(a:func_name)
        let line_num = 0
    else
        execute 'normal! %'
        let line_num = line('.')
    endif
    silent execute "noswapfile edit " . this_file
    call setpos('.', save_cursor)
    return line_num
endfunction
function! expectmocking#LineNumOfFuncDef(func_name, filepath)
    "Return the line number of the function definition func_name in filepath.
    let this_file = expand("%") | let save_cursor = getcurpos()
    silent execute "noswapfile edit " . a:filepath
    if !expectmocking#GoToLineOfDef(a:func_name)
        let line_num = 0
    else
        let line_num = line('.')
    endif
    silent execute "noswapfile edit " . this_file
    call setpos('.', save_cursor)
    return line_num
endfunction
"echo expectmocking#GoToFirstLineOfDef('SetUpMock_FUT')
function! expectmocking#StringIsInFuncBlock(string, func_tag)
    "Search for string in the function block for func_tag.
    "Return 1 if found, 0 if not.
    "Either way, the cursor does not move.
    "This function guards against jumping to a new file,
    "So this is safe:
    "   if !`expectmocking#StringIsInFuncBlock(
    "      'Stub_DOF3',
    "      'SetUpMock_FUT'
    "      )`
    "      " Do something, like add the function call to this definition.
    "   endif
    "But the search only works if the func_tag definition is in the current file.
    "Call `expectmocking#ThisFileHasDefinition(fname)` to check this.
    "Example:
    "if expectmocking#ThisFileHasDefinition('SetUpMock_FUT')
    "   if !expectmocking#StringIsInFuncBlock(
    "      'Stub_DOF3',
    "      'SetUpMock_FUT'
    "      )
    "      " Do something, like add the function call to this definition.
    "   endif
    "endif
    "returns 1 if 'Stub_DOF3' appears in the definition of 'SetUpMock_FUT'
    "returns 0 if 'Stub_DOF3' is not in the definition
    "returns 0 if 'SetUpMock_FUT' is not defined in the current file
    let save_cursor = getcurpos()
    if !expectmocking#GoToFirstLineOfDef(a:func_tag) | return 0 | endif
    let start = line('.')
    execute 'normal! %'
    let end = line('.')
    execute 'normal! %'
    if !search('\M'.a:string, 'n', end) | return 0 | endif
    "If the string is found, return 1.
    return 1
endfunction
"echo expectmocking#StringIsInFuncBlock('Stub_DOF3','SetUpMock_FUT')

function! expectmocking#CallFixtureMock(name_fixt, name_fixt_mock, code_to_add)
    "Add `code_to_add` to fixture `name_fixt`.
    "If `name_fixt` already calls `name_fixt_mock` do nothing.
    "Return 1 in either case.
    "Return 0 and do nothing if `name_fixt` is not defined.
    "Example:
    "void SetUp_FUT(void){
    "    SetUpMock_FUT();    // create the mock object to record calls
    "    // other setup code
    "}
    "name_fixt = 'SetUp_FUT', name_fixt_mock = 'SetUpMock_FUT'
    "code_to_add is the two lines of code.
    "'SetUp_FUT' already calls 'SetUpMock_FUT' so do nothing.
    "Dumb:
    "Only checks if name_fixt_mock is the first line of code.
    "It will add the code if it does not see it on the first line.
    "If the first line is '{' it is smart enough to look on the next line.
    "
    let save_cursor = getcurpos()
    silent execute 'tag ' . a:name_fixt
    if save_cursor == getcurpos()
        echomsg s:sad_kitty '`' . a:name_fixt . '` does not exist.'
        return 0
    endif
    let lnum = line('.')
    let next = lnum+1
    "let next_line = line('.')+1
    " Ignore '{' on its own line.
    if getline(next) == '{' | let lnum = next | let next = lnum+1 | endif
    let fixture_calls_mock = match(getline(next), a:name_fixt_mock) != -1
    if !fixture_calls_mock
        execute lnum . 'put=a:code_to_add'
        silent write
    endif
    execute "normal! \<C-O>"
    return 1
    "echomsg s:happy_kitty '`' . a:name_fixt . '` calls `' . a:name_fixt_mock . '`'
endfunction
function! expectmocking#AddInclude(string)
    "Add an include header line to the top of the file.
    "string is the exact string in '#include string'.
    "Example:
    "1. <> if string is: '<RecordedCall.h>'
    "      add:          '#include <RecordedCall.h>'
    "2. "" if string is: '"mock_DOL.h"'
    "      add:          '#include "mock_DOL.h"'
    "
    "old: execute cursor(1,1)'/'.a:string
    "Search for this line.
    let line_num = search(a:string, 'nw')
    let string_is_included = line_num != 0
    if string_is_included | return | endif
    "echomsg s:happy_kitty "Header needs to be included."
    "return
    " save the current cursor position in the window
    let save_cursor = getcurpos()
    " paste the #include at the first line in the window
    execute '0put=a:string'
    " restore the cursor to the original position in the window
    call setpos('.', save_cursor)
    silent write
endfunction
function! expectmocking#AddIncludeProjHdr(header_path)
    "Add an include header line to the top of the file.
    "Do nothing if header is already included.
    "
    let h_filename = fnamemodify(a:header_path, ":t")
    let include_header = '#include "' . h_filename . '"'
    call expectmocking#AddInclude(include_header)
endfunction
"call expectmocking#AddIncludeProjHdr(expand("%:t:r").'.h')
function! expectmocking#AddIncludeLibHdr(lib_name)
    "Add an include header line to the top of the file.
    "lib_name is a string without the .h
    "Do nothing if header is already included.
    "Example:
    "if lib_name is: 'RecordedCall'
    "add:            '#include <RecordedCall.h>'
    let include_header = '#include <' . a:lib_name . '.h>'
    call expectmocking#AddInclude(include_header)
endfunction
"call expectmocking#AddIncludeLibHdr(expand("%:t:r"))

function! expectmocking#StringExists(string)
    "Return true if the string exists in the current buffer.
    "let string = a:string . ";"
    let string = a:string
    let line_num = search('\M' . string, 'nw')
    return line_num != 0
endfunction
"echo 'bob ' . (expectmocking#StringExists('b0b')   ? 'exists' : 'does not exist')

function! expectmocking#HeaderBoilerplate(path)
    "Return list with lines of boilerplate header code.
    "Note: echomsg will not show the blank line but it is created!
    let h_NAME_H = '_' . toupper(fnamemodify(a:path,":t:r")) . '_H'
    " Eliminate macro name illegal tokens
    let h_NAME_H = substitute(h_NAME_H, "-", "_","g")
    let l:ifndef = '#ifndef '   . h_NAME_H
    let l:define = '#define '   . h_NAME_H
    let l:blank  = ''
    let l:endif  = '#endif // ' . h_NAME_H
    return [l:ifndef, l:define, l:blank, l:blank, l:endif]
endfunction
"let boilerplate = expectmocking#HeaderBoilerplate(expand("%:p:r").'-bob.h')
"for line in boilerplate
"    echomsg line
"endfor

function! expectmocking#ThisLineExpectsACall()
    "Returns true if there is an Expected Call on the current line.
    if matchstr(getline('.'), 'Expect_') == 'Expect_'
        "echo s:happy_kitty "This line has an `Expect_` function."
        return 1
    else
        echo s:sad_kitty "This line does not have an `Expect_` function."
        return 0
    endif
endfunction
"call expectmocking#ThisLineExpectsACall()

function! expectmocking#ThisIsATestFile()
    "Return 1 if this is a test file."
    if !folder#ThisIsInFolder('test')
        echo '>-.-< This is not the right folder.'
        return 0
    endif
    "echomsg ">^.^< This is the right folder."
    let filename_prefix = expand("%:t")[0:4]
    if (filename_prefix !=# 'test_')
        echomsg ">-.-< This is not a test file."
        return 0
    endif
    "echomsg ">^.^< This is a test file."
    return 1
endfunction
"call expectmocking#ThisIsATestFile()

function! expectmocking#NameOfLut()
    "Client is responsible for checking the current file is a test file before
    "calling this function.
    "Use 'ThisIsATestFile()' to check if the current file is a test file.
    return expand("%:t:r")[5:]
endfunction
"echo expectmocking#NameOfLut()

function! expectmocking#NameOfDol()
    "NameOfDol returns the name of the depended on library.
    "If name cannot be determined, return an empty string and log to v:errmsg.
    "
    "NameOfDol uses macro on 'Expect_' line if macro is there.
    let depended_on_lib_header = matchstr(getline('.'),'_MOCK_.*_H;')[0:-2]
    if depended_on_lib_header != ''
        "An explicit _MOCK_.*_H; on this line determines the DOF lib.
        "Try to jump to the DOF lib header file.
        let this_file = expand("%:p")
        silent! execute 'tag ' . depended_on_lib_header
        let header_file_exists = this_file != expand("%:p")
        if !header_file_exists
            echomsg s:sad_kitty '`' . depended_on_lib_header . '` does not exist.'
            return ''
        endif
        let depended_on_lib = expand("%:t:r")[5:]
        silent execute "noswapfile buffer #"
            " return to the test_ file
        return depended_on_lib
    endif
    "
    "NameOfDol uses #include mock line if no macro on Expect line.
    let save_cursor = getcurpos()
    let line_num = search('#include "mock_', 'w')
    if line_num == 0
        echo s:sad_kitty 'There are no #include mocked lib headers.'
        call setpos('.', save_cursor)
            " restore the cursor to the original position in the window
        return ''
    endif
    let only_one_mocked_lib = line_num == search('#include "mock_', 'w')
    call setpos('.', save_cursor)
    if only_one_mocked_lib
        call search('#include "mock_', 'w')
        let mocked_lib = matchstr(getline('.'),'"mock_.*"')[6:-4]
        call setpos('.', save_cursor)
        return mocked_lib
    endif
    "
    "NameOfDol fails if no macro and more than one include mock line.
    echo s:sad_kitty 'There is more than one #include mocked lib header.'
    return ''
endfunction
"echo expectmocking#NameOfDol() | "_MOCK_BLAH_H;

function! expectmocking#NameOfFut()
    "Return the name of the function under test (FUT).
    "Assumes the cursor is within the function definition for a test.
    "Assumes the test function name starts with FUT, followed by ._.
    "Assumes FUT does not contain underscores.
    "Assumes `void` is on this line.
    "Only works in C files.
    "Return empty string '' if test function name cannot be found.
    "
    "Alternative:
    "I don't do it this way, but I could. Determine FUT.func_name by finding
    "'RanAsHoped' and looking at the line above.
    let save_cursor = getcurpos()
    silent call FindPrevFunctionDefinition()
    let found_fdef = save_cursor != getcurpos()
    if !found_fdef                    | return '' | endif
    "Reject blank lines.
    if getline('.') == ''             | return '' | endif
    "Reject lines without spaces.
    if match(getline('.'), ' ') == -1 | return '' | endif
    "Reject lines without underscores.
    if match(getline('.'), '_') == -1 | return '' | endif
    "Should be safe to split this line now.
    let name = split(split(getline('.'),' ')[1],'_')[0]
    call setpos('.', save_cursor)
    return name
endfunction!
"echo expectmocking#NameOfFut()

function! s:StripLeadingWhiteSpace(string)
    "Return string with leading white space removed.
    "If there is no leading white space, string is returned unchanged.
    let string = a:string
    if string[0] == ' '
        let string = string[matchend(string, ' *'):]
    endif
    return string
endfunction
"echo '`alice` is still `' . s:StripLeadingWhiteSpace('alice') . '`'
"echo 'but `    bob` becomes `' . s:StripLeadingWhiteSpace('    bob') . '`'

function! s:StripTrailingWhiteSpace(string)
    "Return string with trailing white space removed.
    "If there is no trailing white space, string is returned unchanged.
    let string = a:string
    while string[-1:-1] == ' ' | let string = string[:-2] | endwhile
    return string
endfunction
"echo '`alice` is still `' . s:StripTrailingWhiteSpace('alice') . '`'
"echo 'but `bob    ` becomes `' . s:StripTrailingWhiteSpace('bob    ') . '`'

function! expectmocking#FuncDecl(input_string) "not used
    "Return the declaration from a line of code with a function definition.
    "Return type is a list even if the function is declaration is only one line.
    "This works for definitions that spread input args across multiple lines.
    "
    "Reject function calls. If input string has a semicolon it is a call.
    let string_has_semicolon = match(a:input_string, ';') != -1
    if string_has_semicolon | return [''] | endif
    "
    let func_decl = matchstr(a:input_string, '.*(.*')
    "Strip leading white space.
    let func_decl = s:StripLeadingWhiteSpace(func_decl)
    "Strip trailing white space.
    let func_decl = s:StripTrailingWhiteSpace(func_decl)
    "
    "Function calls also show up as input args on their own lines.
    "Reject these by checking that a return type is present.
    let before_parenthesis = matchstr(func_decl, '.*(' )
    let has_a_return_type = match(before_parenthesis, ' ') != -1
    if !has_a_return_type | return [''] | endif
    "This entire input string is a legitimate function declaration.
    "If it has balanced () then we are done.
    let keepempty = 1
    let num_o = len(split(func_decl, '(\zs', keepempty))
    let num_c = len(split(func_decl, ')\zs', keepempty))
    if num_c == num_o
        "Strip trailing stuff like '{'
        let func_decl = func_decl[0:(strridx(func_decl,')'))]
        return [func_decl]
    endif
    "
    "But if it does not have all its ')' then it is spread over multiple lines.
    function! NumberOfExtraLines()
        let l:start = line('.')
        let l:next  = line('.')+1
        function! NextLineIsTheEndOfTheDecl(lnum)
            let nextline = getline(a:lnum)
            let keepempty = 1
            let num_o = len(split(nextline, '(\zs', keepempty))
            let num_c = len(split(nextline, ')\zs', keepempty))
            return num_c - num_o
        endfunction
        while !NextLineIsTheEndOfTheDecl(l:next)
            let l:next = l:next + 1
        endwhile
        return l:next - l:start
    endfunction
    "let func_decl = [func_decl] + [getline(line('.')+1)]
    let func_decl = [func_decl] + getline(line('.')+1, line('.')+NumberOfExtraLines())
    return func_decl
    "echo len(split(nextline, ')')) - len(split(nextline, '('))
endfunction
"echo "             Returns a function declaration:" expectmocking#FuncDecl('void DoThing(uint8_t bob)')
"echo "                 Strips leading white space:" expectmocking#FuncDecl('    void DoThing(uint8_t bob)')
"echo "                Strips trailing white space:" expectmocking#FuncDecl('void DoThing(uint8_t bob)    ') + ['--']
"echo "                   Strips trailing anything:" expectmocking#FuncDecl('void DoThing(uint8_t bob){}')
"echo "Returns an empty string for a function call:" expectmocking#FuncDecl('DoThing(bob1);')
"echo "  Returns an empty string if no return type:" expectmocking#FuncDecl('DoThing(bob1)')
"echo "   Accepts multi-line function declarations:" expectmocking#FuncDecl('void DoThing(')
""arg1(argarg),
""arg2)

function! expectmocking#CallSig(input_string) "not used
    "Return the first function signature on the line of code.
    "Usually one line of code is one statement.
    "But expectmocking encourages a macro placed before a function call.
    "Remove this macro statement and any trailing whitespace.
    let func = matchstr(a:input_string, '.*(.*);')
    "Not a function call.
    if func == '' | return '' | endif
    "Drop the final semicolon
    let func = func[:-2]
    let only_one_semicolon = match(func, ';') == -1
    "Only one statement on this line. Simple.
    if only_one_semicolon | return func[matchend(func,' *'):] | endif
    "More than one statement. Find the penultimate semicolon.
    let func = func[strridx(func, ';'):]
    "Return the text after the semicolon and any whitespace.
    return func[matchend(func,'; *'):]
endfunction
"echo "         Not a function:" expectmocking#CallSig('bob')
"echo "          One statement:" expectmocking#CallSig('    Expect_DOF(FtCmdRead);')
"echo "Macro; <space> function:" expectmocking#CallSig('    _MOCK_DOL2_H;   Expect_DOF(FtCmdRead);')

function! expectmocking#ExpectedCallSig(input_string)
    "Return the function signature of the 'Expect_' call in the input.
    "Example:
    "   let function_signature = expectmocking#ExpectedCallSig(getline('.'))
    "if getline('.') returns: 'Expect_DOF(FtCmd_Read);'
    "then function_signature is: 'DOF(FtCmd_Read)'
    return matchstr(a:input_string,'Expect_.*(\&.*)')[7:]
endfunction
"echo expectmocking#ExpectedCallSig('Expect_DOF(FtCmdRead);')
"echo expectmocking#ExpectedCallSig('_MOCK_DOL2_H; Expect_DOF(FtCmdRead);')

function! s:ArgIsANumber(arg)
    "Return 1 if arg starts with a number.
        "Example:
        "if arg is: '0xFF'
        "return:    1
        "if arg is: 'xxFF'
        "return:    0
    if match(a:arg,'\d') == 0 | return 1 | endif
    return 0
endfunction
"echo 'ArgIsANumber returns ' . s:ArgIsANumber('0xFF') . ' when arg starts with a number.'
"echo 'ArgIsANumber returns ' . s:ArgIsANumber('xxFF') . ' otherwise.'
"
function! s:ConvertPointerStarsToPrefixp(arg_type)
    "Convert trailing `*` to a `p` prefix
    "Example: 'uint8_t *' becomes 'p_uint8_t'
    let has_stars = match(a:arg_type, '*') != -1
    if !has_stars | return a:arg_type | endif
    let substring_without_stars = split(a:arg_type,'*')[0]
    "let num_stars = len(split(a:arg_type,'*'))
    let arg_type_with_prefix =  '_' . s:StripTrailingWhiteSpace(substring_without_stars)
    for sub_string in split(a:arg_type,'*')
        let arg_type_with_prefix = 'p' . arg_type_with_prefix
    endfor
    return arg_type_with_prefix
endfunction
"let s:arg_type1 = 'uint8_t'
"let s:arg_type2 = 'uint8_t *'
"let s:arg_type3 = 'uint8_t **'
"echo '"'.s:ConvertPointerStarsToPrefixp(s:arg_type1).'"'
"echo '"'.s:ConvertPointerStarsToPrefixp(s:arg_type2).'"'
"echo '"'.s:ConvertPointerStarsToPrefixp(s:arg_type3).'"'
function! s:CollectPointerStars(type, line)
    "Returns a variable's type with any trailing pointer stars.
    "Input `type` is the datatype of the variable defined on this line of code.
    "Input `line` is the entire line of code defining the variable.
    " Example: let line = 'uint8_t *FtCmd_Read  = 0xC6;'
    " Return: 'uint8_t *'
    " [x] returns type without changes if this is not a pointer
    " [x] returns type with stars as they are spaced in the definition
    " [x] identifies star if there is a  space between type and star
    " [x] identifies star if there is no space between type and star
    " [x] strips trailing white space if there is a space between star and variable name
    " [x] handles multiple stars
    "let everything_after_type = a:line[strlen(a:type):]
    let everything_after_type = a:line[matchend(a:line, a:type):]
    " everything_after_type = ' *FtCmd_Read  = 0xC6;'
    "let first_identifier_char = matchstr(everything_after_type, '\i')
    let start_of_variable_name = match(everything_after_type, '\i')
    " see :h isident
    let substring_with_stars = everything_after_type[0:start_of_variable_name-1]
    let substring_with_stars = s:StripTrailingWhiteSpace(substring_with_stars)
    let has_stars = match(substring_with_stars, '*') != -1
    if !has_stars | return a:type | endif
    if  has_stars | return a:type . substring_with_stars | endif
endfunction
"let s:line0 = '    uint8_t FtCmd_Read  = 0xC6;'
let s:line1 = '    uint8_t * FtCmd_Read  = 0xC6;'
"let s:line2 = '    uint8_t *FtCmd_Read  = 0xC6;'
"let s:line3 = '    uint8_t **FtCmd_Read  = 0xC6;'
"let s:line4 = '    uint8_t* FtCmd_Read  = 0xC6;'
"let s:line5 = '    uint8_t *read_buffer_address = &read_buffer;'
"let s:type = 'uint8_t'
"echo 'CollectPointerStars returns "' . s:CollectPointerStars(s:type, s:line0) . '"'
"echo 'CollectPointerStars returns "' . s:CollectPointerStars(s:type, s:line1) . '"'
"echo 'CollectPointerStars returns "' . s:CollectPointerStars(s:type, s:line2) . '"'
"echo 'CollectPointerStars returns "' . s:CollectPointerStars(s:type, s:line3) . '"'
"echo 'CollectPointerStars returns "' . s:CollectPointerStars(s:type, s:line4) . '"'
"echo 'CollectPointerStars returns "' . s:CollectPointerStars(s:type, s:line5) . '"'
function! s:ReadDatatypeFromDeclStr(decl_str)
    "Handles non-pointer and pointer datatypes.
    "let a:decl_str = getline('.')
    let l:type = split(a:decl_str, ' ')[0]
        "get the type
    let l:type = s:CollectPointerStars(l:type, a:decl_str)
        "catches pointer types
        "include any * after the type, including spaces
    return l:type
endfunction
"let s:decl = '    uint8_t *read_buffer_address = &read_buffer;'
"echo 'ReadDatatypeFromDeclStr returns "' . s:ReadDatatypeFromDeclStr(s:decl) . '"'
function! expectmocking#ArgDatatype(arg)
    "Returns the datatype of the input arg. Input and return values are strings.
    "Return 'undeclared' if arg starts with a number (e.g., 0xFF).
    "Return 'undeclared' if no declaration can be found.
        "Example:
        "   if input arg is: 'FtCmd_Read'
        "   return:          'uint8_t'
        "This assumes the tags file was updated with local variables turned on:
        "   call system("ctags --c-types=+l -R .")
        "   The +l tags local variables, otherwise only globals are found.
    if s:ArgIsANumber(a:arg) | return 'undeclared' | endif
    "preserve file and cursor location
    let this_file = expand("%") | let save_cursor = getcurpos()
    " Try to jump to the arg declaration
    silent! execute 'tag ' . a:arg
    let found_arg_declaration = getcurpos() != save_cursor
    if found_arg_declaration
        let type = s:ReadDatatypeFromDeclStr(getline('.'))
        if this_file != expand("%")
            " return to the test_ file
            silent execute "noswapfile buffer #"
        endif
        call setpos('.', save_cursor)
        return type
    endif
    return 'undeclared'
endfunction

function! expectmocking#RemoveParentheses(string)
    "Returns string with first opening and last closing parentheses removed.
    "Anything before the opening parenthesis and after the closing parenthesis
    "is also removed.
    "Example
    "if string is 'foo(uint8_t, blah())bar'
    "return       'uint8_t, blah()'
    "
    "return a:string[1:-2]
    return a:string[match(a:string,'(')+1:strridx(a:string,')')-1]
endfunction
"echo expectmocking#RemoveParentheses('foo(uint8_t, DoThing())bar')

function! expectmocking#RemoveWhitespace(string)
    "Returns string with whitespace removed.
    "Example
    "if string is 'arg1, arg2, arg3'
    "return       'arg1,arg2,arg3'
    return substitute(a:string, ' ', '', 'g')
endfunction
"echo expectmocking#RemoveWhitespace('arg1, arg2, arg3')
"
function! expectmocking#FuncArgs(func_sig)
    "Return the string with args.
    "Example: expectmocking#FuncArgs('DOF(FtCmdRead)') returns '(FtCmdRead)'.
    "Also works if function args are themselves function calls.
    return matchstr(a:func_sig,'(.*)')
endfunction
"echo expectmocking#FuncArgs('DOF(ReturnThing(), FtCmdRead)')

function! expectmocking#ArgStringToList(args)
    "Return string args as a list of args.
    "If args is '(void)' as in no args, return an empty list [].
    "Example:
    "if args is:    '(arg1, arg2)'
    "return:        ['arg1', 'arg2']
    if a:args == '(void)' | return [] | endif
    let args = a:args
    let args = expectmocking#RemoveParentheses(args)
    let args = expectmocking#RemoveWhitespace(args)
    let arglist = split(args,',')
    return arglist
endfunction
"echo expectmocking#ArgStringToList('(void)')
"echo expectmocking#ArgStringToList('(alice)')
"echo expectmocking#ArgStringToList('(alice, bob)')
function! expectmocking#MakeSetupRecordType(arg_type)
    "Return the name of the `SetupRecord_` function given the arg_type.
    "Input arg_type is ...
    "Prefix arg_type with `SetupRecord_`.
    "If arg is a pointer, turn each * into a `p` prefix.
    "return 'SetupRecord_p_uint8_t'
    let SetupRecord_type = 'SetupRecord_'. s:ConvertPointerStarsToPrefixp(a:arg_type)
    return SetupRecord_type
endfunction
function! expectmocking#ReplaceArgsWithTypes(function_signature)
    "Returns a string of arg types for the function signature.
    "Example:
    "if function_signature is: 'DOF(FtCmd_Read)'
    "then function_input_types are: '(uint8_t)'
    "Uses expectmocking#ArgDatatype(arg) to intelligently determine datatype.
    let func_args = expectmocking#FuncArgs(a:function_signature)
    if func_args == '()' | return '(void)' | endif
    let func_args = expectmocking#RemoveParentheses(func_args)
    let func_args = expectmocking#RemoveWhitespace(func_args)
    let arg_list = split(func_args,',')
    let func_types = '('
    for arg in arg_list
        let type = expectmocking#ArgDatatype(arg)
        let func_types = func_types . type
        if type ==# 'undeclared' | return func_types . ')' | endif
        let is_last_arg = arg == arg_list[-1]
        let func_types = func_types . (is_last_arg ? ')' : ', ')
    endfor
    return func_types
endfunction
function! expectmocking#NumArgs(function_signature)
    "Returns number of arguments.
    "Example:
    "if function_signature is:  'DOF(FtCmd_Read)'
    "returns:                   1
    let func_args = expectmocking#FuncArgs(a:function_signature)
    if func_args == '()' | return 0 | endif
    let func_args = expectmocking#RemoveParentheses(func_args)
    let func_args = expectmocking#RemoveWhitespace(func_args)
    let arg_list = split(func_args,',')
    return len(arg_list)
endfunction
"echo expectmocking#NumArgs('SomeFunc()')
"echo expectmocking#NumArgs('SomeFunc(arg1)')
"echo expectmocking#NumArgs('SomeFunc(arg1, arg2)')
"echo expectmocking#NumArgs('SomeFunc(arg1, arg2, arg3)')
function! expectmocking#ReplaceArgsWithDummies(args)
    "Return a string of dummy arg names.
    "args is the string returned by expectmocking#FuncArgs(sig).
    "If args is '()' as in no args, return '()'.
    "Example:
    "if args is:    '(FtCmdRead, bob)'
    "return:        '(arg1, arg2)'
    if a:args == '()' | return '()' | endif         "expected case
    if a:args == '(void)' | return '(void)' | endif "not expected case
    let args = a:args
    let args = expectmocking#RemoveParentheses(args)
    let args = expectmocking#RemoveWhitespace(args)
    let arglist = split(args,',')
    let dummy_arglist = map(arglist, '"arg". (v:key+1)')
    let dummy_args = '('
    for arg in dummy_arglist
        let dummy_args = dummy_args . arg
        let is_last_arg = arg == dummy_arglist[-1]
        let dummy_args = dummy_args . (is_last_arg ? ')' : ', ')
    endfor
    return dummy_args
endfunction
"echo expectmocking#ReplaceArgsWithDummies('()')
"echo expectmocking#ReplaceArgsWithDummies('(void)')
"echo expectmocking#ReplaceArgsWithDummies('(alice)')
"echo expectmocking#ReplaceArgsWithDummies('(alice, bob)')

function! expectmocking#AddDummyArgsToTypes(func_types)
    "Returns a string of arg types with dummy arg names.
    "func_types is the string returned by ReplaceArgsWithTypes(sig).
    "If argtype is '(void)' as in no arg, return '(void)'.
    "Example:
    "if func_types is:  '(uint8_t, uint16_t)'
    "return:            '(uint8_t arg1, uint16_t arg2)'
    if a:func_types == '(void)' | return '(void)' | endif
    let args = a:func_types
    let args = expectmocking#RemoveParentheses(args)
    let args = expectmocking#RemoveWhitespace(args)
    let arglist = split(args,',')
    let dummy_arglist = map(arglist, 'v:val ." arg". (v:key+1)')
    let dummy_args = '('
    for arg in dummy_arglist
        let dummy_args = dummy_args . arg
        let is_last_arg = arg == dummy_arglist[-1]
        let dummy_args = dummy_args . (is_last_arg ? ')' : ', ')
    endfor
    return dummy_args
endfunction
"echo expectmocking#AddDummyArgsToTypes('(void)')
"echo expectmocking#AddDummyArgsToTypes('(uint8_t, uint16_t)')
"echo expectmocking#AddDummyArgsToTypes('(uint8_t, uint16_t, undeclared)')

function! expectmocking#FuncName(func_sig)
    "Return the function name.
    "Example: expectmocking#FuncName('DOF(FtCmdRead)') returns 'DOF'.
    "Also works if function arguments include function calls.
    return a:func_sig[:match(a:func_sig,'(')-1]
endfunction
"echo expectmocking#FuncName('DOF(DoThing(), FtCmdRead)')
function! expectmocking#FuncNameAndTypes(function_signature) "not used
    "Need expectmocking#ReplaceArgsWithTypes
    "Returns function name and input types from input function signature.
    "Example:
    "if function_signature is: 'DOF(FtCmd_Read)'
    "then function_name is: 'DOF'
    "and  function_input_types are: '(uint8_t)'
    let func_name = expectmocking#FuncName(a:function_signature)
    "let func_name = matchstr(a:function_signature,'.*(')[:-2]
    let func_types = expectmocking#ReplaceArgsWithTypes(a:function_signature)
    return [func_name, func_types]
endfunction
"echo expectmocking#FuncNameAndTypes('DOF(FtCmd_Read)')

function! expectmocking#FnPtrAssign(dof)
    "Return code for function pointer assignment as a list of lines of code.
    "Input is a dictionary with keys `name` and `argtypes`.
    let code = ['void (*' . a:dof.name . ')' . a:dof.argtypes . ' = '
        \. a:dof.name . '_Implementation;']
    return code
endfunction
"let dof={} | let dof.name = 'DOF' | let dof.argtypes = '(uint8_t)'
"echo expectmocking#FnPtrAssign(dof)[0]

"function! expectmocking#CodeFuncPtrAssignment(list_fname_ftypes) "not used
"    let fname  = a:list_fname_ftypes[0]
"    let ftypes = a:list_fname_ftypes[1]
"    let fname_Impl = fname.'_Implementation'
"    let fp_assignment =  'void (*'.fname.')'.ftypes.' = '.fname_Impl.';'
"    return [fp_assignment]
"endfunction
"echo expectmocking#CodeFuncPtrAssignment(['DOF', '(uint8_t)'])

function! expectmocking#FnPtrImplem(dof)
    "Return code for function pointer implementation as a list of lines of code.
    "Input is a dictionary with keys `name` and `argtypes`.
    let code = ['static void '. a:dof.name . '_Implementation' . a:dof.argtypes]
    let code += ['{}']
    return code
endfunction
"let dof={} | let dof.name = 'DOF' | let dof.argtypes = '(uint8_t)'
"echo expectmocking#FnPtrImplem(dof)

"function! expectmocking#CodeFuncPtrImpl(list_fname_ftypes) "not used
"    let fname  = a:list_fname_ftypes[0]
"    let ftypes = a:list_fname_ftypes[1]
"    let fname_Impl = fname.'_Implementation'
"    let func_implementation = ['static void '.fname_Impl.ftypes]
"    let func_implementation += ['{}']
"    return func_implementation
"endfunction
"echo expectmocking#CodeFuncPtrImpl(['DOF', '(uint8_t)'])

function! expectmocking#IsDefined(fname)
    "Return 1 if fname is defined, 0 if not.
    "
    "preserve file and cursor location
    let this_file = expand("%") | let save_cursor = getcurpos()
    execute "normal! l" | let check_cursor = getcurpos()
    " Jump to the source file with the function definition
    silent! execute 'tag ' . a:fname
    let found_fname_definition = getcurpos() != check_cursor
    if  found_fname_definition
        if this_file != expand("%") | silent execute "noswapfile buffer #" | endif
        call setpos('.', save_cursor)
        return 1
    endif
    return 0
endfunction
function! expectmocking#ReturnFuncDef(fname) "not done or tested, just a sketch
    "Return the function definition.
    "Return an empty string if the definition is not found.
    "TODO: copy definition (if found) and return it instead of returning 'bob'.
    "
    "preserve file and cursor location
    if !expectmocking#IsDefined(a:fname) | return '' | endif
    let this_file = expand("%") | let save_cursor = getcurpos()
    " Jump to the source file with the function definition
    silent! execute 'tag ' . a:fname
    let def = 'bob' "TODO: Replace with code to grab the definition!
    " Jump back if file changed needed.
    if this_file != expand("%") | silent execute "noswapfile buffer #" | endif
    " Restore the cursor.
    call setpos('.', save_cursor)
    return def
endfunction
"call expectmocking#FuncAlreadyDefined('DoThing')
"echo 'DoThing is' (expectmocking#FuncAlreadyDefined('DoThing') ? 'defined':'not defined')
function! expectmocking#LastLineIsBlank()
    "Return 1 if last line in current buffer is blank.
    "
    return getline('$') == ''
endfunction
"echo 'Last line is ' . (expectmocking#LastLineIsBlank() ? '' : 'not ') . 'blank.'
function! expectmocking#AppendBlankLine()
    "Append a blank line to the current buffer.
    "
    call append('$',[''])
endfunction
"call expectmocking#AppendBlankLine()
function! FunctionIsAlreadyDefined(func_name)
    "I thought this was deprecated, but it still gets called.
    "I think it was originally `FuncAlreadyDefined()`.
    "Try replacing calls to expectmocking#FuncAlreadyDefined with
    "expectmocking#IsDefined.
    let save_cursor = getcurpos()
    silent! execute 'tag ' . a:func_name
        " Jump to the source file with the function definition
    let cursor_moved = getcurpos() != save_cursor
    if cursor_moved
        "found function definition, go back to the original file
        silent execute "noswapfile buffer #"
        echo '"'.a:func_name.'" is already defined in "'.expand("%").'"'
        return 1
    endif
    return 0
endfunction
function! expectmocking#AddFuncDeclToHeader(add_silently)
    " TODO: why does another function get highlighted after this gets called?
    "Add the function prototype to the header file.
    "If add_silently is 1, do not print messages to screen.
    "
    if !file#ThisIsC() || file#UnsavedChanges()
        echomsg s:sad_kitty 'Nothing was done.'
        return
    endif
    "echomsg s:happy_kitty 'This is a saved C file.'
    let func_decl = expectmocking#FuncDecl(getline('.'))
    " cursor hasn't moved yet...
    " echomsg "DEBUG, getline('.') is '" . getline('.') . "'"
    " 2019-09-18
    " This relies on switching between buffers without the cursor moving.
    " If the buffer is open in more than one window, the cursor goes to the
    " first line, not to its previous position.
    " I'm not sure how to test for this to warn the user, so...
    " for now, I'm just making the sad_kitty message a little more useful.
    if func_decl[0] == ''
        echomsg s:sad_kitty "There is no function declaration: '" . getline('.') "'"
        echomsg s:invisible_kitty 'Nothing was done.'
        return
    endif
    " cursor hasn't moved yet...
    "echomsg s:happy_kitty 'function declaration is'
    "for line in func_decl | echo line | endfor
    "return
    let header_path = expand("%:p:r") . ".h"
    "echomsg s:happy_kitty header_path
    "return
    "Create header file if it does not exist.
    if !file#Exists(header_path)
        if !a:add_silently
            echomsg s:happy_kitty     "  File did not exist:" header_path
        endif
        call file#New(header_path)
        if !a:add_silently
            echomsg s:invisible_kitty "So I made a new file:" header_path
        endif
        let boilerplate = expectmocking#HeaderBoilerplate(header_path)
        call writefile(boilerplate, header_path)
    endif
    " cursor hasn't moved yet...
    let save_cursor = getcurpos()
    "Open header from existing buffer or existing file.
    if bufexists(header_path)
        let header_bufnr = file#FindBufnum(header_path)
        "echomsg s:happy_kitty "buffer number:" header_bufnr
        " cursor hasn't moved yet...
        silent execute "noswapfile buffer "header_bufnr
        " weird cursor movement happens before here
    else
        silent execute "noswapfile edit" header_path
    endif
    " Editing header now...
    "echomsg s:happy_kitty 'Editing file:' header_path
    let func_decl[-1] = func_decl[-1] . ';'
    "Do nothing and return if prototype is already there.
    if ( expectmocking#StringExists(func_decl[-1]) &&
        \expectmocking#StringExists(func_decl[0]) )
        silent execute "noswapfile buffer #"
        echomsg s:happy_kitty 'Function prototype is already in the header.'
        " Restore the cursor.
        call setpos('.', save_cursor)
        return
    endif
    "echomsg s:happy_kitty 'Function prototype is not in the header yet.'
    "Insert prototype two lines up from end of file.
    execute line('$')-2.'put=func_decl'
    silent write
    silent execute "noswapfile buffer #"
    " Returned to file with function definition.
    " Restore the cursor.
    call setpos('.', save_cursor)
    " Cursor is in the right place
    " Add the #include header line to the .c file.
    call expectmocking#AddIncludeProjHdr(header_path)
    if !a:add_silently
        echomsg s:happy_kitty 'Added function prototype: `' . func_decl[0] . '`'
                \ . 'to `' . header_path . '`'
    endif
    " Update Cscope? No, I don't want to wait a few seconds every time I do ;fh.
    " if !a:add_silently
    "     echomsg s:waiting_kitty 'Updating cscope.out and tags...'
    " endif
    " silent call UpdateCscopeDatabase()
    " silent call ctags#Update()
    " if !a:add_silently
    "     echomsg s:happy_kitty   'Updating cscope.out and tags... Done.'
    " endif
endfunction
function! expectmocking#AddCodeToExistingFile(filepath, code_list, lnum)
    "Add list of lines of code before line number lnum at filepath.
    "Put one blank line before adding code.
    "
    if !file#ThisIsC() || file#UnsavedChanges()
        echomsg s:sad_kitty 'Nothing was done.'
        return 0
    endif
    "Preserve current file.
    let this_file = expand("%:p")
    "Open file.
    silent execute "noswapfile edit " . a:filepath
    "Insert code in file.
    " Add a blank line if last line is not blank
    if !expectmocking#LastLineIsBlank() | call expectmocking#AppendBlankLine() | endif
    "call append('$', a:code_list)
    call append(a:lnum, a:code_list)
    silent write
    silent execute "noswapfile edit " . this_file
    silent call ctags#Update()
    return 1
endfunction
function! expectmocking#AddExternDeclToHeader(add_silently)
    "Add the exten declaration to the header file.
    "If add_silently is 1, do not print messages to screen.
    "
    if !file#ThisIsC() || file#UnsavedChanges()
        echomsg s:sad_kitty 'Nothing was done 1.'
        return
    endif
    "echomsg s:happy_kitty 'This is a saved C file.'
    let this_line = getline('.')
    "echomsg s:happy_kitty this_line
    "    ">^.^< void (*BOF2)(uint8_t) = BOF2_Implementation;
    "return
    let no_assignment = match(this_line, '=') == -1
    if  no_assignment
        echomsg s:sad_kitty       'No assignment on this line.'
        echomsg s:invisible_kitty 'Nothing was done 2.'
        return
    endif
    let extdecl = this_line[0:match(this_line, '=')-1]
    let extdecl = s:StripLeadingWhiteSpace(extdecl)
    let extdecl = s:StripTrailingWhiteSpace(extdecl)
    let extdecl = 'extern ' . extdecl . ';'
    "echomsg s:happy_kitty 'extdecl: `' . extdecl . '`'
    "return
    let header_path = expand("%:p:r") . ".h"
    "echomsg s:happy_kitty header_path
    "return
    "----
    "Create header file if it does not exist.
    if !file#Exists(header_path)
        if !a:add_silently
            echomsg s:happy_kitty     "  File did not exist:" header_path
        endif
        call file#New(header_path)
        if !a:add_silently
            echomsg s:invisible_kitty "So I made a new file:" header_path
        endif
        let boilerplate = expectmocking#HeaderBoilerplate(header_path)
        call writefile(boilerplate, header_path)
    endif
    "Open header from existing buffer or existing file.
    if bufexists(header_path)
        let header_bufnr = file#FindBufnum(header_path)
        "echomsg s:happy_kitty "buffer number:" header_bufnr
        silent execute "noswapfile buffer "header_bufnr
    else
        silent execute "noswapfile edit" header_path
    endif
    "echomsg s:happy_kitty 'Editing file:' header_path
    "Do nothing and return if prototype is already there.
    if expectmocking#StringExists(extdecl)
        silent execute "noswapfile buffer #"
        echomsg s:happy_kitty 'Function pointer extern declaration is already in the header.'
        return
    endif
    "echomsg s:happy_kitty 'Function pointer extern declaration is not in the header yet.'
    ""This only checks for exact matches. It does not catch existing
    ""declarations with extra whitespace or argument names in addition to types.
    "return
    "Insert declaration two lines up from end of file.
    execute line('$')-2.'put=extdecl'
    silent write
    silent execute "noswapfile buffer #"
    "Add the #include header line to the .c file.
    call expectmocking#AddIncludeProjHdr(header_path)
    if !a:add_silently
        echomsg s:waiting_kitty 'Updating cscope.out and tags...'
    endif
    silent call UpdateCscopeDatabase()
    silent call ctags#Update()
    if !a:add_silently
        echomsg s:happy_kitty   'Updating cscope.out and tags... Done.'
        echomsg s:happy_kitty 'Added declaration: `' . extdecl . '`'
                \ . 'to `' . header_path . '`'
    endif
endfunction
"Old stuff I am not erasing yet.
function! OldReplaceArgsWithTypes(func_sig) "not used
    "Return the function signature with the arguments replaced by their
    "types.
    "Uses OldLookUpArgDatatype(arg) to intelligently determine datatype.
    let func_args = matchstr(a:func_sig,'(.*)')
    if func_args == '()'
        "No arguments
        return '(void)'
    endif
    let func_args = func_args[1:-2]
        "Get the args as a string without the surrounding ()."
    let func_args = substitute(func_args, ' ', '', 'g')
        "Remove white space from the string.
    let args = split(func_args,',')
        "Make a list of args
    "let types = []
    let func_types = '('
    for arg in args
        let type = OldLookUpArgDatatype(arg)
        let func_types = func_types . type
        if type == 'undeclared'
            "give up and return whatever you found so far
            return func_types . ')'
            "return string(types)
        endif
        " Look up the arg type, add that, not the arg itself
        "call add(types, type)
        let last_arg = arg == args[-1]
        if last_arg
            let func_types = func_types . ')'
        else
            let func_types = func_types . ', '
        endif
    endfor
    "let func_types = '('.string(types).')'
    "let func_types = substitute(func_types, "'", '', 'g')
    "let func_types = substitute(func_types, "[", '', '')
    "let func_types = substitute(func_types, "]", '', '')
    return func_types
endfunction
function! OldLookUpArgDatatype(arg) "not used
    "This assumes ctags was ran with these options:
    "call system("ctags --c-types=+l -R .")
    "The +l tags local variables, otherwise only globals are found.
    let type = 'undeclared'
        " type will be 'undeclared' if the declaration cannot be found
    "First check that arg is not a number.
    let arg_is_a_number = matchstr(a:arg[0],'\d')
    if  arg_is_a_number
        echo "Cannot determine datatype of magic number arguments."
        return type
    endif
    "preserve file and cursor location
    let this_file = expand("%")
    let save_cursor = getcurpos()
    silent! execute 'tag ' . a:arg
        " Jump to the arg declaration
    let cursor_moved = getcurpos() != save_cursor
    if cursor_moved
        "found it
        let type = split(getline('.'), ' ')[0]
            "get the type
        if this_file != expand("%")
            " return to the test_ file
            silent execute "noswapfile buffer #"
        endif
        call setpos('.', save_cursor)
        return type
    endif
    echo 'Cannot find argument declaration for "'.a:arg.'" anywhere in this project.'
    return type
endfunction
function! OldOldLookUpArgDatatype(arg) "not used
    let type = 'undeclared'
        " type will be 'undeclared' if the declaration cannot be found
    "First check that arg is not a number.
    let arg_is_a_number = matchstr(a:arg[0],'\d')
    if  arg_is_a_number
        echo "Cannot determine datatype of magic number arguments."
        return type
    endif
    "preserve file and cursor location
    let save_cursor = getcurpos()
    let look_global = 1
    let look_local = 1
    "Use searchdecl('str', bool_global, bool_local)
        "This does not really use C syntax,
        "it just searches from a start position
        "up to the line the cursor is on.
        "It also excludes comments from the search.
        "Local: start position is the start of the code block
        "Global: start position is the start of the file
        "Cursor:
        "Cursor does not move if it is already on the line with the first
        "occurence of arg.
        "Otherwise, the cursor goes to the position of arg.
        "Local: caution:
        "Local is fooled into thinking a global declaration is local
        "if there is no blank line between the global declaration
        "and the block the cursor is in.
        "Global: caution:
        "Global is fooled into thinking a local declaration is global
        "if it occurs in any code block above the current code block.
        "My precaution against this is to check that
        "the cursor is in column 1 after the search.
    "first look local to this code block:
    call searchdecl(a:arg, !look_global, look_local)
    let cursor_moved = getcurpos() != save_cursor
    if cursor_moved
        "found it
        let this_really_is_a_declaration = matchstr(getline('.'),'(') == ''
        if  this_really_is_a_declaration
        endif
        let type = split(getline('.'), ' ')[0]
            "get the type
        call setpos('.', save_cursor)
            "restore the cursor
        return type
    endif
    "second look global to this file:

    "No, just use tags for whether global or not
    "call searchdecl(a:arg, look_global)
    let this_file = expand("%")
    "let cursor_moved = getcurpos() != save_cursor
    "if cursor_moved
        "execute 'normal! ^'
        "    "Go to first character on this line
        "let this_really_is_a_global = getcurpos()[2] == 1
        "if this_really_is_a_global
        "    "found it
        "    let type = split(getline('.'), ' ')[0]
        "    "get the type
        "    call setpos('.', save_cursor)
        "    "restore the cursor
        "    return type
        "else
        "    call setpos('.', save_cursor)
        "        "restore the cursor
        "endif
    "endif
    "lastly look global to this project:
    silent! execute 'tag ' . a:arg
        " Jump to the arg declaration
    let cursor_moved = getcurpos() != save_cursor
    if cursor_moved
        "found it
        let type = split(getline('.'), ' ')[0]
            "get the type
        if this_file != expand("%")
            " return to the test_ file
            silent execute "noswapfile buffer #"
        endif
        return type
    endif
    echo 'Cannot find argument declaration for "'.a:arg.'" anywhere in this project.'
    return type
endfunction
