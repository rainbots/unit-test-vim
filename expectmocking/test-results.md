# Test Results
## Testing lib: `expectmocking` function: `ReplaceArgsWithTypes()`

- **PASS** *Test #1*: `ReplaceArgsWithTypes_returns_a_string_of_arg_types_for_the_input_sig`

------------------
1 Tests 0 Failures
## Testing lib: `expectmocking` function: `MakeSetupRecordType()`

- **PASS** *Test #1*: `MakeSetupRecordType_prefixes_arg_type_with_SetupRecord`
- **PASS** *Test #2*: `MakeSetupRecordType_prefixes_type_with_p_for_each_star_in_a_pointer_arg`

------------------
2 Tests 0 Failures
## Summary for all tests


------------------
3 Tests 0 Failures
