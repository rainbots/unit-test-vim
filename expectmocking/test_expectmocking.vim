"Unit tests for expectmocking.`vim`.
"TODO: handle pointers
execute 'source expectmocking/expectmocking.vim'
execute 'source autoload/testgroup.vim'
execute 'source autoload/fakeproject.vim'
"=====[ Context ]=====
" mockist tests check a function's behavior in one path through its conditionals
    " The FUT is a function in the LUT
    " FUT := Function Under Test
    " LUT := Lib      Under Test
    " The FUT depends on other functions. This is why we mock.
    " A DOF is a function in the DOL
    " DOF := Depended On Function
    " DOL := Depended On Lib
    " The FUT and DOF are always different functions.
    " The LUT and DOL could be the same lib or different libs.
    " For one FUT there can be 0, 1, or many DOFs.
    " If there are 0 DOFs, the unit test is an ordinary unit test.
    " If there are 1 or more DOFs, it is a mockist test.
    "
" FUT and LUT are a point-of-view
    " All project libs require dev! Which dev lib is the LUT and which are the
    " DOLs depends on the perspective of the lib being developed. Not all libs
    " are depended on and not all libs require mocking, so rather than create
    " behemoth test code files full of unused boiler plate, the project test
    " code is created as-needed.
"=====[ Setup ]=====
" the mocks assume a project structure
    " proj/tags
    " proj/test/test_runner.c
    " proj/src/LUT.c
    " proj/src/LUT.h
    " proj/src/DOL.c
    " proj/src/DOL.h
    " proj/test/test_LUT.c
    " proj/test/test_LUT.h
    " proj/test/mock_DOL.c
    " proj/test/mock_DOL.h
" Setup() creates this project structure and fills it with code from
" Teardown() deletes the project. If the project folder is not empty, the user
" is asked for permission. I can set an 'ignore' list to delete without asking
" permission. If the project folder is in the `expectmocking` folder, do not ask
" the user for permission.
" See hardcoded list of ignore folders in fakeproject#DestroyFolders.

"=====[ List of tests of lib `expectmocking` ]=====
    "[x] ThisIsATestFile returns true if current file is a test file.
    "   [x] ThisIsATestFile is false if current file is a src file.
    "[x] NameOfLut returns name of lib under test.
    "[x] NameOfDol returns name of depended on lib.
    "[x] NameOfDol uses macro on Expect line if it is there.
    "   [x] NameOfDol uses include mock line if no macro on Expect line.
    "   [x] NameOfDol fails by returning an empty string.
    "   [x] NameOfDol fails if no macro and more than one include mock line.
    "   [x] NameOfDol fails if no macro and no include mock lines.
    "[x] ThisLineExpectsACall returns true if Expected Call is on current line.
    "   [x] ThisLineExpectsACall returns false if Expected Call is not on current line.
    "[x] ExpectedCallSig returns signature of Expected Call from input string.
    "   [x] ExpectedCallSig returns an empty string if input has no Expected Call.
    "[x] FuncNameAndTypes returns function name and input types from input sig.
    "[x] ReplaceArgsWithTypes returns a string of arg types for the input sig.
    "[ ] MakeSetupRecordType returns the SetupRecord function name for an arg type.
    "   [ ] MakeSetupRecordType prefixes arg type with the string SetupRecord.
    "   [ ] MakeSetupRecordType prefixes type with p for each star in a pointer arg
    "[x] ArgDatatype returns the datatype of the input arg.
    "   [x] ArgDatatype returns undeclared if arg is a magic number.
    "   [x] ArgDatatype returns undeclared if project is missing arg declaration.
    "   [x] ArgDatatype identifies pointer input arguments
    "[x] FnPtrAssign returns code for function pointer assignment.
    "[x] FnPtrImplem returns code for empty function pointer implementation.
    "[x] IsDefined returns true if function is defined in the project.
    "   [x] IsDefined returns false if function not defined in project.
let run_quickly = !fakeproject#Show_changing_in_Nerdtree()
let s:this_test_file = expand("%:p")
let s:results_save_path = fnamemodify(s:this_test_file, ":h").'/test-results.md'
"It does not matter what the pwd is.
"`test-results.md` is saved here:
" /home/Mike/.vim/pack/bundle/dev/expectmocking/test-results.md
" Open the test results in Chrome locally:
" PS> chrome C:\cygwin64\home\Mike\.vim\pack\bundle\dev\expectmocking\test-results.md
" Or from the remote repo:
" PS> chrome https://bitbucket.org/rainbots/unit-test-vim/src/master/expectmocking/test-results.md
let s:project = expand("%:h") . '/project/'
let s:save_dir = getcwd()

function! s:SetupFakeProject() dict
    "Setup a fake project with all code and a tags file.
    let self.project = fakeproject#NewProject('project')
    if !self.project.Make(g:run_quickly)    | return 0 | endif
    return 1
endfunction
function! s:TeardownFakeProject() dict
    function! s:InTheRightFile(expected_file)
        call assert_equal(a:expected_file, expand("%:p"))
        if expand("%:p") !=# a:expected_file | return 0 | endif
        return 1
    endfunction
    silent execute 'cd ' s:save_dir
    "if getcwd() != s:save_dir | return 0 | endif
    silent execute "noswapfile edit " . s:this_test_file
    if !s:InTheRightFile(s:this_test_file) | return 0 | endif
    silent call self.project.Clean(g:run_quickly)
    unlet self.project
    return 1
endfunction

"=====[ tests ]=====

function! IsDefined_returns_true_if_function_is_defined_in_the_project()
    if !fakeproject#GoToFile(s:project.'test/test_LUT.c') | return | endif
    call fakeproject#CdProjectDir(s:project)
    let fname = 'DOF2'
    call assert_true(expectmocking#IsDefined(fname))
endfunction
function! IsDefined_returns_false_if_function_not_defined_in_project()
    if !fakeproject#GoToFile(s:project.'test/test_LUT.c') | return | endif
    call fakeproject#CdProjectDir(s:project)
    let fname = 'DOF3'
    call assert_false(expectmocking#IsDefined(fname))
endfunction
function! s:Testgroup_IsDefined()
    let tests = testgroup#New('Testing lib: `expectmocking` function: `IsDefined()`')
    let tests.Setup = function('s:SetupFakeProject')
    let tests.Teardown = function('s:TeardownFakeProject')
    call tests.RunTest('IsDefined_returns_true_if_function_is_defined_in_the_project')
    call tests.RunTest('IsDefined_returns_false_if_function_not_defined_in_project')
    call tests.ViewTestResults(s:results_save_path)
    call s:AddTestGroup(s:all_tests, tests)
endfunction

function! FnPtrImplem_returns_code_for_empty_function_pointer_implementation()
    let dof={} | let dof.name = 'DOF' | let dof.argtypes = '(uint8_t)'
    call assert_equal(
        \['static void DOF_Implementation(uint8_t)', '{}'],
        \expectmocking#FnPtrImplem(dof)
        \)
endfunction
function! s:Testgroup_FnPtrImplem()
    let tests = testgroup#New('Testing lib: `expectmocking` function: `FnPtrImplem()`')
    let tests.Setup = function('s:SetupFakeProject')
    let tests.Teardown = function('s:TeardownFakeProject')
    call tests.RunTest('FnPtrImplem_returns_code_for_empty_function_pointer_implementation')
    call tests.ViewTestResults(s:results_save_path)
    call s:AddTestGroup(s:all_tests, tests)
endfunction

function! FnPtrAssign_returns_code_for_function_pointer_assignment()
    "let list_fname_ftypes = ['DOF', '(uint8_t)']
    let dof={} | let dof.name = 'DOF' | let dof.argtypes = '(uint8_t)'
    call assert_equal(
        \['void (*DOF)(uint8_t) = DOF_Implementation;'],
        \expectmocking#FnPtrAssign(dof)
        \)
endfunction
function! s:Testgroup_FnPtrAssign()
    let tests = testgroup#New('Testing lib: `expectmocking` function: `FnPtrAssign()`')
    let tests.Setup = function('s:SetupFakeProject')
    let tests.Teardown = function('s:TeardownFakeProject')
    call tests.RunTest('FnPtrAssign_returns_code_for_function_pointer_assignment')
    call tests.ViewTestResults(s:results_save_path)
    call s:AddTestGroup(s:all_tests, tests)
endfunction

function! ArgDatatype_returns_the_datatype_of_the_input_arg()
    "Enter the project to use the tags file.
    call fakeproject#CdProjectDir(s:project)
    let arg = 'FtCmd_Read'
    call assert_equal(
        \'uint8_t',
        \expectmocking#ArgDatatype(arg)
        \)
endfunction
function! ArgDatatype_returns_undeclared_if_arg_is_a_magic_number()
    "Enter the project to use the tags file.
    call fakeproject#CdProjectDir(s:project)
    let arg = '0xFF'
    call assert_equal(
        \'undeclared',
        \expectmocking#ArgDatatype(arg)
        \)
endfunction
function! ArgDatatype_returns_undeclared_if_project_is_missing_arg_declaration()
    "Enter the project to use the tags file.
    call fakeproject#CdProjectDir(s:project)
    let arg = 'my_declaration_is_missing'
    call assert_equal(
        \'undeclared',
        \expectmocking#ArgDatatype(arg)
        \)
endfunction
function! ArgDatatype_identifies_pointer_input_arguments()
    "Enter the project to use the tags file.
    "=====[ move to fake project code ]=====
    "uint8_t read_buffer = 0x00;
    "uint8_t *read_buffer_address = &read_buffer;
    "Expect_FtRead(read_buffer_address);
    call fakeproject#CdProjectDir(s:project)
    let arg = 'read_buffer_address'
    call assert_equal(
        \'uint8_t *',
        \expectmocking#ArgDatatype(arg)
        \)
endfunction
function! s:Testgroup_ArgDatatype()
    let tests = testgroup#New('Testing lib: `expectmocking` function: `ArgDatatype()`')
    let tests.Setup = function('s:SetupFakeProject')
    let tests.Teardown = function('s:TeardownFakeProject')
    call tests.RunTest('ArgDatatype_returns_the_datatype_of_the_input_arg')
    call tests.RunTest('ArgDatatype_returns_undeclared_if_arg_is_a_magic_number')
    call tests.RunTest('ArgDatatype_returns_undeclared_if_project_is_missing_arg_declaration')
    call tests.RunTest('ArgDatatype_identifies_pointer_input_arguments')
    call tests.ViewTestResults(s:results_save_path)
    call s:AddTestGroup(s:all_tests, tests)
endfunction

function! ReplaceArgsWithTypes_returns_a_string_of_arg_types_for_the_input_sig()
    call fakeproject#CdProjectDir(s:project)
    let function_signature = 'DOF(FtCmd_Read)'
    call assert_equal(
        \'(uint8_t)',
        \expectmocking#ReplaceArgsWithTypes(function_signature)
        \)
endfunction
function! s:Testgroup_ReplaceArgsWithTypes()
    let tests = testgroup#New('Testing lib: `expectmocking` function: `ReplaceArgsWithTypes()`')
    let tests.Setup = function('s:SetupFakeProject')
    let tests.Teardown = function('s:TeardownFakeProject')
    call tests.RunTest('ReplaceArgsWithTypes_returns_a_string_of_arg_types_for_the_input_sig')
    call tests.ViewTestResults(s:results_save_path)
    call s:AddTestGroup(s:all_tests, tests)
endfunction

function! MakeSetupRecordType_prefixes_arg_type_with_SetupRecord()
    call fakeproject#CdProjectDir(s:project)
    let arg_type = 'uint8_t'
    call assert_equal(
        \'SetupRecord_uint8_t',
        \expectmocking#MakeSetupRecordType(arg_type)
        \)
endfunction
function! MakeSetupRecordType_prefixes_type_with_p_for_each_star_in_a_pointer_arg()
    call fakeproject#CdProjectDir(s:project)
    let arg_type = 'uint8_t *'
    call assert_equal(
        \'SetupRecord_p_uint8_t',
        \expectmocking#MakeSetupRecordType(arg_type)
        \)
    let arg_type = 'uint8_t **'
    call assert_equal(
        \'SetupRecord_pp_uint8_t',
        \expectmocking#MakeSetupRecordType(arg_type)
        \)
endfunction
function! s:Testgroup_MakeSetupRecordType()
    let tests = testgroup#New('Testing lib: `expectmocking` function: `MakeSetupRecordType()`')
    let tests.Setup = function('s:SetupFakeProject')
    let tests.Teardown = function('s:TeardownFakeProject')
    call tests.RunTest('MakeSetupRecordType_prefixes_arg_type_with_SetupRecord')
    call tests.RunTest('MakeSetupRecordType_prefixes_type_with_p_for_each_star_in_a_pointer_arg')
    call tests.ViewTestResults(s:results_save_path)
    call s:AddTestGroup(s:all_tests, tests)
endfunction

"On hold until ReplaceArgsWithTypes is available
function! FuncNameAndTypes_returns_function_name_and_input_types_from_input_sig()
    call fakeproject#CdProjectDir(s:project)
    let function_signature = 'DOF(FtCmd_Read)'
    call assert_equal(
        \['DOF','(uint8_t)'],
        \expectmocking#FuncNameAndTypes(function_signature)
        \)
endfunction
function! s:Testgroup_FuncNameAndTypes()
    let tests = testgroup#New('Testing lib: `expectmocking` function: `FuncNameAndTypes()`')
    let tests.Setup = function('s:SetupFakeProject')
    let tests.Teardown = function('s:TeardownFakeProject')
    call tests.RunTest('FuncNameAndTypes_returns_function_name_and_input_types_from_input_sig')
    call tests.ViewTestResults(s:results_save_path)
    call s:AddTestGroup(s:all_tests, tests)
endfunction

function! ExpectedCallSig_returns_signature_of_Expected_Call_from_input_string()
    let input = 'Expect_DOF(FtCmd_Read);'
    call assert_equal(
        \'DOF(FtCmd_Read)',
        \expectmocking#ExpectedCallSig(input)
        \)
endfunction
function! ExpectedCallSig_returns_an_empty_string_if_input_has_no_Expected_Call()
    let input = 'DOF(FtCmd_Read);'
    call assert_equal('', expectmocking#ExpectedCallSig(input) )
endfunction
function! s:Testgroup_ExpectedCallSig()
    let tests = testgroup#New('Testing lib `expectmocking` function `ExpectedCallSig()`')
    call tests.RunTest('ExpectedCallSig_returns_signature_of_Expected_Call_from_input_string')
    call tests.RunTest('ExpectedCallSig_returns_an_empty_string_if_input_has_no_Expected_Call')
    call tests.ViewTestResults(s:results_save_path)
    call s:AddTestGroup(s:all_tests, tests)
endfunction

function! ThisLineExpectsACall_returns_true_if_Expected_Call_is_on_current_line()
    if !fakeproject#GoToFile(s:project.'test/test_LUT.c') | return | endif
    "Put cursor on line with expected call.
    call search('Expect_')
    call assert_true(expectmocking#ThisLineExpectsACall())
endfunction
function! ThisLineExpectsACall_returns_false_if_Expected_Call_is_not_on_current_line()
    if !fakeproject#GoToFile(s:project.'test/test_LUT.c') | return | endif
    "Put cursor on line with expected call.
    call search('Expect_')
    execute 'normal! k'
    call assert_false(expectmocking#ThisLineExpectsACall())
endfunction
function! s:Testgroup_ThisLineExpectsACall()
    let tests = testgroup#New('Testing lib `expectmocking` function `ThisLineExpectsACall()`')
    let tests.Setup = function('s:SetupFakeProject')
    let tests.Teardown = function('s:TeardownFakeProject')
    call tests.RunTest('ThisLineExpectsACall_returns_true_if_Expected_Call_is_on_current_line')
    call tests.RunTest('ThisLineExpectsACall_returns_false_if_Expected_Call_is_not_on_current_line')
    call tests.ViewTestResults(s:results_save_path)
    call s:AddTestGroup(s:all_tests, tests)
endfunction

function! ThisIsATestFile_returns_true_if_current_file_is_a_test_file()
    "Go to a test file.
    if !fakeproject#GoToFile(s:project.'test/test_LUT.c') | return | endif
    call assert_true(expectmocking#ThisIsATestFile())
endfunction
function! ThisIsATestFile_returns_false_if_current_file_is_a_src_file()
    "Go to a file that is not a test file.
    if !fakeproject#GoToFile(s:project.'src/LUT.c') | return | endif
    call assert_false(expectmocking#ThisIsATestFile())
endfunction
function! s:Testgroup_ThisIsATestFile()
    let tests = testgroup#New('Testing lib `expectmocking` function `ThisIsATestFile()`')
    let tests.Setup = function('s:SetupFakeProject')
    let tests.Teardown = function('s:TeardownFakeProject')
    call tests.RunTest('ThisIsATestFile_returns_true_if_current_file_is_a_test_file')
    call tests.RunTest('ThisIsATestFile_returns_false_if_current_file_is_a_src_file')
    call tests.ViewTestResults(s:results_save_path)
    call s:AddTestGroup(s:all_tests, tests)
endfunction

function! NameOfLut_returns_name_of_lib_under_test()
    if !fakeproject#GoToFile(s:project.'test/test_LUT.c') | return | endif
    call assert_equal('LUT', expectmocking#NameOfLut())
endfunction
function! s:Testgroup_NameOfLut()
    let tests = testgroup#New('Testing lib `expectmocking` function `NameOfLut()`')
    let tests.Setup = function('s:SetupFakeProject')
    let tests.Teardown = function('s:TeardownFakeProject')
    call tests.RunTest('NameOfLut_returns_name_of_lib_under_test')
    call tests.ViewTestResults(s:results_save_path)
    call s:AddTestGroup(s:all_tests, tests)
endfunction

function! NameOfDol_uses_macro_on_Expect_line_if_it_is_there()
    if !fakeproject#GoToFile(s:project.'test/test_LUT.c') | return | endif
    "expectmocking/project/test/test_LUT.c
    call fakeproject#CdProjectDir(s:project)
    silent call search('Expect_')
    "
    let macro = '_MOCK_DOL2_H'
    call assert_equal(macro, matchstr(getline('.'), macro) )
    call assert_equal('DOL2', expectmocking#NameOfDol())
endfunction
function! NameOfDol_uses_include_mock_line_if_no_macro_on_Expect_line()
    "Eliminate the other #include mock:
    if !fakeproject#GoToFile(s:project.'test/test_LUT.c') | return | endif
    call fakeproject#CdProjectDir(s:project)
    call search('Expect_')
    "
    call search('#include "mock_DOL2.h"', 'w')
    execute 'delete'
    execute 'w'
    "Put cursor on second Expect call.
    call search('Expect_')
    call search('Expect_')
    call assert_equal('DOL', expectmocking#NameOfDol())
endfunction
function! NameOfDol_fails_by_returning_an_empty_string()
    "Note this is identical to the next test. I'm just listing the specs.
    return 0
    "
    if !fakeproject#GoToFile(s:project.'test/test_LUT.c') | return | endif
    call fakeproject#CdProjectDir(s:project)
    call search('Expect_')
    "
    "Go to the Expect_ line without the macro.
    call search('Expect_')
    call assert_equal('', expectmocking#NameOfDol())
endfunction
function! NameOfDol_fails_if_no_macro_and_more_than_one_include_mock_line()
    if !fakeproject#GoToFile(s:project.'test/test_LUT.c') | return | endif
    call fakeproject#CdProjectDir(s:project)
    call search('Expect_')
    "
    "Go to the Expect_ line without the macro.
    call search('Expect_')
    call assert_equal('', expectmocking#NameOfDol())
endfunction
function! NameOfDol_fails_if_no_macro_and_no_include_mock_lines()
    if !fakeproject#GoToFile(s:project.'test/test_LUT.c') | return | endif
    call fakeproject#CdProjectDir(s:project)
    "
    "Eliminate the #include mock lines:
    call search('#include "mock_DOL.h"', 'w')
    execute 'delete'
    call search('#include "mock_DOL2.h"', 'w')
    execute 'delete'
    execute 'w'
    "Go to the Expect_ line without the macro.
    call search('Expect_')
    call search('Expect_')
    call assert_equal('', expectmocking#NameOfDol())
endfunction
function! s:Testgroup_NameOfDol()
    let tests = testgroup#New('Testing lib `expectmocking` function `NameOfDol()`')
    let tests.Setup = function('s:SetupFakeProject')
    let tests.Teardown = function('s:TeardownFakeProject')
    call tests.RunTest('NameOfDol_uses_macro_on_Expect_line_if_it_is_there')
    call tests.RunTest('NameOfDol_uses_include_mock_line_if_no_macro_on_Expect_line')
    call tests.RunTest('NameOfDol_fails_by_returning_an_empty_string')
    call tests.RunTest('NameOfDol_fails_if_no_macro_and_more_than_one_include_mock_line')
    call tests.RunTest('NameOfDol_fails_if_no_macro_and_no_include_mock_lines')
    call tests.ViewTestResults(s:results_save_path)
    call s:AddTestGroup(s:all_tests, tests)
endfunction

"=====[ test runner ]=====
function! s:TestRunner()
    function! s:AddTestGroup(total, testgroup)
        "Called by each test group before being popped off the stack.
        let a:total.number_tested += a:testgroup.number_tested
        let a:total.number_passed += a:testgroup.number_passed
        let a:total.number_failed += a:testgroup.number_failed
    endfunction
    "Clear out test results file.
    call writefile(['# Test Results'], s:results_save_path)
    let s:all_tests = testgroup#New('Summary for all tests')
        "See :h script-variable
    "Run tests.
    "call s:Testgroup_IsDefined()
    "call s:Testgroup_FnPtrImplem()
    "call s:Testgroup_FnPtrAssign()
    "call s:Testgroup_ArgDatatype()
    call s:Testgroup_ReplaceArgsWithTypes()
    call s:Testgroup_MakeSetupRecordType()
    "call s:Testgroup_FuncNameAndTypes()
    "call s:Testgroup_ExpectedCallSig()
    "call s:Testgroup_ThisLineExpectsACall()
    "call s:Testgroup_ThisIsATestFile()
    "call s:Testgroup_NameOfLut()
    "call s:Testgroup_NameOfDol()
    "View the summary for all tests.
    call s:all_tests.ViewTestResults(s:results_save_path)
"unlet all_tests
endfunction

call s:TestRunner()

"TODO: refactor test runner into an object
    "Here is a rough sketch of the idea
    "goal: pull boilerplate out of each test group
    "that boilerplate goes in a single `AddGroup` function called by test_runner
    "let test_runner = {}
    "let test_runner.Testgroups = []
    ""Clear out test results file.
    "call writefile(['# Test Results'], s:results_save_path)
    "let s:all_tests = testgroup#New('Summary for all tests')
    "let test_runner.AddTestgroup('s:Testing_BlahFunc'group_name)
    "call add(test_runner.Testgroups, function('s:Testgroup_BlahFunc'))
    "call add(test_runner.Testgroups, function('s:Testgroup_BlahFunc'))

