#ifndef _MOCK_DOL2_H
#define _MOCK_DOL2_H

#include <stdint.h>   // uint8_t

void Expect_DOF2(uint8_t arg1);
void DOF2_Stubbed(uint8_t arg1);

#endif // _MOCK_DOL2_H
