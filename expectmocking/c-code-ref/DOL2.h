#ifndef _DOL2_H
#define _DOL2_H

extern uint8_t const FtCmd_Read2;
extern void (*DOF2)(uint8_t FtCmd);

#endif // _DOL2_H
