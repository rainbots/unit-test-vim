#ifndef _MOCK_DOL_H
#define _MOCK_DOL_H

#include <stdint.h>   // uint8_t

void Expect_DOF(uint8_t arg1);
void DOF_Stubbed(uint8_t arg1);

#endif // _MOCK_DOL_H
