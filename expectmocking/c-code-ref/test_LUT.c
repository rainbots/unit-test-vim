#include "test_LUT.h"           // header for this file
#include "test_LUT_MockUps.h"   // mockups for testing each FUT in FUT lib
#include <LUT.h>                // FUT lib
#include <DOL.h>                // DOF lib
#include <DOL2.h>               // DOF2 lib
#include "mock_DOL.h"           // mocked version of DOF lib
#include "mock_DOL2.h"          // mocked version of DOF2 lib
#include <Mock.h>               // RanAsHoped, WhyDidItFail
#include <unity.h>              // all the TEST_blah macros

void SetUp_FUT(void){
    SetUpMock_FUT();    // create the mock object to record calls
    // other setup code
}
void TearDown_FUT(void){
    TearDownMock_FUT(); // destroy the mock object
    // other teardown code
}
void FUT_Read_does_entire_command_phase_for_ReadCmd(void)
{
    //Example of what to do if there is more than one mocked lib:
    _MOCK_DOL2_H; Expect_DOF2(FtCmd_Read);
    Expect_DOF(FtCmd_Read);
    //=====[ Operate ]=====
    FUT(FtCmd_Read);
    TEST_ASSERT_TRUE_MESSAGE(
        RanAsHoped(mock),           // If this is false,
        WhyDidItFail(mock)          // print this message.
        );
}
void UsbRead_should_read_until_buffer_is_empty(void)
{
    //=====[ Mock-up values returned by stubbed functions ]=====
    FtBusTurnaround_StubbedReturnValue = true;
    FtRead_StubbedReturnValue = false;
    // mock-up one value for MIOSIO port to return
    //=====[ Set expectations ]=====
    Expect_FtSendCommand(FtCmd_Read);
    Expect_FtBusTurnaround();
    uint8_t read_buffer = 0x00;
    uint8_t *read_buffer_address = &read_buffer;
    Expect_FtRead(read_buffer_address);
    Expect_FtDeactivateInterface();
    //=====[ Operate ]=====
    UsbRead();
    //=====[ Test ]=====
    TEST_ASSERT_TRUE_MESSAGE(
        RanAsHoped(mock),           // If this is false,
        WhyDidItFail(mock)          // print this message.
        );
}
