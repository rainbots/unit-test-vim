#include "test_LUT_MockUps.h"       // SetUpMock_FUT, TearDownMock_FUT
#include "DOL.h"                    // lib DOF
#include "DOL2.h"                   // lib DOF2
#include "mock_DOL.h"               // lib DOF_Stubbed
#include "mock_DOL2.h"              // lib DOF2_Stubbed
#include <Mock.h>                   // Class: Mock

static void (*DOF_Saved)(uint8_t FtCmd);
static void Stub_DOF(void) {
    DOF_Saved = DOF;
    DOF = DOF_Stubbed;
}
static void Unstub_DOF(void) {
    DOF = DOF_Saved;
}

static void (*DOF2_Saved)(uint8_t FtCmd);
static void Stub_DOF2(void) {
    DOF2_Saved = DOF2;
    DOF2 = DOF2_Stubbed;
}
static void Unstub_DOF2(void) {
    DOF2 = DOF2_Saved;
}
void SetUpMock_FUT(void)
{
    mock = Mock_new();
    //
    Stub_DOF();
    Stub_DOF2();
}
void TearDownMock_FUT(void)
{
    Unstub_DOF();
    Unstub_DOF2();
    //
    Mock_destroy(mock); mock=NULL;
}


