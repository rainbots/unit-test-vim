# Table of Contents
- [Repo links](#markdown-header-repo-links)
- [Usage for the real deal](#markdown-header-usage-for-the-real-deal)
    - [Paths](#markdown-header-paths)
        - [runtimepath](#markdown-header-runtimepath)
        - [dev is where I develop Vim scripts](#markdown-header-dev-is-where-i-develope-vim-scripts)
        - [Vim library scripts or libs](#markdown-header-vim-library-scripts-or-libs)
        - [Vim OOP](#markdown-header-vim-oop)
        - [autoload on demand](#markdown-header-autoload-on-demand)
    - [unit test libs](#markdown-header-unit-test-libs)
        - [vimtdd is now testgroup](#markdown-header-vimtdd-is-now-testgroup)
        - [fakeproject](#markdown-header-fakeproject)
- [Idioms](#markdown-header-idioms)
    - [if function pipes endif](#markdown-header-if-function-pipes-endif)
    - [log a failed test](#markdown-header-log-a-failed-test)
    - [old failed test logging before I pulled in Setup and Teardown](#markdown-header-old-failed-test-logging-before-i-pulled-in-setup-and-teardown)

---e-n-d---

# Usage for the real deal
## Paths
### runtimepath
I *git clone* Vim plugins into my `~/.vim/pack/bundle/start` folder:
```bash
/home/Mike/.vim/pack/bundle/start
```
Vim automatically adds folders in `start` to the `runtimepath` *option*. (I call
it an *option* because it can be used in an expression with the syntax
`&runtimepath`, see `:h 41.3`).

My `start` folder has the plugins
`vim-markdown`, `tabulous`, and `nerdtree`, so when I query my `runtimepath`:
```vim
set runtimepath
```
or
```vim
echo &runtimepath
```
I get this:
```vim
runtimepath=
~/.vim,
~/.vim/pack/bundle/start/vim-markdown,
~/.vim/pack/bundle/start/tabulous,
~/.vim/pack/bundle/start/nerdtree,
/usr/share/vim/vimfiles,
/usr/share/vim/vim80,
/usr/share/vim/vimfiles/after,
~/.vim/after,
~/.vim/pack/bundle/dev
```
### dev is where I develop Vim scripts
That last path is where I develop Vim scripts:
```bash
/home/Mike/.vim/pack/bundle/dev
```
It is the only `runtimepath` I explicitly have to add in my `.vimrc`:
```vim
set runtimepath+=~/.vim/pack/bundle/dev
```
My scripts in `dev` are not robust plugins. I am the sole user, so I do not care
about making them robust. But they are basically plugins, just in a different
path. From hereon, I'm going to call these my *libs*, short for *Vim library
scripts*.
### Vim library scripts or libs
Splitting a `.vimrc` into multiple *libs* raises the possibility of namespace
conflicts: two functions or global variables that use the same name. The *Vim*
solution to namespace conflicts is to name all public symbols in `foo.vim` with
the prefix `foo#`:

=====[ `file.vim` ]=====
```vim
function! file#New(filepath)
    " Creates the file in a:filepath.
endfunction
```
=====[ `folder.vim` ]=====
```vim
function! folder#New(path)
    " Creates the folder in a:path.
endfunction
```
Then clients of `file.vim` and `folder.vim` call `file#New(filepath)` or
`folder#New(path)`. There is no conflict because the prefix is the filename. I
keep all of my *libs* in the same `dev` folder so they cannot possibly have the
same name.

Anything that is not public is private to the script and gets the `s:` prefix to
limit its scope to the script:

=====[ `folder.vim` ]=====
```vim
function! s:FolderExists(folder)
    " Returns true if a:folder exists.
endfunction
```
But if a public function like `folder#New(path)` is called from another script,
it cannot call script-scoped functions like `s:FolderExists(folder)` because
that function definition is not loaded.

So all symbols end up being public for *libs*. A *lib* is not meant to run on
its own. If it runs on its own, I call it a *script*. A *lib* is a cohesive
collection of functions for a client *script* to call.

I put an underscore `_` prefix in front of the symbol name to show it is not
intended for use by clients. The `_` functions are my plumbing, the rest are
porcelain. Use the porcelain.
### Vim OOP
The `foo#` naming gets awkward. *Vim* supports an OOP style. This is similar to
creating an object in `C`, but it is vastly improved in three ways:

- *Vim* has a `self` keyword for functions to refer to the object they are
  associated with
- *Vim* is dynamically typed, so the class definition only needs function names
- *Vim* mostly handles memory management

Here is a real example:

=====[ `vimtdd.vim` ]=====
```vim
function! vimtdd#NewTestGroup()
    "USAGE: let tests = vimtdd#NewTestGroup
    let TestGroup = {
        \'_RecordThisTest'      : function('vimtdd#_RecordThisTest'),
        \'_EchoResults'         : function('vimtdd#_EchoResults'),
        \'ViewTestResults'      : function('vimtdd#ViewTestResults'),
        \'SaveResultsToFile'    : function('vimtdd#SaveResultsToFile'),
        \'RunTest'              : function('vimtdd#RunTest')
        \}
    let TestGroup.failed = []
    let TestGroup.number_tested = 0
    let TestGroup.number_passed = 0
    let TestGroup.number_failed = 0
    return TestGroup
endfunction
```
The script file acts as a *class* definition. This is not the best example since
my script file and my *class* are different names, but whatever.

`vimtdd#NewTestGroup` creates a new `TestGroup`. If I didn't goof the naming,
this would be just `foo#New` or `foo#Create` for defining class `Foo` in a file
called `foo.vim`.

A `TestGroup` has:

- `failed`: a list of strings to hold each failure message logged to `v:errors`
- three numbers to record total number tested, passed, and failed
- functions

The data members are not a surprise except maybe for the notation. *Vim* syntax
for dict key/value pairs allows this *dict.key = value* style, and using that
style makes sense in a class definition.

The functions are the same idea as putting function pointers in a `C` struct,
only it's way easier because *Vim script* is dynamically typed. All you need in
the class definition is the function name. There is no mention of what the
function returns or what arguments it takes.

A client instantiates a `Foo` with `let bar = foo#New()`. After that, the
functions use the dot notation: `call tests.SaveResultsToFile()`. This is more
than just aesthetics or organization, it is true OOP. The function definitions
can refer to the `dict` they are members of using keyword `self`. Again, `C`
cannot do this.

An example using `self`, note the function name needs the trailing `dict` to
use `self` in the definition:

=====[ `vimtdd.vim` ]=====
```vim
function! vimtdd#_EchoResults() dict
    for line in self.failed
        echomsg line
    endfor
    echomsg '------------------'
    echomsg self.number_tested 'Tests' self.number_failed 'Failures'
    "Example:
    "------------------
    "1 Tests 0 Failures
endfunction
```

If a client calls `let bar = foo#New()` within a function, `Foo` instance `bar`
it is automatically deallocated from memory when the function exits.

If the client instantiates `Foo` outside of a function definition, the client
has to explicitly deallocate at the end of the script, but this is a simple
one-liner: `unlet bar`. No arguments. No giant pointer chain of heap locations
like in `C`.

### autoload on demand
I put all my `libs` in an `autoload` folder:
```bash
/home/Mike/.vim/pack/bundle/dev/autoload
```
`autoload` is a special folder name. See `:h write-library-script`.

When a client references a function with `foo#func()` syntax, *Vim* looks
through the `runtimepath` for folders named `autoload`. *Vim* looks through the
`.vim` files in each `autoload` folder until it finds this function definition.
Vim loads that function *on demand* when the call is made.

You do not have to source the lib file for Vim to find the function! My `.vimrc`
is not full of lines like `source ~/.vim/pack/bundle/dev/blah.vim`. I reserve
explicit sourcing for `.vim` files that contains key-maps and options.

If you edit the function definition, manually source that lib file for the
current session to pick up the change. The *on demand* loading only sources the
function once, then it is in memory until *Vim* exits.

## unit test libs
### vimtdd is now testgroup
~~The main unit test lib is `vimtdd.vim`.~~
The main unit test lib is `testgroup.vim`.

Here is a simple test file example:

=====[ `dev/autoload/tests/test_file.vim` ]=====
```vim

execute 'source autoload/file.vim'

"=====[ List of tests ]=====
    "[x] New returns 0 if filepath directory does not exist.
    "[ ] New creates a file if the folder exists.
    "[ ] New creates nothing if filepath directory does not exist.

"=====[ tests ]=====
function! New_returns_0_if_filepath_directory_does_not_exist()
    " Setup
    let fake = fakeproject#NewProject('project')
    let filepath = fake.Toplevel() . '/blah.c'
    " Operate and Test
    call assert_false(file#New(filepath))
    " Teardown
endfunction

"=====[ test runner ]=====
let tests = vimtdd#NewTestGroup()
call tests.RunTest('New_returns_0_if_filepath_directory_does_not_exist')
call tests.ViewTestResults()
call tests.SaveResultsToFile('./autoload/tests/test-results.md')
unlet tests
```
And the `lib` being tested:

=====[ `dev/autoload/file.vim` ]=====
```vim
function! file#New(path)
    "Create a new file.
    "Do nothing and return 0 if `path` directory does not exist.
    if !folder#Exists(fnamemodify(a:path, ":h"))
        return 0
    endif
    let empty_file = []
    call writefile(empty_file, a:path)
    return 1
endfunction
```
The test calls `assert_false`. If the assertion fails, a failure message is
logged as a string in list `v:errors`. The message follows the form
`Expected {asserted_value} but got {actual_value}`, like this:
```vim
Expected False but got 1
```
That is all *out of the box Vim* so far.

Building off of Vim's built-in `assert_` and `v:errors`, `vimtdd.vim`
defines a `TestGroup` class to run tests and record the results for display:

- `tests.RunTest('New_returns_0_if_filepath_directory_does_not_exist')`:
    - clears `v:errors` in preparation for logging the failure message
    - creates a `funcref` using the test name to then call the test function
    - the test function calls `assert_false` resulting in a failure message in
      `v:errors` if it failed or leaving `v:errors` empty if it passed
    - the `test.number_tested` is incremented to record that a test was done
    - *pass* or *fail* is determined by looking at `v:errors`
    - `test.number_failed` or `test.number_passed` is incremented
    - if it failed, the message is augmented with the test name and the word
      `FAIL`
    - the *failed* message is logged in the list `tests.failed`

An example *failed* message:
```vim
New_returns_0_if_filepath_directory_does_not_exist:FAIL: Expected False but got 1
```

- `tests.ViewTestResults()`
    - echoes each logged *failed* message from list `tests.failed`
    - ends with a summary showing the total number of tests and failures

Example test results:
```vim

------------------
1 Tests 0 Failures
```
Here there were no failures, so there is nothing but the summary. And to get rid
of any old message garbage, I cleared the messages first with:
```vim
:messages clear
```
### fakeproject
Looking back at the list of tests from the previous example, the last test
requires *Vim* interact with the filesystem.

=====[ `dev/autoload/tests/test_file.vim` ]=====
```vim
"=====[ List of tests ]=====
    "[x] New returns 0 if filepath directory does not exist.
    "[ ] New creates nothing if filepath directory does not exist.
    "[ ] New creates a file if the folder exists.
```
`fakeproject.vim` provides a `FakeProject` class for:

- making and cleaning projects
    - includes refreshing the *NERDTree* view
- copying contents between files
- opening files
- changing Vim's present working directory

`fakeproject.vim` has a `Make()` function that defines a whole `C` project with
`test` and `src` folders, but in this case, all that is needed is a single
folder to operate `file#New(filepath)`. Still, `fakeproject.vim` makes this
convenient by providing a consistent path to the `Toplevel` folder with
`Toplevel()`.

=====[ `dev/autoload/tests/test_file.vim` ]=====
```vim
function! New_creates_a_file_if_the_folder_exists()
    " Setup
    let fake = fakeproject#NewProject('project')
    call folder#Create(fake.Toplevel())
    let filepath = fake.Toplevel() . '/blah.c'
    " Operate
    call file#New(filepath)
    " Test
    call assert_true(file#Exists(filepath))
    " Teardown
    call s:Teardown_Testing_New(fake)
endfunction
```

*Teardown* calls `fakeproject.vim` function `Clean()`. As the names `Make` and
`Clean` imply for anyone familiar with Makefiles, `Clean()` deletes temporary
garbage created by `Make()`. For `fakeproject.vim`, everything created by
`Make()` is temporary garbage that is no longer needed after the test runs, so
`Clean()` deletes the entire project.

=====[ `dev/autoload/tests/test_file.vim` ]=====
```vim
function! s:Teardown_Testing_New(Project)
    call a:Project.Clean(g:run_quickly)
endfunction
```
The global `g:run_quickly` just tells `Clean` not to bother refreshing the
*NERDTree* view. Seeing it refresh is reassuring when the test *Setup* and
*Teardown* are first wired up. This shows that the fake project is created where
expected. But each refresh takes time. It is annoying when running multiple
tests and having to wait 5 seconds instead of 0.5 seconds.

### Setup and Teardown
A good unit test framework removes calls to *Setup* and *Teardown* from the
tests. I just added this.

The *test runner* becomes responsible for assigning `tests.Setup()`
and `tests.Teardown()` to the correct `Setup()` and `Teardown()` functions prior
to calling `test.RunTest()` for a particular test.

The testgroup gets a name. The tests end with `ViewTestResults()` which echoes
the but also writes them to file. The `unlet tests` at the end of each group
erases the old tests from memory, but each write to file is an append so they
are all there.

The `Setup()` and `Teardown()` functions are still defined in the test
script, but maybe they can be moved out to sub-folder `fixtures` and then
accessed with beasts like this: `tests#fixtures#test_foo#Setup_BlahFunc()`.

# Idioms

## if function pipes endif
Vim functions return 0 by default.

```vim
function! EchoKitty()
    echo ">^.^<"
endfunction
```
There is no explicit `return` in `EchoKitty()`. Clients just call it:
```vim
call EchoKitty()
```
It always returns zero, so there is no point in doing this:
```vim
let echoed = EchoKitty()
```
Or this:
```vim
if !EchoKitty()
    " do stuff
endif
```

But often a function has more than one execution path. Execution paths fall into
two categories: happy paths where the function succeeds and sad paths where
the function failed to do what the client wanted.

`GoToFile()` has one happy path and one sad path.
```vim
function! GoToFile(filepath)
    if !file#Exists(a:filepath)
        echo assert_report('No file at `' . a:filepath )
        return
    endif
    "File exists. Go to it.
    silent execute "noswapfile edit " . a:filepath
endfunction
```
`GoToFile()` edits the file at `filepath`. This is the happy path. If the
filepath does not exist, `GoToFile()` returns before attempting to edit it. That
is the sad path. Happy and sad is the difference between the caller being in a
file or not, so the caller needs to know which path was taken.

The caller could check if `GoToFile()` succeeded:
```vim
call GoToFile('./examples/examples.vim')
let in_examples = expand("%")
if in_examples
    "do stuff"
endif
```
But that is extra work. Laziness is a virtue for lib code, but `GoToFile()` is
too lazy. Since *Vim* dynamically typed, it is almost no effort for `GoToFile()`
to return true/false to tell the caller if it succeeded or not.

Vim uses `0` for *false* and non-zero for *true*. The convention is to use `1` for
*true*.

Modifying `GoToFile()` slighty:
```vim
function! GoToFile(filepath)
    if !file#Exists(a:filepath)
        echo assert_report('No file at `' . a:filepath )
        return 0
    endif
    "File exists. Go to it.
    silent execute "noswapfile edit " . a:filepath
    return 1
endfunction
```
I added one line: `return 1`. Technically that is the only change that was
necessary to make `GoToFile()` announce whether it succeeds. *Vim* returns `0`
by default, but I made that explicit as a sign that the return value *actually
matters*. Since the intent is for the client to check the return value, it
is good form to not leave any ambiguous `return` statements. 

Now the client code is a little simpler:
```vim
if !GoToFile('./examples/examples.vim')
    " Did not change files. Probably do not want to make any edits. Just return.
    return
endif
"Changed files, now do stuff in the file.
```
The *if block* does not need a comment if the function is named well. It
literally reads *if not GoToFile*. I think it is not a stretch to expect my
future self to read this as *if failed to go to the file*.

Eliminating the comment:
```vim
if !GoToFile('./examples/examples.vim')
    return
endif
"Changed files, now do stuff in the file.
```
This is good but it can be even better. I think `return` and `endif` on their
own lines is noisy. *Vim* allows multiple lines of code on the same line with
`|`:

```vim
if !GoToFile('./examples/examples.vim') | return | endif
```
That's my idiom. The meat of the line is calling `GoToFile()`, but if the call
fails to do what it promises, break out of this code block.

## log a failed test
The test runner instantiates a `TestProject` to call `RunTest` on each test.
`RunTest` calls functions that make `assert_` calls. Failed tests are logged
whenever an `assert_` call fails.

`RunTest` clears `v:errors` at the start of each test. It runs the test.
`assert_` calls are made, possibly some of them fail. If an `assert_` call
fails, *Vim* records the failure in the list `v:errors`. `RunTest` calls
`RecordTest` to record the results of the test. `RecordTest` checks `v:errors`
to see if the test passed or failed. It also saves any failure messages in
`v:errors` before the next test clears `v:errors` again.

The takeaway is that a failed `assert_` causes a failed test. A failed `assert_`
is a failed expectation, not a signpost for sad paths. `assert_` should only be
used in the test code itself, or in the functions called by the test fixtures.

*If a function has any clients that are not test fixtures, do not call `assert_`.*

The proper way to report that the happy path was *not* taken is to write a unit
test that checks for that path, and let the test runner report what happened. To
quickly check an unhappy path while developing, just use echo to check the
logic, then comment out the echo message and make the comment a *TODO*.

Here is an example. This lib function is wrong because it uses `assert_`:
```vim
function! expectmocking#NameOfDol()
    let line_num = search('#include "mock_', 'w')
    if line_num == 0
        assert_report("There are no #include mocked lib headers.")
        return ''
    endif
    " do other stuff and return name of depended on library
```
I wanted to record the reason this function could not do its job. But this is a
sad path, not a failed expectation. There will be times that a program correctly
takes this path. It is not a bug.

The correct first step is to change `assert_report` to `echo`:
```vim
function! expectmocking#NameOfDol()
    let line_num = search('#include "mock_', 'w')
    if line_num == 0
        echo "There are no #include mocked lib headers."
        return ''
    endif
    " do other stuff and return name of depended on library
```
and manually run this to see that the sad path is taken.

The code is good enough to continue developing, but do not leave the codebase
like this! Change the `echo` into a *TODO comment* to flag that there is a new
conditional path that needs a unit test.

```vim
function! expectmocking#NameOfDol()
    let line_num = search('#include "mock_', 'w')
    "TODO: write this unit test
    "NameOfDol returns an empty string if there are no #include mocked lib headers."
    if line_num == 0 | return '' | endif
    " do other stuff and return name of depended on library
```
The unit test documents this behavior. And the documentation is code. If new
code breaks this behavior, this unit test will fail.

---
A test can fail because:

- the code under test fails
- the test fixture fails

Unit tests usually need fixtures, i.e., *Setup* and *Teardown* functions. The
test *should fail* if the fixtures cannot properly *Setup* and *Teardown*. If
*Setup* fails, the test fails before it even runs! If *Teardown* fails, the test
fails even if the actual `assert_` did not fail.

A fixture failure is logged like any other failed test:

- an `assert_` call fails
- the instance of `TestProject` calls `RecordTest`

This is not practical to implement if each test is responsible for its own
*Setup* and *Teardown*. But after I figured out how to handle fixturing at the
test runner, this became practical.

Calling `RecordTest` to catch a failing *Setup* or *Teardown* just required a
change in the logic in `RunTest`:

```vim
function! testgroup#RunTest(test_name) dict
    "Call 'test_name' and log the result in the TestGroup instance.
    let v:errors = []
    let Fref = function(a:test_name)
    if self.Setup() | call Fref()
    else | call testgroup#_LogFailedSetup()    | endif
    if self.Teardown()
    else | call testgroup#_LogFailedTeardown() | endif
    call self.RecordTest(a:test_name)
endfunction
```
If the *Setup* is OK, the test is run. Otherwise, the failed *Setup* is logged.
Eitherway, *Teardown* runs. And if *Teardown* fails, it is logged. My `if/else`
structure for *Teardown* is a little odd, but I found this repetition of the
*Setup* `if/else` was the clearest representation for reading the code.

Earlier I said each item in `v:errors` is reported. I clear `v:errors` at the
start of each test, so `v:errors` usually has only one item. It is
possible to have a second item in `v:errors` if the *Teardown* fails in addition
to a failed *Setup* or a failed test.

Another possibility is that *Setup* or *Teardown* fails (returns 0) without
making any assertion. `testgroup.vim` catches this as the default case. Here is
the default for *Teardown*. *Setup* is identical.

```vim
function! testgroup#_LogFailedTeardown()
    let fail_msg = '**Teardown failed** '
    let default  = '..Teardown failed without any assertion..so no traceback:'
                    \'Cause of failure unknown'
    if len(v:errors) == 0
        let v:errors = [fail_msg . default]
    else
        let v:errors[-1] = '**Teardown failed** ' . v:errors[-1]
    endif
endfunction
```

The intent is for *Setup* and *Teardown* to call another function that does the
assertion and returns 1 or 0. This keeps the *Setup* and *Teardown* functions
short and clear.

An exmaple is this *Teardown* checking if the cursor is back in the original
test file.
```vim
function! s:InTheRightFile(expected_file)
    call assert_equal(a:expected_file, expand("%:p"))
    if expand("%:p") !=# a:expected_file | return 0 | endif
    return 1
endfunction
function! s:Teardown_AllTests() dict
    silent execute 'cd ' g:save_dir
    "if getcwd() != g:save_dir | return 0 | endif
    silent execute "noswapfile edit " . g:this_test_file
    if !s:InTheRightFile(g:this_test_file) | return 0 | endif
    silent call self.project.Clean(g:run_quickly)
    unlet self.project
    return 1
endfunction
```
The *Teardown* calls a function that does the assertion and provides a return
value for *Teardown* to use the `if !DoThing() | return 0 | endif` idiom.

The takeaway is that if a test fixture calls a function by checking if it fails,
that function should also handle doing an assert so that when the fail is picked
up by `RunTest`, the failure is logged with a useful failed assertion message.

I ran an example of all possible passing/failing test with passing/failing Setup
and Teardown here:
```bash
/home/Mike/.vim/pack/bundle/dev/examples/test_examples.vim
```
### Test results output by examples

> # Test group: No *Setup* or *Teardown*
> 
> - *Test #1*:`ReturnsTrue_returns_true` **PASS**
> - *Test #2*:`ReturnsTrue_returns_false` **FAIL**: Expected False but got 1.
>     - *Traceback*: `..ReturnsTrue_returns_false line 1`
> 
> ------------------
> 2 Tests 1 Failures
> # Test group: A passing *Setup* and passing *Teardown*
> 
> - *Test #1*:`ReturnsTrue_returns_true` **PASS**
> - *Test #2*:`ReturnsTrue_returns_false` **FAIL**: Expected False but got 1.
>     - *Traceback*: `..ReturnsTrue_returns_false line 1`
> 
> ------------------
> 2 Tests 1 Failures
> # Test group: A failing *Setup* and passing *Teardown*
> 
> - **Setup failed** for *Test #1*:`ReturnsTrue_returns_true`: Expected True but got 0.
>     - *Traceback*: `..<SNR>79_Setup_PickObjectMood_FAIL[5]..obj#DoHardThing line 2`
> - **Setup failed** for *Test #2*:`ReturnsTrue_returns_false`: Expected True but got 0.
>     - *Traceback*: `..<SNR>79_Setup_PickObjectMood_FAIL[5]..obj#DoHardThing line 2`
> 
> ------------------
> 2 Tests 2 Failures
> # Test group: A passing *Setup* and failing *Teardown*
> 
> - **Teardown failed** for *Test #1*:`ReturnsTrue_returns_true`: Expected True but got 0.
>     - *Traceback*: `..<SNR>79_Teardown_ShowObjectMood_FAIL[2]..obj#DoHardThing line 2`
> - *Test #2*:`ReturnsTrue_returns_false` **FAIL**: Expected False but got 1.
>     - *Traceback*: `..ReturnsTrue_returns_false line 1`
> - **Teardown failed** for *Test #2*:`ReturnsTrue_returns_false`: Expected True but got 0.
>     - *Traceback*: `..<SNR>79_Teardown_ShowObjectMood_FAIL[2]..obj#DoHardThing line 2`
> 
> ------------------
> 2 Tests 2 Failures
> # Test group: A failing *Setup* and failing *Teardown*
> 
> - **Setup failed** for *Test #1*:`ReturnsTrue_returns_true`: Expected True but got 0.
>     - *Traceback*: `..<SNR>79_Setup_PickObjectMood_FAIL[5]..obj#DoHardThing line 2`
> - **Teardown failed** for *Test #1*:`ReturnsTrue_returns_true`: Expected True but got 0.
>     - *Traceback*: `..<SNR>79_Teardown_ShowObjectMood_FAIL[2]..obj#DoHardThing line 2`
> - **Setup failed** for *Test #2*:`ReturnsTrue_returns_false`: Expected True but got 0.
>     - *Traceback*: `..<SNR>79_Setup_PickObjectMood_FAIL[5]..obj#DoHardThing line 2`
> - **Teardown failed** for *Test #2*:`ReturnsTrue_returns_false`: Expected True but got 0.
>     - *Traceback*: `..<SNR>79_Teardown_ShowObjectMood_FAIL[2]..obj#DoHardThing line 2`
> 
> ------------------
> 2 Tests 2 Failures

## old failed test logging before I pulled in Setup and Teardown
Unit tests usually need fixtures, i.e., *Setup* and *Teardown* functions. The
test *should fail* if the fixtures cannot properly *Setup* and *Teardown*. If
*Setup* fails, the test fails before it even runs! If *Teardown* fails, the test
fails even if the actual `assert_` did not fail. A fixture failure is logged
like any other test failure.

My approach to this builds on the `if !DoThing() | return | endif` idiom.

My example is tests using `fakeproject.vim`. *Setup* creates folders and files
with `fakeproject#Make()`, and *Teardown* deletes all of that with
`fakeproject#Clean()`. If `Make()` failed, the test will not run properly
because none of the files exist. If `Clean()` failed, the next test will not run
properly because the files already exist and were modified by the previous test.

Using the idiom:
```vim
function! s:Setup_TestsUsingFile_test_LUT(Project)
    if !a:Project.Make(g:run_quickly)    | return 0 | endif
    if !a:Project.GoToFile('test_LUT.c') | return 0 | endif
    return 1
endfunction
```
Refactoring the fixture to remove one line of code:
```vim
function! s:Setup_TestsUsingFile_test_LUT(Project)
    if !a:Project.Make(g:run_quickly) | return 0 | endif
    return a:Project.GoToFile('test_LUT.c')
endfunction
```
This fixture sets up the test for running in a file called `test_LUT.c`. The
fixture has to do two things before it can claim victory:

- create a project with the file to run the test in
- go to that file

The client is the test definition that uses the fixture. The intent is for the
client to check that *Setup* worked before it proceeds with the test:
```vim
function! ThisIsATestFile_returns_true_if_current_file_is_a_test_file()
    let proj = fakeproject#NewProject('project')
    if !s:Setup_TestsUsingFile_test_LUT(proj) | return | endif
    call assert_true(expectmocking#ThisIsATestFile())
    call !s:Teardown_AllTests(proj)
```
If *Setup* fails the test and *Teardown* are skipped. If *Teardown* fails, there
is nothing to skip, so checking for failure does not matter.

But in the test report, I want to know if *Setup* or *Teardown* failed. To log
the test failure, *Setup* and *Teardown* call functions that make `assert_`
calls. This only makes sense in a unit testing context. In other words, only
functions in a lib that supports unit testing, e.g., `fakeproject.vim`.
For the lib under development, I just do a humble `echo` (one day I'll learn
Vim's try/catch and error-handling and do it the right way), and I let the
test-code-client that is calling the lib function do the actual `assert_` call.
Otherwise, I'd end up with tests failing every time a sad path is taken, even if
the sad path was the correct path to take, in which case the test shouldn't
fail, or this test failure is possibly masking a real failure.

If *Setup* fails and everything else is skipped, but *Setup* did some stuff that
should be teared down, it should *Teardown* before it returns. It is still
possible to do do this on one line using a function pointer to shrink the
*Teardown* call to just `Teardown()`:
```vim
function! s:Setup_Testing_NameOfDol(Project)
    "Setup a fake project with all code and a tags file.
    "Place cursor on the Expect_ call line in the test_LUT.c
    "CD into the fake project directory.
    let Teardown = function('s:Teardown_AllTests', [a:Project])
    if !a:Project.Make(g:run_quickly)    | call Teardown() | return 0 | endif
    if !a:Project.UpdateTags()           | call Teardown() | return 0 | endif
    if !a:Project.GoToFile('test_LUT.c') | call Teardown() | return 0 | endif
    call search('Expect_')
    call a:Project.CdProjectDir()
    return 1
endfunction
```
This undoes that previous refactoring where I shaved off the final `return 1`:
```vim
function! s:Setup_TestsUsingFile_test_LUT(Project)
    "Setup a fake project. Prepare to run the test from file `test_LUT.c`.
    let Teardown = function('s:Teardown_AllTests', [a:Project])
    if !a:Project.Make(g:run_quickly)    | call Teardown() | return 0 | endif
    if !a:Project.GoToFile('test_LUT.c') | call Teardown() | return 0 | endif
    return 1
endfunction
```
Notice how the function pointer `Teardown` is created. The argument
`a:Project` is passed in the assignment. Later, when `Teardown()` is called, the
argument is not passed again. *Vim* knows to execute `s:Teardown_AllTests()`
with the argument `a:Project`! I think this is just amazing. And this is key to
doing a proper *Setup* and *Teardown*.

### Proper Setup and Teardown
A proper unit test environment provides a *Setup* and *Teardown*. Instead of
each test calling its own *Setup* and *Teardown*, the test runner selects the
*Setup* and *Teardown* functions to call for a group of tests. This reduces
noise in the test code making it easier to read what each test is actually
testing for. It also eliminates repetition.

As before, the test runner creates a `TestGroup` object to call tests and record
the results.
```vim
let tests = testgroup#New()
"... run tests
unlet tests
```
Here is a simplified version of `testgroup#New()` that is focused on how I
implemented *Setup* and *Teardown*.
```vim
function! testgroup#New()
    "USAGE: let tests = testgroup#New()
    let TestGroup = {
        \'RunTest'              : function('testgroup#RunTest'),
        \'Setup'                : function('testgroup#Setup'),
        \'Teardown'             : function('testgroup#Teardown')
        \}
    return TestGroup
endfunction
```


Eliminate 
I would modify `vimtdd` to invoke *Setup* and *Teardown* directly from
`vimtdd#RunTest()`, except I cannot figure out how to pass in `proj`. If I ever

### Convention to log fixture failures
My convention is that if `DoThing()` is called by fixtures using the
`if !DoThing() | return 0 | endif` idiom, then `DoThing()` should have an
`assert_` to log its failure to `v:errors` if it fails.

## less noisy fixtures
I would modify `vimtdd` to invoke *Setup* and *Teardown* directly from
`vimtdd#RunTest()`, except I cannot figure out how to pass in `proj`. If I ever
do figure that out, the code gets even simpler:
```vim
function! vimtdd#RunTest(test_name) dict
    "Call 'test_name' and log the result using the test-results object.
    let v:errors = []
    let Fref = function(a:test_name)
    " Only problem: how did proj get here?
    if self.Setup(proj) | call Fref() | call self.Teardown(proj) | endif
    call self._RecordThisTest(a:test_name)
endfunction
```
Then the client test would just be
```vim
function! ThisIsATestFile_returns_true_if_current_file_is_a_test_file()
    let proj = fakeproject#NewProject('project')
    call assert_true(expectmocking#ThisIsATestFile())
endfunction
```
And the runner would be
```vim
let tests = vimtdd#NewTestGroup()
tests.Setup = function('Setup_TestsUsingFile_test_LUT')
tests.Teardown = function('Teardown_AllTests')
call tests.RunTest('ThisIsATestFile_is_true_if_current_file_is_a_test_file')
```
Maybe *Setup* can just add a `FakeProject` dict to the `TestGroup` dict on the
fly and it will persist until *Teardown* calls `unlet proj`. But how would the
*Setup* definition know about the `TestGroup`? I think there is a way, see `:h
function(` and see `When {arglist} or {dict} is present this creates a
partial.`.

Even if that works, the test case itself cannot access the `FakeProject`.
But maybe that restriction will help me write cleaner test code, forcing all
fixturing work into the fixture code.

# Usage for a few lines of code to copy into a stand alone test script
## Set up for developing a Vim script
- Create *plugin* and *plugin unit test* files.

For example, for *plugin*
  `mock-c`:
```bash
~/.vim/pack/bundle/dev/mock-c/mock-c.vim
~/.vim/pack/bundle/dev/mock-c/test_mock-c.vim
```
- Copy `unit-test-vim.vim` to the `test_` file as a starting point.

## Write tests
- Define a test by calling an `assert_` function:
```vim
function! This_should_pass()
    call assert_equal(1,1)
endfunction
```
- Add the test to the test runner by calling `RunTest(test_name)`:
```vim
call RunTest(test_name)
```
`test_name` is a string. Each `test_name` is a function that defines one test.

For example:
```vim
call RunTest('This_should_pass')
```

## Output results
The last line in the test runner outputs the test results.

This **displays** the test results at the *Vim command line*:
```vim
call DisplayTestResults()
```

This **saves** the test results to file:
```vim
call SaveTestResultsToFile('./example-test-results.md')
```

### Example output:
```
This_should_fail_as_not_equal:FAIL: Expected 1 but got 2
This_should_fail_as_not_true:FAIL: Expected True but got 0
This_should_fail_with_a_message:FAIL: Result was false, here is the message.

------------------
5 Tests 3 Failures
```

## Free memory
The last line of script in the `test_` file frees the memory allocated to the
dictionary that holds the test results. This line cannot come before any other
test code.
```vim
call CollectTestResultGarbage()
```

## Careful about the order of stuff in the file
- Here is the rough outline and order:
    - copied from template:
        - private implementation of `unit-test-vim`
        - public API for `unit-test-vim`
    - test definitions
    - test runner:
        - run each test
        - display results
        - free memory

The test runner must come *after* the test definitions because `RunTest()`
creates a function reference. If the test function name is passed to `RunTest()`
before the function has been defined, the function reference does not point to
the function definition!

# Recommended folders
```bash
~/.vim/pack/bundle/dev/{plug-in-name}/{plug-in}.vim
~/.vim/pack/bundle/dev/{plug-in-name}/test_{plug-in}.vim
```

## Reasoning
The intent is to have a sane place to find everything. I know where to find
plugins. These scripts are like plugins, but they are for my internal use. And
if I ever make them plugins, the folder structure is already very similar.

The plugin folder structure is the new Vim 8.0 way to automatically load plugins
on startup, instead of using a plugin manager.

Everyone has a `.vim` folder in their home directory:

```bash
~/.vim
```

Add a `pack` subfolder to this:
```bash
~/.vim/pack
```

From `:help packages`:

>A Vim package is a directory that contains one or more plugins.

If the `pack/*/start/` convention is followed, the plugin folders under the
`start` folder are automatically added to the Vim global `runtimepath`.

```bash
~/.vim/pack/{any-name-you-want}/start/{plug-in-name}/plugin/{plug-in}.vim
```

For example:
```bash
~/.vim/pack/bundle/start/nerdtree/plugin/NERD_tree.vim
```

Mirroring the above, I am doing development on my own packages here:
```bash
~/.vim/pack/bundle/dev/{plug-in-name}/{plug-in}.vim
~/.vim/pack/bundle/dev/{plug-in-name}/test_{plug-in}.vim
```
For example:
```bash
~/.vim/pack/bundle/dev/mock-c/mock-c.vim
~/.vim/pack/bundle/dev/mock-c/test_mock-c.vim
```


# General tips for writing any Vim scripts:
## Put all function definitions before their callers!
If a function is defined after it is called, Vim uses whatever definition
the function had *the last time* it was sourced because Vim encounters the
call *before* it encounters the definition. The script has to be
**sourced twice** to see the effect of a change in a function definition.

The same goes for function references. A function reference is created like
this:
```vim
let FuncRef = function('NameOfSomeFunction')
```
The function reference is then used as an alias for the original function:
```vim
call NameOfSomeFunction()
call FuncRef()
```
Again, the function reference creation and any code that calls function must come
*after* the function definition.

## Do your own garbage collection.
`unlet` variables that are not local to functions.

Call `unlet` at the end of the script, e.g.:
```vim
   unlet s:results
```

# Repo links
Link to this repo: https://bitbucket.org/rainbots/unit-test-vim/src/master/

Clone this repo:

    git clone https://rainbots@bitbucket.org/rainbots/unit-test-vim.git
