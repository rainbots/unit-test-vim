" The main unit test lib is `testgroup.vim`.
" USAGE:
" 1. Create *dev* and *test* files:
"   ~/.vim/pack/bundle/dev/{plug-in-name}/{plug-in}.vim
"   ~/.vim/pack/bundle/dev/{plug-in-name}/test_{plug-in}.vim
" 2. Copy this unittest to the `test_` file as a starting point.

"----------------------------------------------------------------------------
"=====[ Private ]=====
" Use a `dict` as an "object" to store test results.
let s:_results = {}
let s:_results.failed = []
let s:_results.number_tested = 0
let s:_results.number_passed = 0
let s:_results.number_failed = 0
function! s:_results.record_this_test(test_name) dict
    "Increment number of tests/pass/fail. If `test_name` failed, record why.
    let self.number_tested += 1
    let test_passed = v:errors == []
    "if v:errors == []
    if test_passed
        let s:_results.number_passed += 1
    else
        let s:_results.number_failed += 1
        let fail_msg = a:test_name.':FAIL:'.split(v:errors[0],':')[1]
        call add(s:_results.failed, fail_msg)
    endif
endfunction
"=====[ Public ]=====
function! DisplayTestResults()
    for line in s:_results.failed
        echo line
    endfor
    echo '------------------'
    echo s:_results.number_tested 'Tests' s:_results.number_failed 'Failures'
    "Example:
    "------------------
    "1 Tests 0 Failures
endfunction
function! SaveTestResultsToFile(filepath)
    call add(s:_results.failed, '')
    call add(s:_results.failed, '------------------')
    let summary = s:_results.number_tested.' Tests '
                \.s:_results.number_failed.' Failures'
    call add(s:_results.failed, summary)
    "Example:
    "------------------
    "1 Tests 0 Failures
    call writefile(s:_results.failed, a:filepath)
endfunction
function! CollectTestResultGarbage()
    unlet s:_results
endfunction
function! RunTest(test_name)
    "Call 'test_name' and log the result using the test-results object.
    let v:errors = []
    let Fref = function(a:test_name)
    call Fref()
    call s:_results.record_this_test(a:test_name)
endfunction
"----------------------------------------------------------------------------

"=====[ Your tests ]=====
function! This_should_pass()
    call assert_equal(1,1)
endfunction
function! This_should_pass_also()
    call assert_equal('alice','alice')
endfunction
function! This_should_fail_as_not_equal()
    call assert_equal(1,2)
endfunction
function! This_should_fail_as_not_true()
    call assert_true(0)
endfunction
function! This_should_fail_with_a_message()
    call assert_true(0, 'Result was false, here is the message.')
endfunction

"=====[ Your test runner -- up to you to maintain this list! ]=====
call RunTest('This_should_pass')
call RunTest('This_should_fail_as_not_equal')
call RunTest('This_should_fail_as_not_true')
call RunTest('This_should_fail_with_a_message')
call RunTest('This_should_pass_also')
call DisplayTestResults()
call SaveTestResultsToFile('./example-test-results.md')
"=====[ This must be the last line of script in this file ]=====
call CollectTestResultGarbage()

" Full list of Vim assertions:
" assert_equal({expected}, {actual} [, {msg}])
" assert_equalfile({fname-one}, {fname-two})
" assert_exception({error} [, {msg}])
" assert_fails({cmd} [, {error}])
" assert_false({actual} [, {msg}])
" assert_inrange({lower}, {upper}, {actual} [, {msg}])
" assert_match({pattern}, {actual} [, {msg}])
" assert_notequal({expected}, {actual} [, {msg}])
" assert_notmatch({pattern}, {actual} [, {msg}])
" assert_report({msg})
" assert_true({actual} [, {msg}])

